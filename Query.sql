select CAST(CAST(t.YR AS varchar) + '-' + CAST(t.MN AS varchar) + '-' + CAST(t.Month_Part AS varchar) AS DATETIME), total
    from (
             SELECT YEAR(CreatedTime)  AS                                       YR,
                    MONTH(CreatedTime) AS                                       MN,
                    CASE WHEN DATEPART(DD, CreatedTime) < 16 THEN 1 ELSE 15 END Month_Part,
                    COUNT(1)           AS                                       total
             FROM InputTransactions
             GROUP BY YEAR(CreatedTime),
                      MONTH(CreatedTime),
                      CASE WHEN DATEPART(DD, CreatedTime) < 16 THEN 1 ELSE 15 END
         ) t


         ##########################

         select sum(a.totalAccount) as TotalAccount,
                sum(a.totalTrans)   as TotalTrans,
                sum(a.totalFlow)    as TotalFlow,
                sum(a.totalRevenue) as TotalRevenue,
                sum(a.totalAccountNew)   as TotalAccountNew,
                sum(a.totalTransNew)                                             as TotalTransNew,
                sum(a.totalFlowNew)                                            as TotalFlowNew,
                sum(a.totalRevenueNew)                                              as TotalRevenueNew,
                a.datetime          as Datetime
         from (
                  select count(distinct t.AccountID)                    as totalAccount,
                         0                                              as totalTrans,
                         0                                              as totalFlow,
                         0                                              as totalRevenue,
                         0                                              as totalAccountNew,
                         0                                              as totalTransNew,
                         0                                              as totalFlowNew,
                         0                                              as totalRevenueNew,
                         dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                  from (
                           select it.AccountID, it.CreatedTime, it.ServiceID, it.RoomID, it.SourceID
                           FROM InputTransactions it
                           where it.CreatedTime between :fromDate and :toDate
                           union all
                           select ot.AccountID, ot.CreatedTime, ot.ServiceID, ot.RoomID, ot.SourceID
                           FROM OutputTransactions ot
                           where ot.CreatedTime between :fromDate and :toDate
                       ) t
                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))


                  union all

                  select 0                                              as totalAccount,
                         count(1)                                       as totalTrans,
                         0                                              as totalFlow,
                         0                                              as totalRevenue,
                         0                                              as totalAccountNew,
                         0                                              as totalTransNew,
                         0                                              as totalFlowNew,
                         0                                              as totalRevenueNew,
                         dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                  from (
                           select it.AccountID, it.CreatedTime, it.ServiceID, it.RoomID, it.SourceID
                           FROM InputTransactions it
                           where it.CreatedTime between :fromDate and :toDate
                           union all
                           select ot.AccountID, ot.CreatedTime, ot.ServiceID, ot.RoomID, ot.SourceID
                           FROM OutputTransactions ot
                           where ot.CreatedTime between :fromDate and :toDate
                       ) t

                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                  union all

                  select 0                                              as totalAccount,
                         0                                              as totalTrans,
                         sum((t.OutAmount - t.InAmount))                as totalFlow,
                         0                                              as totalRevenue,
                         0                                              as totalAccountNew,
                         0                                              as totalTransNew,
                         0                                              as totalFlowNew,
                         0                                              as totalRevenueNew,
                         dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                  from (
                           select it.AccountID,
                                  it.CreatedTime,
                                  it.ServiceID,
                                  it.RoomID,
                                  it.SourceID,
                                  it.Amount as InAmount,
                                  0         as
                                               OutAmount,
                                  s.InOut
                           FROM InputTransactions it
                                    inner join Services s on s.ServiceID = it.ServiceID
                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                           WHERE s.InOut = 3
                             and st.CategoryId = 1
                             and it.CreatedTime between :fromDate and :toDate
                           union all
                           select ot.AccountID,
                                  ot.CreatedTime,
                                  ot.ServiceID,
                                  ot.RoomID,
                                  ot.SourceID,
                                  0         as InAmount,
                                  ot.Amount as OutAmount,
                                  s.InOut
                           FROM OutputTransactions ot
                                    inner join Services s on s.ServiceID = ot.ServiceID
                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                           WHERE s.InOut = 1
                             and st.CategoryId = 1
                             and ot.CreatedTime between :fromDate and :toDate
                       ) t

                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                  union all

                  select 0                                              as totalAccount,
                         0                                              as totalTrans,
                         0                                              as totalFlow,
                         sum((t.OutAmount - t.InAmount))                as totalRevenue,
                         0                                              as totalAccountNew,
                         0                                              as totalTransNew,
                         0                                              as totalFlowNew,
                         0                                              as totalRevenueNew,
                         dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                  from (
                           select it.AccountID,
                                  it.CreatedTime,
                                  it.ServiceID,
                                  it.RoomID,
                                  it.SourceID,
                                  it.Amount as InAmount,
                                  0         as
                                               OutAmount,
                                  s.InOut
                           FROM InputTransactions it
                                    inner join Services s on s.ServiceID = it.ServiceID
                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID

                           WHERE (s.InOut = 3 or s.InOut = 2)
                             and st.CategoryId = 1
                             and it.CreatedTime between :fromDate and :toDate
                           union all
                           select ot.AccountID,
                                  ot.CreatedTime,
                                  ot.ServiceID,
                                  ot.RoomID,
                                  ot.SourceID,
                                  0         as InAmount,
                                  ot.Amount as OutAmount,
                                  s.InOut
                           FROM OutputTransactions ot
                                    inner join Services s on s.ServiceID = ot.ServiceID
                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                           WHERE s.InOut = 1
                             and st.CategoryId = 1
                             and ot.CreatedTime between :fromDate and :toDate
                       ) t
                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                  union all

                  select 0                    as totalAccount,
                         0                                              as totalTrans,
                         0                                              as totalFlow,
                         0                                              as totalRevenue,
                         count(distinct t.AccountID)                    as totalAccountNew,
                         0                                              as totalTransNew,
                         0                                              as totalFlowNew,
                         0                                              as totalRevenueNew,
                         dateadd(DAY, 0, datediff(day, 0, t.CreatedTime)) as datetime
                  from (
                           select it.AccountID, it.CreatedTime, it.ServiceID, it.RoomID, it.SourceID
                           FROM InputTransactions it
                                    inner join
                                (select AccountID, CreateTime from Accounts
                                    WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = it.AccountID
                           where it.CreatedTime between :fromDate and :toDate
                                    and cast(a.CreateTime as date) = cast(it.CreatedTime as DATE)
                           union all
                           select ot.AccountID, ot.CreatedTime, ot.ServiceID, ot.RoomID, ot.SourceID
                           FROM OutputTransactions ot
                                    inner join
                                (select AccountID, CreateTime from Accounts
                                    WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = ot.AccountID
                           where ot.CreatedTime between :fromDate and :toDate
                                    and cast(a.CreateTime as date) = cast(ot.CreatedTime as DATE)
                       ) t
                  GROUP BY dateadd(DAY, 0, datediff(day, 0, t.CreatedTime))

                  union all

                  select 0                                              as totalAccount,
                         0                                       as totalTrans,
                         0                                              as totalFlow,
                         0                                              as totalRevenue,
                         0                                              as totalAccountNew,
                         count(1)                                              as totalTransNew,
                         0                                              as totalFlowNew,
                         0                                              as totalRevenueNew,
                         dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                  from (
                           select it.AccountID, it.CreatedTime, it.ServiceID, it.RoomID, it.SourceID
                           FROM InputTransactions it
                                    inner join
                                (select AccountID, CreateTime from Accounts
                                    WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = it.AccountID
                           where it.CreatedTime between :fromDate and :toDate
                                    and cast(a.CreateTime as date) = cast(it.CreatedTime as DATE)
                           union all
                           select ot.AccountID, ot.CreatedTime, ot.ServiceID, ot.RoomID, ot.SourceID
                           FROM OutputTransactions ot
                                    inner join
                                (select AccountID, CreateTime from Accounts
                                    WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = ot.AccountID
                           where ot.CreatedTime between :fromDate and :toDate
                                    and cast(a.CreateTime as date) = cast(ot.CreatedTime as DATE)
                       ) t
                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                  union all

                  select 0                                              as totalAccount,
                         0                                              as totalTrans,
                         0                as totalFlow,
                         0                                              as totalRevenue,
                         0                                              as totalAccountNew,
                         0                                              as totalTransNew,
                         sum((t.OutAmount - t.InAmount))                                              as totalFlowNew,
                         0                                              as totalRevenueNew,
                         dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                  from (
                           select it.AccountID,
                                  it.CreatedTime,
                                  it.ServiceID,
                                  it.RoomID,
                                  it.SourceID,
                                  it.Amount as InAmount,
                                  0         as
                                               OutAmount,
                                  s.InOut
                           FROM InputTransactions it
                                    inner join Services s on s.ServiceID = it.ServiceID
                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                    inner join
                                (select AccountID, CreateTime from Accounts
                                    WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = it.AccountID
                           WHERE s.InOut = 3
                             and st.CategoryId = 1
                             and it.CreatedTime between :fromDate and :toDate
                                    and cast(a.CreateTime as date) = cast(it.CreatedTime as DATE)
                           union all
                           select ot.AccountID,
                                  ot.CreatedTime,
                                  ot.ServiceID,
                                  ot.RoomID,
                                  ot.SourceID,
                                  0         as InAmount,
                                  ot.Amount as OutAmount,
                                  s.InOut
                           FROM OutputTransactions ot
                                    inner join Services s on s.ServiceID = ot.ServiceID
                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                    inner join
                                (select AccountID, CreateTime from Accounts
                                    WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = ot.AccountID
                           WHERE s.InOut = 1
                             and st.CategoryId = 1
                             and ot.CreatedTime between :fromDate and :toDate
                                    and cast(a.CreateTime as date) = cast(ot.CreatedTime as DATE)
                       ) t
                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                  union all

                  select 0                                              as totalAccount,
                         0                                              as totalTrans,
                         0                                              as totalFlow,
                         0               as totalRevenue,
                         0                                              as totalAccountNew,
                         0                                              as totalTransNew,
                         0                                              as totalFlowNew,
                         sum((t.OutAmount - t.InAmount))                                               as totalRevenueNew,
                         dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                  from (
                           select it.AccountID,
                                  it.CreatedTime,
                                  it.ServiceID,
                                  it.RoomID,
                                  it.SourceID,
                                  it.Amount as InAmount,
                                  0         as
                                               OutAmount,
                                  s.InOut
                           FROM InputTransactions it
                                    inner join Services s on s.ServiceID = it.ServiceID
                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                    inner join
                                (select AccountID, CreateTime from Accounts
                                    WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = it.AccountID

                           WHERE (s.InOut = 3 or s.InOut = 2)
                             and st.CategoryId = 1
                             and it.CreatedTime between :fromDate and :toDate
                             and cast(a.CreateTime as date) = cast(it.CreatedTime as DATE)

                           union all
                           select ot.AccountID,
                                  ot.CreatedTime,
                                  ot.ServiceID,
                                  ot.RoomID,
                                  ot.SourceID,
                                  0         as InAmount,
                                  ot.Amount as OutAmount,
                                  s.InOut
                           FROM OutputTransactions ot
                                    inner join Services s on s.ServiceID = ot.ServiceID
                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                    inner join
                                (select AccountID, CreateTime from Accounts
                                    WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = ot.AccountID
                           WHERE s.InOut = 1
                             and st.CategoryId = 1
                             and ot.CreatedTime between :fromDate and :toDate
                             and cast(a.CreateTime as date) = cast(ot.CreatedTime as DATE)
                       ) t
                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

              ) a

         group by a.datetime
         order by a.datetime


         ########################
         select sum(a.totalAccount) as TotalAccount,
                        sum(a.totalTrans) as TotalTrans,
                        sum(a.totalFlow) as TotalFlow,
                        sum(a.totalRevenue) as TotalRevenue
                        , a.datetime as Datetime
                 from (
                          select count(distinct t.AccountID)                    as totalAccount,
                                 0                                              as totalTrans,
                                 0                                              as totalFlow,
                                 0                                              as totalRevenue,
                                 dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                          from (
                                   select it.AccountID, it.CreatedTime, it.ServiceID, it.RoomID, it.SourceID
                                   FROM InputTransactions it
                                   where it.CreatedTime between #{fromDate} and #{toDate}
                                   union all
                                   select ot.AccountID, ot.CreatedTime, ot.ServiceID, ot.RoomID, ot.SourceID
                                   FROM OutputTransactions ot
                                   where ot.CreatedTime between #{fromDate} and #{toDate}
                               ) t


                          GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))
                          union all
                          select 0                                              as totalAccount,
                                 count(1)                                       as totalTrans,
                                 0                                              as totalFlow,
                                 0                                              as totalRevenue,
                                 dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                          from (
                                   select it.AccountID, it.CreatedTime, it.ServiceID, it.RoomID, it.SourceID
                                   FROM InputTransactions it
                                   where it.CreatedTime between #{fromDate} and #{toDate}
                                   union all
                                   select ot.AccountID, ot.CreatedTime, ot.ServiceID, ot.RoomID, ot.SourceID
                                   FROM OutputTransactions ot
                                   where ot.CreatedTime between #{fromDate} and #{toDate}
                               ) t

                          GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                          union all

                          select 0                                              as totalAccount,
                                 0                                              as totalTrans,
                                 sum((t.OutAmount - t.InAmount))                as totalFlow,
                                 0                                              as totalRevenue,
                                 dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                          from (
                                   select it.AccountID,
                                          it.CreatedTime,
                                          it.ServiceID,
                                          it.RoomID,
                                          it.SourceID,
                                          it.Amount as InAmount,
                                          0         as
                                                       OutAmount,
                                          s.InOut
                                   FROM InputTransactions it
                                            inner join Services s on s.ServiceID = it.ServiceID
                                            inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                   WHERE s.InOut = 3
                                     and st.CategoryId = 1
                                     and it.CreatedTime between #{fromDate} and #{toDate}
                                   union all
                                   select ot.AccountID,
                                          ot.CreatedTime,
                                          ot.ServiceID,
                                          ot.RoomID,
                                          ot.SourceID,
                                          0         as InAmount,
                                          ot.Amount as OutAmount,
                                          s.InOut
                                   FROM OutputTransactions ot
                                            inner join Services s on s.ServiceID = ot.ServiceID
                                            inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                   WHERE s.InOut = 1
                                     and st.CategoryId = 1
                                     and ot.CreatedTime between #{fromDate} and #{toDate}
                               ) t

                          GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                          union all

                          select 0                                              as totalAccount,
                                 0                                              as totalTrans,
                                 0                                              as totalFlow,
                                 sum((t.OutAmount - t.InAmount))                as totalRevenue,
                                 dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                          from (
                                   select it.AccountID,
                                          it.CreatedTime,
                                          it.ServiceID,
                                          it.RoomID,
                                          it.SourceID,
                                          it.Amount as InAmount,
                                          0         as
                                                       OutAmount,
                                          s.InOut
                                   FROM InputTransactions it
                                            inner join Services s on s.ServiceID = it.ServiceID
                                            inner join ServicesType st on s.ServiceType = st.ServicesTypeID

                                   WHERE (s.InOut = 3 or s.InOut = 2)
                                     and st.CategoryId = 1
                                     and it.CreatedTime between #{fromDate} and #{toDate}
                                   union all
                                   select ot.AccountID,
                                          ot.CreatedTime,
                                          ot.ServiceID,
                                          ot.RoomID,
                                          ot.SourceID,
                                          0         as InAmount,
                                          ot.Amount as OutAmount,
                                          s.InOut
                                   FROM OutputTransactions ot
                                            inner join Services s on s.ServiceID = ot.ServiceID
                                            inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                   WHERE s.InOut = 1
                                     and st.CategoryId = 1
                                     and ot.CreatedTime between #{fromDate} and #{toDate}
                               ) t
                          GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))) a

                 group by a.datetime
                 order by a.datetime



                 #########################################

                 <select id="findAllReportStatisticsSummaryNewAccount"
                             resultMap="ReportStatistics">
                         select sum(a.totalAccount) as TotalAccount,
                                sum(a.totalTrans) as TotalTrans,
                                sum(a.totalFlow) as TotalFlow,
                                sum(a.totalRevenue) as TotalRevenue
                                 , a.datetime as Datetime
                         from (
                                  select count(distinct t.AccountID)                    as totalAccount,
                                         0                                              as totalTrans,
                                         0                                              as totalFlow,
                                         0                                              as totalRevenue,
                                         dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                                  from (
                                           select it.AccountID, it.CreatedTime, it.ServiceID, it.RoomID, it.SourceID
                                           FROM InputTransactions it
                                                    inner join
                                                (select AccountID, CreateTime from Accounts
                                                    WHERE CreateTime between #{fromDate} and #{toDate}) as a on a.AccountID = it.AccountID
                                           where it.CreatedTime between #{fromDate} and #{toDate}
                                           union all
                                           select ot.AccountID, ot.CreatedTime, ot.ServiceID, ot.RoomID, ot.SourceID
                                           FROM OutputTransactions ot
                                                    inner join
                                                (select AccountID, CreateTime from Accounts
                                                    WHERE CreateTime between #{fromDate} and #{toDate}) as a on a.AccountID = ot.AccountID
                                           where ot.CreatedTime between #{fromDate} and #{toDate}
                                       ) t

                                           inner join Accounts a on a.AccountID = t.AccountID
                                  where convert(varchar(10), a.CreateTime, 102) = convert(varchar(10), dateadd(DAY, 0, datediff(day, 0, t.CreatedTime)), 102)
                                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))
                                  union all
                                  select 0                                              as totalAccount,
                                         count(1)                                       as totalTrans,
                                         0                                              as totalFlow,
                                         0                                              as totalRevenue,
                                         dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                                  from (
                                           select it.AccountID, it.CreatedTime, it.ServiceID, it.RoomID, it.SourceID
                                           FROM InputTransactions it
                                           where it.CreatedTime between #{fromDate} and #{toDate}
                                           union all
                                           select ot.AccountID, ot.CreatedTime, ot.ServiceID, ot.RoomID, ot.SourceID
                                           FROM OutputTransactions ot
                                           where ot.CreatedTime between #{fromDate} and #{toDate}
                                       ) t
                                           inner join Accounts a on a.AccountID = t.AccountID
                                  where convert(varchar(10), a.CreateTime, 102) = convert(varchar(10), dateadd(DAY, 0, datediff(day, 0, t.CreatedTime)), 102)
                                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                                  union all

                                  select 0                                              as totalAccount,
                                         0                                              as totalTrans,
                                         sum((t.OutAmount - t.InAmount))                as totalFlow,
                                         0                                              as totalRevenue,
                                         dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                                  from (
                                           select it.AccountID,
                                                  it.CreatedTime,
                                                  it.ServiceID,
                                                  it.RoomID,
                                                  it.SourceID,
                                                  it.Amount as InAmount,
                                                  0         as
                                                               OutAmount,
                                                  s.InOut
                                           FROM InputTransactions it
                                                    inner join Services s on s.ServiceID = it.ServiceID
                                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                           WHERE s.InOut = 3
                                             and st.CategoryId = 1
                                             and it.CreatedTime between #{fromDate} and #{toDate}
                                           union all
                                           select ot.AccountID,
                                                  ot.CreatedTime,
                                                  ot.ServiceID,
                                                  ot.RoomID,
                                                  ot.SourceID,
                                                  0         as InAmount,
                                                  ot.Amount as OutAmount,
                                                  s.InOut
                                           FROM OutputTransactions ot
                                                    inner join Services s on s.ServiceID = ot.ServiceID
                                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                           WHERE s.InOut = 1
                                             and st.CategoryId = 1
                                             and ot.CreatedTime between #{fromDate} and #{toDate}
                                       ) t
                                           inner join Accounts a on a.AccountID = t.AccountID
                                  where convert(varchar(10), a.CreateTime, 102) = convert(varchar(10), dateadd(DAY, 0, datediff(day, 0, t.CreatedTime)), 102)
                                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                                  union all

                                  select 0                                              as totalAccount,
                                         0                                              as totalTrans,
                                         0                                              as totalFlow,
                                         sum((t.OutAmount - t.InAmount))                as totalRevenue,
                                         dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                                  from (
                                           select it.AccountID,
                                                  it.CreatedTime,
                                                  it.ServiceID,
                                                  it.RoomID,
                                                  it.SourceID,
                                                  it.Amount as InAmount,
                                                  0         as
                                                               OutAmount,
                                                  s.InOut
                                           FROM InputTransactions it
                                                    inner join Services s on s.ServiceID = it.ServiceID
                                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID

                                           WHERE (s.InOut = 3 or s.InOut = 2)
                                             and st.CategoryId = 1
                                             and it.CreatedTime between #{fromDate} and #{toDate}
                                           union all
                                           select ot.AccountID,
                                                  ot.CreatedTime,
                                                  ot.ServiceID,
                                                  ot.RoomID,
                                                  ot.SourceID,
                                                  0         as InAmount,
                                                  ot.Amount as OutAmount,
                                                  s.InOut
                                           FROM OutputTransactions ot
                                                    inner join Services s on s.ServiceID = ot.ServiceID
                                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                           WHERE s.InOut = 1
                                             and st.CategoryId = 1
                                             and ot.CreatedTime between #{fromDate} and #{toDate}
                                       ) t
                                           inner join Accounts a on a.AccountID = t.AccountID
                                  where convert(varchar(10), a.CreateTime, 102) = convert(varchar(10), dateadd(DAY, 0, datediff(day, 0, t.CreatedTime)), 102)
                                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))) a

                         group by a.datetime
                         order by a.datetime

                     </select>
                     <select id="findAllReportStatisticsSummaryNewAccountDetails"
                             resultMap="ReportStatistics">

                         select
                             a.ServiceType,
                             a.ServiceName,
                             sum(a.totalAccount) as TotalAccount,
                             sum(a.totalTrans) as TotalTrans,
                             sum(a.totalFlow) as TotalFlow,
                             sum(a.totalRevenue) as TotalRevenue
                                 , a.datetime as Datetime
                         from (
                                  select
                                      t.ServiceType,
                                      t.ServiceName,
                                      count(distinct t.AccountID)                    as totalAccount,
                                      0                                              as totalTrans,
                                      0                                              as totalFlow,
                                      0                                              as totalRevenue,
                                      dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                                  from (
                                           select it.AccountID, s.ServiceType, it.CreatedTime, st.ServiceName
                                           FROM InputTransactions it
                                                    inner join Services s on it.ServiceID = s.ServiceID
                                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                           where it.CreatedTime between #{fromDate} and #{toDate}
                                           union all
                                           select ot.AccountID, s.ServiceType, ot.CreatedTime, st.ServiceName
                                           FROM OutputTransactions ot
                                                    inner join Services s on ot.ServiceID = s.ServiceID
                                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                           where ot.CreatedTime between #{fromDate} and #{toDate}
                                       ) t
                                           inner join Accounts a on a.AccountID = t.AccountID
                                  where convert(varchar(10), a.CreateTime, 102) = convert(varchar(10), dateadd(DAY, 0, datediff(day, 0, t.CreatedTime)), 102)
                                  GROUP BY  t.ServiceType, t.ServiceName, dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                                  union all

                                  select
                                      t.ServiceType,
                                      t.ServiceName,

                                      0                                              as totalAccount,
                                      count(1)                                       as totalTrans,
                                      0                                              as totalFlow,
                                      0                                              as totalRevenue,
                                      dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                                  from (
                                           select it.AccountID, s.ServiceType, it.CreatedTime, st.ServiceName
                                           FROM InputTransactions it
                                                    inner join Services s on it.ServiceID = s.ServiceID
                                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                           where it.CreatedTime between #{fromDate} and #{toDate}
                                           union all
                                           select ot.AccountID, s.ServiceType, ot.CreatedTime, st.ServiceName
                                           FROM OutputTransactions ot
                                                    inner join Services s on ot.ServiceID = s.ServiceID
                                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                           where ot.CreatedTime between #{fromDate} and #{toDate}
                                       ) t
                                           inner join Accounts a on a.AccountID = t.AccountID
                                  where convert(varchar(10), a.CreateTime, 102) = convert(varchar(10), dateadd(DAY, 0, datediff(day, 0, t.CreatedTime)), 102)
                                  GROUP BY  t.ServiceType, t.ServiceName, dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                                  union all

                                  select
                                      t.ServiceType,
                                      t.ServiceName,
                                      0                                              as totalAccount,
                                      0                                              as totalTrans,
                                      sum((t.OutAmount - t.InAmount))                as totalFlow,
                                      0                                              as totalRevenue,
                                      dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                                  from (
                                           select it.AccountID,
                                                  s.ServiceType, it.CreatedTime, st.ServiceName,
                                                  it.Amount as InAmount,
                                                  0         as
                                                               OutAmount
                                           FROM InputTransactions it
                                                    inner join Services s on s.ServiceID = it.ServiceID
                                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                           WHERE s.InOut = 3
                                             and st.CategoryId = 1
                                             and it.CreatedTime between #{fromDate} and #{toDate}
                                           union all
                                           select ot.AccountID,
                                                  s.ServiceType, ot.CreatedTime, st.ServiceName,
                                                  0         as InAmount,
                                                  ot.Amount as OutAmount
                                           FROM OutputTransactions ot
                                                    inner join Services s on s.ServiceID = ot.ServiceID
                                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                           WHERE s.InOut = 1
                                             and st.CategoryId = 1
                                             and ot.CreatedTime between #{fromDate} and #{toDate}
                                       ) t
                                           inner join Accounts a on a.AccountID = t.AccountID
                                  where convert(varchar(10), a.CreateTime, 102) = convert(varchar(10), dateadd(DAY, 0, datediff(day, 0, t.CreatedTime)), 102)
                                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime)), t.ServiceType, t.ServiceName

                                  union all

                                  select
                                      t.ServiceType,
                                      t.ServiceName,
                                      0                                              as totalAccount,
                                      0                                              as totalTrans,
                                      0                                              as totalFlow,
                                      sum((t.OutAmount - t.InAmount))                as totalRevenue,
                                      dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                                  from (
                                           select it.AccountID,
                                                  s.ServiceType, it.CreatedTime, st.ServiceName,
                                                  it.Amount as InAmount,
                                                  0         as
                                                               OutAmount
                                           FROM InputTransactions it
                                                    inner join Services s on s.ServiceID = it.ServiceID
                                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID

                                           WHERE (s.InOut = 3 or s.InOut = 2)
                                             and st.CategoryId = 1
                                             and it.CreatedTime between #{fromDate} and #{toDate}
                                           union all
                                           select ot.AccountID,
                                                  s.ServiceType, ot.CreatedTime, st.ServiceName,
                                                  0         as InAmount,
                                                  ot.Amount as OutAmount
                                           FROM OutputTransactions ot
                                                    inner join Services s on s.ServiceID = ot.ServiceID
                                                    inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                           WHERE s.InOut = 1
                                             and st.CategoryId = 1
                                             and ot.CreatedTime between #{fromDate} and #{toDate}
                                       ) t
                                           inner join Accounts a on a.AccountID = t.AccountID
                                  where convert(varchar(10), a.CreateTime, 102) = convert(varchar(10), dateadd(DAY, 0, datediff(day, 0, t.CreatedTime)), 102)
                                  GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime)), t.ServiceType, t.ServiceName) a
                         group by a.datetime, a.ServiceName, a.ServiceType
                         order by a.datetime
                     </select>

                     ########################################
                     select sum(a.totalAccount)    as TotalAccount,
                            sum(a.totalTrans)      as TotalTrans,
                            sum(a.totalFlow)       as TotalFlow,
                            sum(a.totalRevenue)    as TotalRevenue,
                            sum(a.totalAccountNew) as TotalAccountNew,
                            sum(a.totalTransNew)   as TotalTransNew,
                            sum(a.totalFlowNew)    as TotalFlowNew,
                            sum(a.totalRevenueNew) as TotalRevenueNew,
                            a.datetime             as Datetime
                     from (
                              select count(distinct t.AccountID)                    as totalAccount,
                                     0                                              as totalTrans,
                                     0                                              as totalFlow,
                                     0                                              as totalRevenue,
                                     0                                              as totalAccountNew,
                                     0                                              as totalTransNew,
                                     0                                              as totalFlowNew,
                                     0                                              as totalRevenueNew,
                                     dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                              from (
                                       select it.AccountID, it.CreatedTime, it.ServiceID, it.RoomID, it.SourceID
                                       FROM InputTransactions it
                                                inner join Services s on s.ServiceID = it.ServiceID
                                       where it.CreatedTime between :fromDate and :toDate
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or it.RoomID = :roomId)
                                         and (:sourceId is null or it.SourceID = :sourceId)
                                       union all
                                       select ot.AccountID, ot.CreatedTime, ot.ServiceID, ot.RoomID, ot.SourceID
                                       FROM OutputTransactions ot
                                                inner join Services s on s.ServiceID = ot.ServiceID
                                       where ot.CreatedTime between :fromDate and :toDate
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or ot.RoomID = :roomId)
                                         and (:sourceId is null or ot.SourceID = :sourceId)
                                   ) t
                              GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))


                              union all

                              select 0                                              as totalAccount,
                                     count(1)                                       as totalTrans,
                                     0                                              as totalFlow,
                                     0                                              as totalRevenue,
                                     0                                              as totalAccountNew,
                                     0                                              as totalTransNew,
                                     0                                              as totalFlowNew,
                                     0                                              as totalRevenueNew,
                                     dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                              from (
                                       select it.AccountID, it.CreatedTime, it.ServiceID, it.RoomID, it.SourceID
                                       FROM InputTransactions it
                                                inner join Services s on s.ServiceID = it.ServiceID
                                       where it.CreatedTime between :fromDate and :toDate
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or it.RoomID = :roomId)
                                         and (:sourceId is null or it.SourceID = :sourceId)
                                       union all
                                       select ot.AccountID, ot.CreatedTime, ot.ServiceID, ot.RoomID, ot.SourceID
                                       FROM OutputTransactions ot
                                                inner join Services s on s.ServiceID = ot.ServiceID
                                       where ot.CreatedTime between :fromDate and :toDate
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or ot.RoomID = :roomId)
                                         and (:sourceId is null or ot.SourceID = :sourceId)
                                   ) t

                              GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                              union all

                              select 0                                              as totalAccount,
                                     0                                              as totalTrans,
                                     sum((t.OutAmount - t.InAmount))                as totalFlow,
                                     0                                              as totalRevenue,
                                     0                                              as totalAccountNew,
                                     0                                              as totalTransNew,
                                     0                                              as totalFlowNew,
                                     0                                              as totalRevenueNew,
                                     dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                              from (
                                       select it.AccountID,
                                              it.CreatedTime,
                                              it.ServiceID,
                                              it.RoomID,
                                              it.SourceID,
                                              it.Amount as InAmount,
                                              0         as
                                                           OutAmount,
                                              s.InOut
                                       FROM InputTransactions it
                                                inner join Services s on s.ServiceID = it.ServiceID
                                                inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                       WHERE s.InOut = 3
                                         and st.CategoryId = 1
                                         and it.CreatedTime between :fromDate and :toDate
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or it.RoomID = :roomId)
                                         and (:sourceId is null or it.SourceID = :sourceId)
                                       union all
                                       select ot.AccountID,
                                              ot.CreatedTime,
                                              ot.ServiceID,
                                              ot.RoomID,
                                              ot.SourceID,
                                              0         as InAmount,
                                              ot.Amount as OutAmount,
                                              s.InOut
                                       FROM OutputTransactions ot
                                                inner join Services s on s.ServiceID = ot.ServiceID
                                                inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                       WHERE s.InOut = 1
                                         and st.CategoryId = 1
                                         and ot.CreatedTime between :fromDate and :toDate
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or ot.RoomID = :roomId)
                                         and (:sourceId is null or ot.SourceID = :sourceId)
                                   ) t

                              GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                              union all

                              select 0                                              as totalAccount,
                                     0                                              as totalTrans,
                                     0                                              as totalFlow,
                                     sum((t.OutAmount - t.InAmount))                as totalRevenue,
                                     0                                              as totalAccountNew,
                                     0                                              as totalTransNew,
                                     0                                              as totalFlowNew,
                                     0                                              as totalRevenueNew,
                                     dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                              from (
                                       select it.AccountID,
                                              it.CreatedTime,
                                              it.ServiceID,
                                              it.RoomID,
                                              it.SourceID,
                                              it.Amount as InAmount,
                                              0         as
                                                           OutAmount,
                                              s.InOut
                                       FROM InputTransactions it
                                                inner join Services s on s.ServiceID = it.ServiceID
                                                inner join ServicesType st on s.ServiceType = st.ServicesTypeID

                                       WHERE (s.InOut = 3 or s.InOut = 2)
                                         and st.CategoryId = 1
                                         and it.CreatedTime between :fromDate and :toDate
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or it.RoomID = :roomId)
                                         and (:sourceId is null or it.SourceID = :sourceId)
                                       union all
                                       select ot.AccountID,
                                              ot.CreatedTime,
                                              ot.ServiceID,
                                              ot.RoomID,
                                              ot.SourceID,
                                              0         as InAmount,
                                              ot.Amount as OutAmount,
                                              s.InOut
                                       FROM OutputTransactions ot
                                                inner join Services s on s.ServiceID = ot.ServiceID
                                                inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                       WHERE s.InOut = 1
                                         and st.CategoryId = 1
                                         and ot.CreatedTime between :fromDate and :toDate
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or ot.RoomID = :roomId)
                                         and (:sourceId is null or ot.SourceID = :sourceId)
                                   ) t
                              GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                              union all

                              select 0                                                as totalAccount,
                                     0                                                as totalTrans,
                                     0                                                as totalFlow,
                                     0                                                as totalRevenue,
                                     count(distinct t.AccountID)                      as totalAccountNew,
                                     0                                                as totalTransNew,
                                     0                                                as totalFlowNew,
                                     0                                                as totalRevenueNew,
                                     dateadd(DAY, 0, datediff(day, 0, t.CreatedTime)) as datetime
                              from (
                                       select it.AccountID, it.CreatedTime, it.ServiceID, it.RoomID, it.SourceID
                                       FROM InputTransactions it
                                                inner join Services s on s.ServiceID = it.ServiceID
                                                inner join
                                            (select AccountID, CreateTime
                                             from Accounts
                                             WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = it.AccountID
                                       where it.CreatedTime between :fromDate and :toDate
                                         and cast(a.CreateTime as date) = cast(it.CreatedTime as DATE)
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or it.RoomID = :roomId)
                                         and (:sourceId is null or it.SourceID = :sourceId)
                                       union all
                                       select ot.AccountID, ot.CreatedTime, ot.ServiceID, ot.RoomID, ot.SourceID
                                       FROM OutputTransactions ot
                                                inner join Services s on s.ServiceID = ot.ServiceID
                                                inner join
                                            (select AccountID, CreateTime
                                             from Accounts
                                             WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = ot.AccountID
                                       where ot.CreatedTime between :fromDate and :toDate
                                         and cast(a.CreateTime as date) = cast(ot.CreatedTime as DATE)
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or ot.RoomID = :roomId)
                                         and (:sourceId is null or ot.SourceID = :sourceId)
                                   ) t
                              GROUP BY dateadd(DAY, 0, datediff(day, 0, t.CreatedTime))

                              union all

                              select 0                                              as totalAccount,
                                     0                                              as totalTrans,
                                     0                                              as totalFlow,
                                     0                                              as totalRevenue,
                                     0                                              as totalAccountNew,
                                     count(1)                                       as totalTransNew,
                                     0                                              as totalFlowNew,
                                     0                                              as totalRevenueNew,
                                     dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                              from (
                                       select it.AccountID, it.CreatedTime, it.ServiceID, it.RoomID, it.SourceID
                                       FROM InputTransactions it
                                                inner join Services s on s.ServiceID = it.ServiceID
                                                inner join
                                            (select AccountID, CreateTime
                                             from Accounts
                                             WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = it.AccountID
                                       where it.CreatedTime between :fromDate and :toDate
                                         and cast(a.CreateTime as date) = cast(it.CreatedTime as DATE)
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or it.RoomID = :roomId)
                                         and (:sourceId is null or it.SourceID = :sourceId)
                                       union all
                                       select ot.AccountID, ot.CreatedTime, ot.ServiceID, ot.RoomID, ot.SourceID
                                       FROM OutputTransactions ot
                                                inner join Services s on s.ServiceID = ot.ServiceID
                                                inner join
                                            (select AccountID, CreateTime
                                             from Accounts
                                             WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = ot.AccountID
                                       where ot.CreatedTime between :fromDate and :toDate
                                         and cast(a.CreateTime as date) = cast(ot.CreatedTime as DATE)
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or ot.RoomID = :roomId)
                                         and (:sourceId is null or ot.SourceID = :sourceId)
                                   ) t
                              GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                              union all

                              select 0                                              as totalAccount,
                                     0                                              as totalTrans,
                                     0                                              as totalFlow,
                                     0                                              as totalRevenue,
                                     0                                              as totalAccountNew,
                                     0                                              as totalTransNew,
                                     sum((t.OutAmount - t.InAmount))                as totalFlowNew,
                                     0                                              as totalRevenueNew,
                                     dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                              from (
                                       select it.AccountID,
                                              it.CreatedTime,
                                              it.ServiceID,
                                              it.RoomID,
                                              it.SourceID,
                                              it.Amount as InAmount,
                                              0         as
                                                           OutAmount,
                                              s.InOut
                                       FROM InputTransactions it
                                                inner join Services s on s.ServiceID = it.ServiceID
                                                inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                                inner join
                                            (select AccountID, CreateTime
                                             from Accounts
                                             WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = it.AccountID
                                       WHERE s.InOut = 3
                                         and st.CategoryId = 1
                                         and it.CreatedTime between :fromDate and :toDate
                                         and cast(a.CreateTime as date) = cast(it.CreatedTime as DATE)
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or it.RoomID = :roomId)
                                         and (:sourceId is null or it.SourceID = :sourceId)
                                       union all
                                       select ot.AccountID,
                                              ot.CreatedTime,
                                              ot.ServiceID,
                                              ot.RoomID,
                                              ot.SourceID,
                                              0         as InAmount,
                                              ot.Amount as OutAmount,
                                              s.InOut
                                       FROM OutputTransactions ot
                                                inner join Services s on s.ServiceID = ot.ServiceID
                                                inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                                inner join
                                            (select AccountID, CreateTime
                                             from Accounts
                                             WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = ot.AccountID
                                       WHERE s.InOut = 1
                                         and st.CategoryId = 1
                                         and ot.CreatedTime between :fromDate and :toDate
                                         and cast(a.CreateTime as date) = cast(ot.CreatedTime as DATE)
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or ot.RoomID = :roomId)
                                         and (:sourceId is null or ot.SourceID = :sourceId)
                                   ) t
                              GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))

                              union all

                              select 0                                              as totalAccount,
                                     0                                              as totalTrans,
                                     0                                              as totalFlow,
                                     0                                              as totalRevenue,
                                     0                                              as totalAccountNew,
                                     0                                              as totalTransNew,
                                     0                                              as totalFlowNew,
                                     sum((t.OutAmount - t.InAmount))                as totalRevenueNew,
                                     dateadd(DAY, 0, datediff(day, 0, CreatedTime)) as datetime
                              from (
                                       select it.AccountID,
                                              it.CreatedTime,
                                              it.ServiceID,
                                              it.RoomID,
                                              it.SourceID,
                                              it.Amount as InAmount,
                                              0         as
                                                           OutAmount,
                                              s.InOut
                                       FROM InputTransactions it
                                                inner join Services s on s.ServiceID = it.ServiceID
                                                inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                                inner join
                                            (select AccountID, CreateTime
                                             from Accounts
                                             WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = it.AccountID

                                       WHERE (s.InOut = 3 or s.InOut = 2)
                                         and st.CategoryId = 1
                                         and it.CreatedTime between :fromDate and :toDate
                                         and cast(a.CreateTime as date) = cast(it.CreatedTime as DATE)
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or it.RoomID = :roomId)
                                         and (:sourceId is null or it.SourceID = :sourceId)

                                       union all
                                       select ot.AccountID,
                                              ot.CreatedTime,
                                              ot.ServiceID,
                                              ot.RoomID,
                                              ot.SourceID,
                                              0         as InAmount,
                                              ot.Amount as OutAmount,
                                              s.InOut
                                       FROM OutputTransactions ot
                                                inner join Services s on s.ServiceID = ot.ServiceID
                                                inner join ServicesType st on s.ServiceType = st.ServicesTypeID
                                                inner join
                                            (select AccountID, CreateTime
                                             from Accounts
                                             WHERE CreateTime between :fromDate and :toDate) as a on a.AccountID = ot.AccountID
                                       WHERE s.InOut = 1
                                         and st.CategoryId = 1
                                         and ot.CreatedTime between :fromDate and :toDate
                                         and cast(a.CreateTime as date) = cast(ot.CreatedTime as DATE)
                                         and (:serviceId is null or s.ServiceType = :serviceId)
                                         and (:roomId is null or ot.RoomID = :roomId)
                                         and (:sourceId is null or ot.SourceID = :sourceId)
                                   ) t
                              GROUP BY dateadd(DAY, 0, datediff(day, 0, CreatedTime))
                          ) a

                     group by a.datetime

                     ##############


                     select sum(ot.Amount) from [Billing.Database].dbo.OutputTransactions ot
inner join [Billing.Database].dbo.Services s on ot.ServiceID = s.ServiceID
inner join [Billing.Database].dbo.ServicesType st on st.CategoryId = 1
where s.InOut = 1 and ot.CreatedTime between :fromDate and :toDate;

select sum(ot.Amount) from [Billing.Database].dbo.InputTransactions ot
                  inner join [Billing.Database].dbo.Services s on ot.ServiceID = s.ServiceID
                  inner join [Billing.Database].dbo.ServicesType st on st.CategoryId = 1
where s.InOut = 3 and ot.CreatedTime between :fromDate and :toDate;

--1364236366850

--50132567690



CREATE FUNCTION GetTimePoints
(
    @timeFrom DATETIME,
    @timeTo DATETIME,
    @intervalMinutes INT
)
    RETURNS TABLE AS RETURN
            (
                WITH C (LastTimeCheck) AS
                         (
                             SELECT @timeFrom
                             UNION ALL
                             SELECT DATEADD(MINUTE, @intervalMinutes, LastTimeCheck)
                             FROM C
                             WHERE LastTimeCheck < @timeTo
                         )
                SELECT
                    LastTimeCheck
                FROM C
                where LastTimeCheck < @timeTo
            )
go




SELECT count(distinct AccountID), A.LastTimeCheck
FROM [Billing.Database].dbo.InputTransactions M
         OUTER APPLY
     (
         SELECT LastTimeCheck
         FROM GetTimePoints(:fromDate, :toDate, :interval) A
         WHERE (datediff (minute, 0, LastTimeCheck) % :interval = 0)
           AND M.CreatedTime <= dateadd (minute, :interval, A.LastTimeCheck)
           AND M.CreatedTime >= DATEADD(minute, -15, A.LastTimeCheck)
     ) A
where M.CreatedTime between :fromDate and :toDate
group by A.LastTimeCheck