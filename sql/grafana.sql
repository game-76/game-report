------------------ VARIABLES-------------------------
select t.label as __text, t.value as __value from (                   select 'WEB' as label, 1 as value                   union all                   select 'APP' as label, 2 as value                   union all                   select 'IOS' as label, 3 as value                   union all                   select 'ANDROID' as label, 4 as value               ) t

select st.ServicesTypeID as __value, st.ServiceName as __text from RP_ServicesType st         where st.CategoryId = 1           and st.ServicesStatus = 1

select distinct s.RoomID         from RP_ServiceRoom s where s.ServiceTypeId  = $serviceTypeID

------------- DAU ----------------
select t.time, COUNT(DISTINCT t.AccountID) as DAU from  (
                                                            SELECT
                                                                DISTINCT
                                                                $__timeGroup(it.CreatedTime, $interval) as time,
                                                                it.AccountID
                                                            FROM
                                                                RP_InputTransactions it
                                                                    inner join RP_Services rs on it.ServiceID = rs.ServiceID
                                                            WHERE
                                                                $__timeFilter(it.CreatedTime)
                                                              and ($sourceID is null or it.SourceID = $sourceID)
                                                              and ($roomID is null or it.RoomID  = $roomID)
                                                              and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
                                                            union

                                                            SELECT
                                                                DISTINCT
                                                                $__timeGroup(it.CreatedTime, $interval) as time,
                                                                it.AccountID
                                                            FROM
                                                                RP_OutputTransactions it
                                                                    inner join RP_Services rs on it.ServiceID = rs.ServiceID
                                                            WHERE
                                                                $__timeFilter(it.CreatedTime)
                                                              and ($sourceID is null or it.SourceID = $sourceID)
                                                              and ($roomID is null or it.RoomID  = $roomID)
                                                              and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
                                                        ) t
group by time
ORDER by time

             --------------- Transaction -----------------------------

SELECT
    count(1) as TotalTrans,
    $__timeGroup(it.CreatedTime, $interval) as time
FROM
    RP_OutputTransactions it
        inner join RP_Services rs on it.ServiceID = rs.ServiceID
        inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
WHERE
        rs.InOut = 1 and rst.CategoryId = 1
  and $__timeFilter(it.CreatedTime)
  and ($sourceID is null or it.SourceID = $sourceID)
  and ($roomID is null or it.RoomID  = $roomID)
  and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
group by $__timeGroup(it.CreatedTime, $interval)
ORDER by time

    ------------ Flow money ----------------------------
select sum((t.OutAmount - t.InAmount)) as FlowMoney,
       t.time
from (
         select sum(it.Amount) as InAmount,
                0 as OutAmount,
                $__timeGroup(it.CreatedTime, $interval) as time
         FROM RP_InputTransactions it
                  inner join RP_Services rs on rs.ServiceID = it.ServiceID
                  inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
         WHERE
                 rs.InOut = 3
           and rst.CategoryId = 1
           and $__timeFilter(it.CreatedTime)
           and ($sourceID is null or it.SourceID = $sourceID)
           and ($roomID is null or it.RoomID  = $roomID)
           and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
         group by $__timeGroup(it.CreatedTime, $interval)

         union all

         select 0 as InAmount,
                sum(it.Amount) as OutAmount,
                $__timeGroup(it.CreatedTime, $interval) as time
         FROM RP_OutputTransactions it
                  inner join RP_Services rs on rs.ServiceID = it.ServiceID
                  inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
         WHERE
                 rs.InOut = 1
           and rst.CategoryId = 1
           and $__timeFilter(it.CreatedTime)
           and ($sourceID is null or it.SourceID = $sourceID)
           and ($roomID is null or it.RoomID  = $roomID)
           and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
         group by $__timeGroup(it.CreatedTime, $interval)
     ) t
group by t.time
order by t.time

             ---------------- Doanh thu -----------------------------------

select sum((t.OutAmount - t.InAmount)) as FlowMoney,
       t.time
from (
         select sum(it.Amount) as InAmount,
                0 as OutAmount,
                $__timeGroup(it.CreatedTime, $interval) as time
         FROM RP_InputTransactions it
                  inner join RP_Services rs on rs.ServiceID = it.ServiceID
                  inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
         WHERE
                 rs.InOut in (3,2)
           and rst.CategoryId = 1
           and $__timeFilter(it.CreatedTime)
           and ($sourceID is null or it.SourceID = $sourceID)
           and ($roomID is null or it.RoomID  = $roomID)
           and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
         group by $__timeGroup(it.CreatedTime, $interval)

         union all

         select 0 as InAmount,
                sum(it.Amount) as OutAmount,
                $__timeGroup(it.CreatedTime, $interval) as time
         FROM RP_OutputTransactions it
                  inner join RP_Services rs on rs.ServiceID = it.ServiceID
                  inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
         WHERE
                 rs.InOut = 1
           and rst.CategoryId = 1
           and $__timeFilter(it.CreatedTime)
           and ($sourceID is null or it.SourceID = $sourceID)
           and ($roomID is null or it.RoomID  = $roomID)
           and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
         group by $__timeGroup(it.CreatedTime, $interval)
     ) t
group by t.time
order by t.time

             --------------- Tổng hơp ---------------------------------------------

select dateadd(s, a.time, '19700101') as 'Ngày tháng',
       sum(a.totalAccount)    as 'SL',
       sum(a.totalTrans)      as 'SGD',
       sum(a.totalFlow)       as 'Luồng tiền',
       sum(a.totalRevenue)    as 'Lợi nhuận',
       sum(a.totalAccountNew) as 'SL (Mới)',
       sum(a.totalTransNew)   as 'SGD (Mới)',
       sum(a.totalFlowNew)    as 'Luồng tiền (Mới)',
       sum(a.totalRevenueNew) as 'Lợi nhuận (Mới)'
from (
         select
             count(distinct t.AccountID) as totalAccount,
             0                           as totalTrans,
             0                           as totalFlow,
             0                           as totalRevenue,
             0                           as totalAccountNew,
             0                           as totalTransNew,
             0                           as totalFlowNew,
             0                           as totalRevenueNew,
             t.time
         from (
                  select distinct it.AccountID, $__timeGroup(it.CreatedTime, '1d') as time
                  FROM RP_InputTransactions it
                           inner join RP_Services rs on it.ServiceID = rs.ServiceID
                           inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                  where $__timeFilter(it.CreatedTime)
                    and ($sourceID is null or it.SourceID = $sourceID)
                    and ($roomID is null or it.RoomID  = $roomID)
                    and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
                  union all
                  select distinct it.AccountID, $__timeGroup(it.CreatedTime, '1d') as time
                  FROM RP_OutputTransactions it
                           inner join RP_Services rs on it.ServiceID = rs.ServiceID
                           inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                  where $__timeFilter(it.CreatedTime)
                    and ($sourceID is null or it.SourceID = $sourceID)
                    and ($roomID is null or it.RoomID  = $roomID)
                    and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
              ) t
         GROUP BY t.time

         union all

         select
             0        as totalAccount,
             count(1) as totalTrans,
             0        as totalFlow,
             0        as totalRevenue,
             0        as totalAccountNew,
             0        as totalTransNew,
             0        as totalFlowNew,
             0        as totalRevenueNew,
             $__timeGroup(it.CreatedTime, '1d') as time
         FROM RP_OutputTransactions it
                  inner join RP_Services rs on it.ServiceID = rs.ServiceID
                  inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
         where $__timeFilter(it.CreatedTime)
           and rst.CategoryId = 1 and rs.InOut = 1
           and ($sourceID is null or it.SourceID = $sourceID)
           and ($roomID is null or it.RoomID  = $roomID)
           and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
         GROUP BY $__timeGroup(it.CreatedTime, '1d')

         union all

         select
             0                               as totalAccount,
             0                               as totalTrans,
             sum((t.OutAmount - t.InAmount)) as totalFlow,
             0                               as totalRevenue,
             0                               as totalAccountNew,
             0                               as totalTransNew,
             0                               as totalFlowNew,
             0                               as totalRevenueNew,
             t.time
         from (
                  select
                      sum(it.Amount) as InAmount,
                      0         as OutAmount,
                      $__timeGroup(it.CreatedTime, '1d') as time
                  FROM RP_InputTransactions it
                           inner join RP_Services rs on it.ServiceID = rs.ServiceID
                           inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                  WHERE rs.InOut = 3
                    and rst.CategoryId = 1
                    and $__timeFilter(it.CreatedTime)
                    and ($sourceID is null or it.SourceID = $sourceID)
                    and ($roomID is null or it.RoomID  = $roomID)
                    and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
                  group by $__timeGroup(it.CreatedTime, '1d')

                  union all
                  select
                      0         as InAmount,
                      sum(it.Amount) as OutAmount,
                      $__timeGroup(it.CreatedTime, '1d') as time
                  FROM RP_OutputTransactions it
                           inner join RP_Services rs on it.ServiceID = rs.ServiceID
                           inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                  WHERE rs.InOut = 1
                    and rst.CategoryId = 1
                    and $__timeFilter(it.CreatedTime)
                    and ($sourceID is null or it.SourceID = $sourceID)
                    and ($roomID is null or it.RoomID  = $roomID)
                    and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
                  group by $__timeGroup(it.CreatedTime, '1d')
              ) t
         GROUP BY t.time


         union all


         select
             0                               as totalAccount,
             0                               as totalTrans,
             0                               as totalFlow,
             sum((t.OutAmount - t.InAmount)) as totalRevenue,
             0                               as totalAccountNew,
             0                               as totalTransNew,
             0                               as totalFlowNew,
             0                               as totalRevenueNew,
             t.time
         from (
                  select
                      sum(it.Amount) as InAmount,
                      0         as
                                        OutAmount,
                      $__timeGroup(it.CreatedTime, '1d') as time
                  FROM RP_InputTransactions it
                           inner join RP_Services rs on it.ServiceID = rs.ServiceID
                           inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                  WHERE (rs.InOut = 3 or rs.InOut = 2)
                    and rst.CategoryId = 1
                    and $__timeFilter(it.CreatedTime)
                    and ($sourceID is null or it.SourceID = $sourceID)
                    and ($roomID is null or it.RoomID  = $roomID)
                    and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
                  group by $__timeGroup(it.CreatedTime, '1d')

                  union all
                  select
                      0         as InAmount,
                      sum(it.Amount) as OutAmount,
                      $__timeGroup(it.CreatedTime, '1d') as time
                  FROM RP_OutputTransactions it
                           inner join RP_Services rs on it.ServiceID = rs.ServiceID
                           inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                  WHERE rs.InOut = 1
                    and rst.CategoryId = 1
                    and $__timeFilter(it.CreatedTime)
                    and ($sourceID is null or it.SourceID = $sourceID)
                    and ($roomID is null or it.RoomID  = $roomID)
                    and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
                  group by $__timeGroup(it.CreatedTime, '1d')
              ) t
         GROUP BY t.time

         union all


         select
             0                           as totalAccount,
             0                           as totalTrans,
             0                           as totalFlow,
             0                           as totalRevenue,
             count(distinct t.AccountID) as totalAccountNew,
             0                           as totalTransNew,
             0                           as totalFlowNew,
             0                           as totalRevenueNew,
             t.time
         from (
                  select distinct it.AccountID, $__timeGroup(it.CreatedTime, '1d') as time
                  FROM RP_InputTransactions it
                           inner join RP_Services rs on it.ServiceID = rs.ServiceID
                           inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                           inner join RP_Accounts ra on ra.AccountID = it.AccountID
                  where $__timeFilter(it.CreatedTime)
                    and $__timeGroup(ra.CreateTime, '1d') = $__timeGroup(it.CreatedTime, '1d')
                    and ($sourceID is null or it.SourceID = $sourceID)
                    and ($roomID is null or it.RoomID  = $roomID)
                    and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)

                  union all

                  select distinct it.AccountID,$__timeGroup(it.CreatedTime, '1d') as time
                  FROM RP_OutputTransactions it
                           inner join RP_Services rs on it.ServiceID = rs.ServiceID
                           inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                           inner join RP_Accounts ra on ra.AccountID = it.AccountID
                  where $__timeFilter(it.CreatedTime)
                    and $__timeGroup(ra.CreateTime, '1d') = $__timeGroup(it.CreatedTime, '1d')
                    and ($sourceID is null or it.SourceID = $sourceID)
                    and ($roomID is null or it.RoomID  = $roomID)
                    and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
              ) t
         GROUP BY t.time

         union all

         select
             0        as totalAccount,
             0        as totalTrans,
             0        as totalFlow,
             0        as totalRevenue,
             0        as totalAccountNew,
             count(1) as totalTransNew,
             0        as totalFlowNew,
             0        as totalRevenueNew,
             $__timeGroup(it.CreatedTime, '1d') as time
         FROM RP_OutputTransactions it
                  inner join RP_Services rs on it.ServiceID = rs.ServiceID
                  inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                  inner join RP_Accounts ra on ra.AccountID = it.AccountID
         where $__timeFilter(it.CreatedTime)
           and $__timeGroup(ra.CreateTime, '1d') = $__timeGroup(it.CreatedTime, '1d')
           and ($sourceID is null or it.SourceID = $sourceID)
           and ($roomID is null or it.RoomID  = $roomID)
           and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
         GROUP BY $__timeGroup(it.CreatedTime, '1d')

         union all
         select
             0                               as totalAccount,
             0                               as totalTrans,
             0                               as totalFlow,
             0                               as totalRevenue,
             0                               as totalAccountNew,
             0                               as totalTransNew,
             sum((t.OutAmount - t.InAmount)) as totalFlowNew,
             0                               as totalRevenueNew,
             t.time
         from (
                  select
                      sum(it.Amount) as InAmount,
                      0         as OutAmount,
                      $__timeGroup(it.CreatedTime, '1d') as time
                  FROM RP_InputTransactions it
                           inner join RP_Services rs on it.ServiceID = rs.ServiceID
                           inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                           inner join RP_Accounts ra on ra.AccountID = it.AccountID
                  WHERE rs.InOut = 3
                    and rst.CategoryId = 1
                    and $__timeFilter(it.CreatedTime)
                    and $__timeGroup(ra.CreateTime, '1d') = $__timeGroup(it.CreatedTime, '1d')
                    and ($sourceID is null or it.SourceID = $sourceID)
                    and ($roomID is null or it.RoomID  = $roomID)
                    and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
                  group by $__timeGroup(it.CreatedTime, '1d')
                  union all
                  select
                      0         as InAmount,
                      sum(it.Amount) as OutAmount,
                      $__timeGroup(it.CreatedTime, '1d') as time
                  FROM RP_OutputTransactions it
                           inner join RP_Services rs on it.ServiceID = rs.ServiceID
                           inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                           inner join RP_Accounts ra on ra.AccountID = it.AccountID
                  WHERE rs.InOut = 1
                    and rst.CategoryId = 1
                    and $__timeFilter(it.CreatedTime)
                    and $__timeGroup(ra.CreateTime, '1d') = $__timeGroup(it.CreatedTime, '1d')
                    and ($sourceID is null or it.SourceID = $sourceID)
                    and ($roomID is null or it.RoomID  = $roomID)
                    and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
                  group by $__timeGroup(it.CreatedTime, '1d')
              ) t
         GROUP BY t.time


         union all

         select
             0                               as totalAccount,
             0                               as totalTrans,
             0                               as totalFlow,
             0                               as totalRevenue,
             0                               as totalAccountNew,
             0                               as totalTransNew,
             0                               as totalFlowNew,
             sum((t.OutAmount - t.InAmount)) as totalRevenueNew,
             t.time
         from (
                  select
                      sum(it.Amount) as InAmount,
                      0         as OutAmount,
                      $__timeGroup(it.CreatedTime, '1d') as time
                  FROM RP_InputTransactions it
                           inner join RP_Services rs on it.ServiceID = rs.ServiceID
                           inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                           inner join RP_Accounts ra on ra.AccountID = it.AccountID
                  WHERE (rs.InOut = 3 or rs.InOut = 2)
                    and rst.CategoryId = 1
                    and $__timeFilter(it.CreatedTime)
                    and $__timeGroup(ra.CreateTime, '1d') = $__timeGroup(it.CreatedTime, '1d')
                    and ($sourceID is null or it.SourceID = $sourceID)
                    and ($roomID is null or it.RoomID  = $roomID)
                    and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
                  group by $__timeGroup(it.CreatedTime, '1d')
                  union all
                  select
                      0         as InAmount,
                      sum(it.Amount) as OutAmount,
                      $__timeGroup(it.CreatedTime, '1d') as time
                  FROM RP_OutputTransactions it
                           inner join RP_Services rs on it.ServiceID = rs.ServiceID
                           inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
                           inner join RP_Accounts ra on ra.AccountID = it.AccountID
                  WHERE rs.InOut = 1
                    and rst.CategoryId = 1
                    and $__timeFilter(it.CreatedTime)
                    and $__timeGroup(ra.CreateTime, '1d') = $__timeGroup(it.CreatedTime, '1d')
                    and ($sourceID is null or it.SourceID = $sourceID)
                    and ($roomID is null or it.RoomID  = $roomID)
                    and ($serviceTypeID is null or rs.ServiceType = $serviceTypeID)
                  group by $__timeGroup(it.CreatedTime, '1d')
              ) t
         GROUP BY t.time
     ) a
group by a.time

             --- Tong hop 2 ---------
select convert(varchar,rts.TimeDate,103) as 'Ngày tháng',
       sum(rts.TotalAccount) as 'SL',
       sum(rts.TotalTrans) as 'SGD',
       sum(rts.TotalFlow) as 'Luồng tiền',
       sum(rts.TotalRevenue) as 'Lợi nhuận',
       sum(rts.TotalAccountNew) as 'SL (Mới)',
       sum(rts.TotalTransNew) as 'SGD (Mới)',
       sum(rts.TotalFlowNew) as 'Luồng tiền (Mới)',
       sum(rts.TotalRevenueNew) as 'Lợi nhuận (Mới)'
from RP_Transaction_Statistic rts
where $__timeFilter(rts.TimeDate)
  and ($sourceID is null or rts.SourceID = $sourceID)
  and ($roomID is null or rts.RoomID  = $roomID)
  and ($serviceTypeID is null or rts.ServiceTypeId = $serviceTypeID)
group by rts.TimeDate

---- charge account ----------


select count(distinct ucl.AccountID) as total,
       ($__timeGroup(ucl.CreatedTime, $interval) + 86400) as time,
       'Hôm qua' as metric
from RP_UserCardLog ucl
WHERE ucl.CreatedTime BETWEEN DATEADD(day,-1,convert(date, current_timestamp)) and convert(date, current_timestamp)
  and ($cardType is null or ucl.Type = $cardType)
  and ($partner is null or ucl.PartnerId = $partner)
  and ($cardValue is null or  ucl.Amount = $cardValue)
  and ($sourceID is null or ucl.SourceID = $sourceID)
GROUP BY ($__timeGroup(ucl.CreatedTime, $interval) + 86400)
order by time