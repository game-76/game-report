--- Nguoi choi nhieu
select it.AccountID,a.UserName, a.UserFullname, m.Mobile, a.TotalVCoin, a.TotalVcoinPayment,
       (select max(itx.CreatedTime) from RP_OutputTransactions itx where itx.AccountID = it.AccountID and
                                                                         itx.CreatedTime between :fromDate and :toDate) as LastPlay,
       count(*) as TotalBet
from RP_OutputTransactions it
         inner join RP_Services RS on it.ServiceID = RS.ServiceID
         inner join RP_ServicesType RST on RS.ServiceType = RST.ServicesTypeID
         inner join [Billing.Database].dbo.Accounts a on it.AccountID = a.AccountID
         left join [Billing.Database].dbo.Mobile m on a.AccountID = m.AccountID
where RST.CategoryId = 1
  and RS.InOut = 1
  and it.CreatedTime between :fromDate and :toDate
group by it.AccountID, a.UserName, a.UserFullname, m.Mobile, a.TotalVCoin, a.TotalVcoinPayment
order by TotalBet desc;

--- Người chơi thắng nhiều

select t.AccountID                        as AccountID,
       a.UserName, a.UserFullname, a.TotalVCoin, a.TotalVcoinPayment,
       m.Mobile,
       (select max(itx.CreatedTime) from RP_OutputTransactions itx where itx.AccountID = t.AccountID and
           itx.CreatedTime between :fromDate and :toDate) as LastPlay,
       sum(t.ReturnAmount - t.PlayAmount) as TotalAmount
from (
         select ri.AccountID,
                sum(ri.Amount) as ReturnAmount,
                0              as PlayAmount
         from RP_InputTransactions ri
                  inner join RP_Services rs on ri.ServiceID = rs.ServiceID
                  inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
         where rs.InOut in (2, 3)
           and rst.CategoryId = 1
           and ri.CreatedTime between :fromDate and :toDate
         group by ri.AccountID
         union all
         select ri.AccountID,
                0              as ReturnAmount,
                sum(ri.Amount) as PlayAmount
         from RP_OutputTransactions ri
                  inner join RP_Services rs on ri.ServiceID = rs.ServiceID
                  inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
         where rs.InOut = 1
           and rst.CategoryId = 1
           and ri.CreatedTime between :fromDate and :toDate
         group by ri.AccountID
     ) t
inner join [Billing.Database].dbo.Accounts a on a.AccountID = t.AccountID
left join [Billing.Database].dbo.Mobile m on a.AccountID = m.AccountID
group by t.AccountID, a.UserName, t.AccountID, a.UserFullname, a.TotalVCoin, a.TotalVcoinPayment, m.Mobile
having sum(t.ReturnAmount - t.PlayAmount) > 0
order by TotalAmount desc

--- Người chơi thua nhiều

select t.AccountID                        as AccountID,
       a.UserName, a.UserFullname, a.TotalVCoin, a.TotalVcoinPayment,
       m.Mobile,
       (select max(itx.CreatedTime) from RP_OutputTransactions itx where itx.AccountID = t.AccountID and
           itx.CreatedTime between :fromDate and :toDate) as LastPlay,
       sum(t.ReturnAmount - t.PlayAmount) as TotalAmount
from (
         select ri.AccountID,
                sum(ri.Amount) as ReturnAmount,
                0              as PlayAmount
         from RP_InputTransactions ri
                  inner join RP_Services rs on ri.ServiceID = rs.ServiceID
                  inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
         where rs.InOut in (2, 3)
           and rst.CategoryId = 1
           and ri.CreatedTime between :fromDate and :toDate
         group by ri.AccountID
         union all
         select ri.AccountID,
                0              as ReturnAmount,
                sum(ri.Amount) as PlayAmount
         from RP_OutputTransactions ri
                  inner join RP_Services rs on ri.ServiceID = rs.ServiceID
                  inner join RP_ServicesType rst on rs.ServiceType = rst.ServicesTypeID
         where rs.InOut = 1
           and rst.CategoryId = 1
           and ri.CreatedTime between :fromDate and :toDate
         group by ri.AccountID
     ) t
         inner join [Billing.Database].dbo.Accounts a on a.AccountID = t.AccountID
         left join [Billing.Database].dbo.Mobile m on a.AccountID = m.AccountID
group by t.AccountID, a.UserName, t.AccountID, a.UserFullname, a.TotalVCoin, a.TotalVcoinPayment, m.Mobile
having sum(t.ReturnAmount - t.PlayAmount) < 0
order by TotalAmount desc

---- Người bỏ chơi ------------
select a.AccountID,
       a.UserName,
       a.UserFullname,
       a.TotalVCoin,
       a.TotalVcoinPayment,
       m.Mobile,
       (select max(itx.CreatedTime)
        from RP_OutputTransactions itx
        where itx.AccountID = a.AccountID
          and itx.CreatedTime < :fromDate) as LastPlay
from [Billing.Database].dbo.Accounts a
         left join [Billing.Database].dbo.Mobile m on a.AccountID = m.AccountID
where not exists(select distinct it.AccountID
                 from RP_OutputTransactions it
                 where it.CreatedTime between :fromDate and :toDate
                   and a.AccountID = it.AccountID)
order by UserName


------- Nguoi choi khong phat sinh giao dich IAU -------------
--- User đã phát sinh giao dịch Bon/Xu , tuy nhiên sau đó không truy cập và phát sinh giao dịch nào trong khoảng thời gian tùy biến, ví dụ: 1 ngày, 3 ngày, 1 tuần,….
select
    a.AccountID,
    a.UserName,
    a.UserFullname,
    a.TotalVCoin,
    a.TotalVcoinPayment,
    m.Mobile,
    (select max(itx.CreatedTime)
     from RP_OutputTransactions itx
     where itx.AccountID = a.AccountID
       and itx.CreatedTime < :fromDate) as LastPlay
from (
                  select distinct it.AccountID
                  from RP_InputTransactions it
                  union
                  select distinct it.AccountID
                  from RP_OutputTransactions it
              ) t
inner join [Billing.Database].dbo.Accounts a on a.AccountID = t.AccountID
left join [Billing.Database].dbo.Mobile m on a.AccountID = m.AccountID
where not exists(select distinct it.AccountID
                 from RP_OutputTransactions it
                 where it.CreatedTime between :fromDate and :toDate
                   and t.AccountID = it.AccountID)



----------------


select sum(a.NRU)          as NRU,
       (case sum(NAU) when 0 then 0 else round(sum(NRU) * 100.0 / sum(NAU), 0) end) as NRUPERNAU,
       sum(NAU)            as NAU,
       (case sum(NAU) when 0 then 0 else sum(PU) / sum(NAU) end) as PayRate,
       sum(PU)             as PU,
       (case sum(PU) when 0 then 0 else sum(REV) / sum(PU) end) as ARPPU,
       sum(REV)            as REV,
       (case sum(NAU) when 0 then 0 else sum(REV) / sum(NAU) end) as ARPU,
       a.MerchantID as CampaignSource
from (
-- NRU
         select count(distinct t.AccountID) as NRU,
                0                           as NAU,
                0                           as PU,
                0                           as REV,
                t.MerchantID
         from (
                  select it.AccountID, a.MerchantID
                  FROM RP_Account_New it
                           inner join [Billing.Database].dbo.Accounts a on it.AccountID = a.AccountID
                  WHERE it.FirstLogin BETWEEN :fromDate AND :toDate
              ) t
         GROUP BY t.MerchantID

         union all
-- NAU
         select 0                           as NRU,
                count(distinct t.AccountID) as NAU,
                0                           as PU,
                0                           as REV,
                t.MerchantID
         from (
                  select distinct it.AccountID, it.MerchantID
                  FROM RP_InputTransactions it
                           inner join RP_Services s on it.ServiceID = s.ServiceID
                           inner join RP_ServicesType st on s.ServiceType = st.ServicesTypeID
                  where CreatedTime BETWEEN :fromDate AND :toDate
                    and st.CategoryId = 1
                  union
                  select distinct it.AccountID, it.MerchantID
                  FROM RP_OutputTransactions it
                           inner join RP_Services s on it.ServiceID = s.ServiceID
                           inner join RP_ServicesType st on s.ServiceType = st.ServicesTypeID
                  where CreatedTime BETWEEN :fromDate AND :toDate
                    and st.CategoryId = 1
              ) t
         group by t.MerchantID

         union all
-- PU
         select 0                            as NRU,
                0                            as NAU,
                count(distinct it.AccountID) as PU,
                0                            as REV,
                it.MerchantID
         FROM RP_InputTransactions it
                  inner join RP_Services s on it.ServiceID = s.ServiceID
                  inner join RP_ServicesType st on s.ServiceType = st.ServicesTypeID
         where CreatedTime BETWEEN :fromDate AND :toDate
           and st.ServicesTypeID in (36, 37, 38)
         group by it.MerchantID

         union all
--- Rev
         select 0                               as NRU,
                0                               as NAU,
                0                               as PU,
                sum((t.OutAmount - t.InAmount)) as REV,
                t.MerchantID
         from (
                  select sum(it.Amount) as InAmount,
                         0              as OutAmount,
                         it.MerchantID
                  FROM RP_InputTransactions it
                           inner join RP_Services s on s.ServiceID = it.ServiceID
                           inner join RP_ServicesType st on s.ServiceType = st.ServicesTypeID
                  WHERE CreatedTime BETWEEN :fromDate AND :toDate
                    and (s.InOut = 3 or s.InOut = 2)
                    and st.CategoryId = 1
                  group by it.MerchantID
                  union all
                  select 0              as InAmount,
                         sum(it.Amount) as OutAmount,
                         it.MerchantID
                  FROM RP_OutputTransactions it
                           inner join RP_Services s on s.ServiceID = it.ServiceID
                           inner join RP_ServicesType st on s.ServiceType = st.ServicesTypeID
                  WHERE it.CreatedTime BETWEEN :fromDate AND :toDate
                    and s.InOut = 1
                    and st.CategoryId = 1
                  group by it.MerchantID
              ) t
         GROUP BY t.MerchantID
     ) a
group by a.MerchantID











