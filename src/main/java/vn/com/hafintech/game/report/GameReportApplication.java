package vn.com.hafintech.game.report;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameReportApplication {

    public static void main(String[] args) {
        SpringApplication.run(GameReportApplication.class, args);
    }

}
