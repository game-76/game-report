package vn.com.hafintech.game.report.constant;

public interface ReportConstant {

    int MAX_MONTH_REPORT = 3;
    int MAX_TIME_OFFLINE_MINUTES = 10;

}
