package vn.com.hafintech.game.report.constant.enums;

public enum SourceType {
    WEB(1, "Web"), APP(2, "App"), IOS(3, "IOS"), ANDROID(4, "Android");

    private final int type;
    private final String name;

    SourceType(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
