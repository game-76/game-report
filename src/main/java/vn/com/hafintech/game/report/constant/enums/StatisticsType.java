package vn.com.hafintech.game.report.constant.enums;

public enum  StatisticsType {
    /**
     * Số tài khoản phát sinh giao dịch trong ngày (tất cả các hành động làm thay đổi số dư)
     */
    DAU(0),
    /**
     * Đây là số giao dịch phát sinh theo thời gian lựa chọn. Ví dụ: giao dịch phát sinh theo ngày/giờ
     */
    SO_GIAO_DICH(1),
    /**
     * Luồng tiền = Tổng đặt = Tổng thắng chưa nhân với 98%
     */
    LUONG_TIEN(2),
    /**
     * Doanh thu =Lợi nhuận = ∑ đặt - ∑ trả - ∑ hoàn
     */
    DOANH_THU(3);

    private final int type;

    StatisticsType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
