package vn.com.hafintech.game.report.constant.enums;

public enum  TimeType {

    THEO_NGAY(0), THEO_PHUT(1);

    private final int type;

    TimeType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
