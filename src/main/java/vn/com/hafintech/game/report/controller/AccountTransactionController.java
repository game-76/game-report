package vn.com.hafintech.game.report.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.com.hafintech.game.report.model.search.AccountBalanceHistorySearch;
import vn.com.hafintech.game.report.model.search.AccountWinHistorySearch;
import vn.com.hafintech.game.report.model.search.TopWinLossSearch;
import vn.com.hafintech.game.report.service.RpAccountsService;
import vn.com.hafintech.game.report.service.TransactionService;
import vn.com.hafintech.game.report.util.R;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountTransactionController {

    private final TransactionService transactionService;
    private final RpAccountsService accountsService;

    @GetMapping("/transaction/top")
    public R<?> findTopTransaction(@Validated @NotNull @RequestParam("top") int top, @Validated @NotNull @RequestParam("accountId") Integer accountId) {
        return R.ok(transactionService.findTopTransaction(accountId, top));
    }


    @GetMapping("/balance-history")
    public R<?> findAllAccountBalanceHistory(AccountBalanceHistorySearch accountBalanceHistorySearch) {
        return R.ok(accountsService.findAllAccountBalanceHistory(accountBalanceHistorySearch));
    }

    @GetMapping("/top-win-loss")
    public R<?> findAllTopWinLoss(TopWinLossSearch topWinLossSearch) {
        return R.ok(accountsService.findAllTopWinLoss(topWinLossSearch));
    }

    @GetMapping("/no-hu")
    public R<?> findAllNoHu(AccountWinHistorySearch accountWinHistorySearch) {
        return R.ok(accountsService.findAllNoHu(accountWinHistorySearch));
    }

    @GetMapping("/win-history")
    public R<?> findAllWinHistory(AccountWinHistorySearch accountWinHistorySearch) {
        return R.ok(accountsService.findAllWin(accountWinHistorySearch));
    }

}
