package vn.com.hafintech.game.report.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import vn.com.hafintech.game.report.entity.RpCampaign;
import vn.com.hafintech.game.report.service.RpCampaignService;
import vn.com.hafintech.game.report.util.R;

@RestController
@RequestMapping("/campaign")
@RequiredArgsConstructor
public class CampaignController {

    private final RpCampaignService campaignService;

    @GetMapping
    public R<?> findAllCampaign(RpCampaign rpCampaign) {
        return R.ok(campaignService.list(new QueryWrapper<>(rpCampaign)));
    }

    @PostMapping
    public R<?> saveRpCampaign(@RequestBody RpCampaign campaign) {
        campaignService.saveCampaign(campaign);
        return R.ok();
    }

    @PutMapping
    public R<?> updateCampaign(@RequestBody RpCampaign campaign) {
        campaignService.updateCampaign(campaign);
        return R.ok();
    }

    @DeleteMapping
    public R<?> deleteCampaign(@RequestBody Integer id) {
        campaignService.deleteRpCampaign(id);
        return R.ok();
    }

}
