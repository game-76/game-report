package vn.com.hafintech.game.report.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.com.hafintech.game.report.model.BaseStatistics;
import vn.com.hafintech.game.report.model.search.ChargeStatisticSearchInterval;
import vn.com.hafintech.game.report.model.search.ChargeStatisticSearchPage;
import vn.com.hafintech.game.report.service.CashoutStatisticService;
import vn.com.hafintech.game.report.util.R;

import java.util.List;

@RestController
@RequestMapping("/cashout-statistic")
@RequiredArgsConstructor
public class CashoutStatisticController {

    private final CashoutStatisticService cashoutStatisticService;

    @GetMapping("/cashout-account")
    public R<?> findAllCashoutAccountByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        List<BaseStatistics> baseStatistics = cashoutStatisticService.findAllCashoutAccountByIntervalMinute(searchInterval);

        return R.ok(cashoutStatisticService.findAllCashoutAccountByIntervalMinute(searchInterval));
    }

    @GetMapping("/cashout-trans")
    public R<?> findAllCashoutTransactionByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return R.ok(cashoutStatisticService.findAllCashoutTransactionByIntervalMinute(searchInterval));
    }

    @GetMapping("/cashout-amount")
    public R<?> findAllCashoutAmountByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return R.ok(cashoutStatisticService.findAllCashoutAccountByIntervalMinute(searchInterval));
    }

    @GetMapping("/cashout-j")
    public R<?> findAllCashoutJByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return R.ok(cashoutStatisticService.findAllCashoutJByIntervalMinute(searchInterval));
    }

    @GetMapping
    public R<?> findAllCashoutStatistic(ChargeStatisticSearchPage statisticSearch) {
        return R.ok(cashoutStatisticService.findAllCashoutStatistic(statisticSearch));
    }

    @GetMapping("/by-card-value")
    public R<?> findAllCashoutStatisticByCardValue(ChargeStatisticSearchPage statisticSearch) {
        return R.ok(cashoutStatisticService.findAllCashoutStatisticByCardValue(statisticSearch));
    }

    @GetMapping("/by-card-type")
    public R<?> findAllCashoutStatisticByCardType(ChargeStatisticSearchPage statisticSearch) {
        return R.ok(cashoutStatisticService.findAllCashoutStatisticByCardType(statisticSearch));
    }
}
