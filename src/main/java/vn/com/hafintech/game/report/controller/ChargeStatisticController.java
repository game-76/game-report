package vn.com.hafintech.game.report.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.com.hafintech.game.report.model.search.ChargeStatisticSearchInterval;
import vn.com.hafintech.game.report.model.search.ChargeStatisticSearchPage;
import vn.com.hafintech.game.report.service.ChargeStatisticService;
import vn.com.hafintech.game.report.util.R;

@RestController
@RequestMapping("/charge-statistic")
@RequiredArgsConstructor
public class ChargeStatisticController {

    private final ChargeStatisticService chargeStatisticService;

    @GetMapping("/partner")
    public R<?> findAllPartner(@RequestParam(value = "isTopup", required = false) Integer isTopup) {
        return R.ok(chargeStatisticService.findAllPartner(isTopup));
    }

    @GetMapping("/charge-account")
    public R<?> findAllChargeAccountByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return R.ok(chargeStatisticService.findAllChargeAccountByIntervalMinute(searchInterval));
    }

    @GetMapping("/charge-trans")
    public R<?> findAllChargeTransactionByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return R.ok(chargeStatisticService.findAllChargeTransactionByIntervalMinute(searchInterval));
    }

    @GetMapping("/charge-amount")
    public R<?> findAllChargeAmountByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return R.ok(chargeStatisticService.findAllChargeAccountByIntervalMinute(searchInterval));
    }

    @GetMapping("/charge-j")
    public R<?> findAllChargeJByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return R.ok(chargeStatisticService.findAllChargeJByIntervalMinute(searchInterval));
    }

    @GetMapping
    public R<?> findAllReportStatistic(ChargeStatisticSearchPage statisticSearch) {
        return R.ok(chargeStatisticService.findAllReportStatistic(statisticSearch));
    }

    @GetMapping("/by-card-value")
    public R<?> findAllChargeStatisticByCardValue(ChargeStatisticSearchPage statisticSearch) {
        return R.ok(chargeStatisticService.findAllChargeStatisticByCardValue(statisticSearch));
    }

    @GetMapping("/by-card-type")
    public R<?> findAllChargeStatisticByCardType(ChargeStatisticSearchPage statisticSearch) {
        return R.ok(chargeStatisticService.findAllChargeStatisticByCardType(statisticSearch));
    }
}
