package vn.com.hafintech.game.report.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.com.hafintech.game.report.entity.RpGameBank;
import vn.com.hafintech.game.report.mapper.RpGameBankStatisticMapper;
import vn.com.hafintech.game.report.service.RpGameBankService;
import vn.com.hafintech.game.report.service.RpGameBankStatisticService;
import vn.com.hafintech.game.report.util.R;

@RestController
@RequestMapping("/game-bank")
@RequiredArgsConstructor
public class GameBankStatisticController {

    private final RpGameBankStatisticService gameBankStatisticService;
    private final RpGameBankService gameBankService;

    private final RpGameBankStatisticMapper gameBankStatisticMapper;

    @GetMapping
    public R<?> findAllGameBank() {
        return R.ok(gameBankService.list(new QueryWrapper<RpGameBank>().lambda().eq(RpGameBank::getStatus, 1)));
    }

    @GetMapping("/statistic")
    public R<?> findAllGameBankStatistic(@RequestParam(value = "serviceTypeId", required = false) Integer serviceTypeId,
                                         @RequestParam(value = "roomID", required = false) Integer roomId) {
        return R.ok(gameBankStatisticService.findAllGameBankStatisticInfo(serviceTypeId, roomId));
    }

    @GetMapping("/find-room")
    public R<?> findAllRoomByServiceTypeId(@RequestParam(value = "serviceTypeId") Integer serviceTypeId) {
        return R.ok(gameBankStatisticMapper.findAllServiceRoom(serviceTypeId));
    }

}
