package vn.com.hafintech.game.report.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import vn.com.hafintech.game.report.exception.HaftApiException;
import vn.com.hafintech.game.report.util.R;

import java.util.List;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandlerResolver {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public R handleGlobalException(Exception e) {
        log.error("Global exception information ex={}", e.getMessage(), e);
        return R.failed(e.getLocalizedMessage());
    }

    @ExceptionHandler(HaftApiException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public R handleApiException(HaftApiException e) {
        log.error(e.getMessage());
        return new R()
                .setMsg(e.getMessage())
                .setCode(e.getCode());
    }

    /**
     * validation Exception
     *
     * @param exception
     * @return R
     */
    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public R handleBodyValidException(Exception exception) {
        List<FieldError> fieldErrors;
        if (exception instanceof MethodArgumentNotValidException) {
            fieldErrors = ((MethodArgumentNotValidException) exception).getBindingResult().getFieldErrors();
        } else {
            fieldErrors = ((BindException) exception).getBindingResult().getFieldErrors();
        }
        FieldError fieldError = fieldErrors.get(0);
        String message = fieldError == null ? exception.getMessage() : fieldError.getField() + " " + fieldError.getDefaultMessage();
        log.error("Parameter binding exception, ex = {}", message, exception);
        return R.failed(message);
    }
}
