package vn.com.hafintech.game.report.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.com.hafintech.game.report.model.search.MarketStatisticSearch;
import vn.com.hafintech.game.report.service.MarketStatisticService;
import vn.com.hafintech.game.report.util.R;

@RestController
@RequestMapping("/market-statistic")
@RequiredArgsConstructor
public class MarketStatisticController {

    private final MarketStatisticService marketStatisticService;

    @GetMapping("/dau-service")
    public R<?> findAllDAUMarketStatistic(MarketStatisticSearch statisticSearch) {
        return R.ok(marketStatisticService.findAllDAUMarketStatistic(statisticSearch.getFromDate(), statisticSearch.getToDate()));
    }

    @GetMapping("/trans-service")
    public R<?> findAllTransactionMarketStatistic(MarketStatisticSearch statisticSearch) {
        return R.ok(marketStatisticService.findAllTransactionMarketStatistic(statisticSearch.getFromDate(), statisticSearch.getToDate()));
    }

    @GetMapping("/flow-money-service")
    public R<?> findAllFlowMoneyMarketStatistic(MarketStatisticSearch statisticSearch) {
        return R.ok(marketStatisticService.findAllFlowMoneyMarketStatistic(statisticSearch.getFromDate(), statisticSearch.getToDate()));
    }

    @GetMapping("/revenue-service")
    public R<?> findAllRevenueMarketStatistic(MarketStatisticSearch statisticSearch) {
        return R.ok(marketStatisticService.findAllRevenueMarketStatistic(statisticSearch.getFromDate(), statisticSearch.getToDate()));
    }


    @GetMapping("/dau-source")
    public R<?> findAllDAUMarketStatisticBySource(MarketStatisticSearch statisticSearch) {
        return R.ok(marketStatisticService.findAllDAUMarketStatisticBySource(statisticSearch.getFromDate(), statisticSearch.getToDate()));
    }

    @GetMapping("/trans-source")
    public R<?> findAllTransactionMarketStatisticBySource(MarketStatisticSearch statisticSearch) {
        return R.ok(marketStatisticService.findAllTransactionMarketStatisticBySource(statisticSearch.getFromDate(), statisticSearch.getToDate()));
    }

    @GetMapping("/flow-money-source")
    public R<?> findAllFlowMoneyMarketStatisticBySource(MarketStatisticSearch statisticSearch) {
        return R.ok(marketStatisticService.findAllFlowMoneyMarketStatisticBySource(statisticSearch.getFromDate(), statisticSearch.getToDate()));
    }

    @GetMapping("/revenue-source")
    public R<?> findAllRevenueMarketStatisticBySource(MarketStatisticSearch statisticSearch) {
        return R.ok(marketStatisticService.findAllRevenueMarketStatisticBySource(statisticSearch.getFromDate(), statisticSearch.getToDate()));
    }

    @GetMapping("/charge-account")
    public R<?> findAllChargeAccountMarketStatistic(MarketStatisticSearch statisticSearch) {
        return R.ok(marketStatisticService.findAllChargeAccountMarketStatistic(statisticSearch.getFromDate(), statisticSearch.getToDate()));
    }

    @GetMapping("/charge-trans")
    public R<?> findAllChargeTransactionMarketStatistic(MarketStatisticSearch statisticSearch) {
        return R.ok(marketStatisticService.findAllChargeTransactionMarketStatistic(statisticSearch.getFromDate(), statisticSearch.getToDate()));
    }

    @GetMapping("/charge-j")
    public R<?> findAllChargeJMarketStatistic(MarketStatisticSearch statisticSearch) {
        return R.ok(marketStatisticService.findAllChargeJMarketStatistic(statisticSearch.getFromDate(), statisticSearch.getToDate()));
    }

}
