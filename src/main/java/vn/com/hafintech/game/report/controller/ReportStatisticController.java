package vn.com.hafintech.game.report.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.com.hafintech.game.report.model.search.ReportStatisticSearch;
import vn.com.hafintech.game.report.model.search.TableStatisticSearch;
import vn.com.hafintech.game.report.service.ReportStatisticService;
import vn.com.hafintech.game.report.util.R;

@RestController
@RequestMapping("/statistic")
@RequiredArgsConstructor
public class ReportStatisticController {

    private final ReportStatisticService reportStatisticService;

    @GetMapping("/service-type")
    public R<?> findAllServiceTypeGame() {
        return R.ok(reportStatisticService.findAllServiceTypeGame());
    }

    @GetMapping("/room-trans")
    public R<?> findAllRoomService(@RequestParam("serviceTypeId") Integer serviceTypeId) {
        return R.ok(reportStatisticService.findAllRoomService(serviceTypeId));
    }

    @GetMapping("/dau")
    public R<?> findAllDAUbyIntervalMinute(ReportStatisticSearch reportStatisticSearch) {
        return R.ok(reportStatisticService.findAllDAUbyIntervalMinute(reportStatisticSearch));
    }

    @GetMapping("/transaction")
    public R<?> findAllTransactionbyIntervalMinute(ReportStatisticSearch reportStatisticSearch) {
        return R.ok(reportStatisticService.findAllTransactionbyIntervalMinute(reportStatisticSearch));
    }

    @GetMapping("/flow-money")
    public R<?> findAllFlowMoneyByIntervalMinute(ReportStatisticSearch reportStatisticSearch) {
        return R.ok(reportStatisticService.findAllFlowMoneyByIntervalMinute(reportStatisticSearch));
    }

    @GetMapping("/revenue")
    public R<?> findAllRevenueByIntervalMinute(ReportStatisticSearch reportStatisticSearch) {
        return R.ok(reportStatisticService.findAllRevenueByIntervalMinute(reportStatisticSearch));
    }

    @GetMapping("/table-summary")
    public R<?> findAllReportStatisticsSummary(TableStatisticSearch tss) {
        return R.ok(reportStatisticService.findAllReportStatisticsSummaryList(tss));
    }

    @GetMapping("/table-details")
    public R<?> findAllReportStatisticsDetails(TableStatisticSearch tss) {
        return R.ok(reportStatisticService.findAllReportStatisticsDetailsList(tss));
    }

}
