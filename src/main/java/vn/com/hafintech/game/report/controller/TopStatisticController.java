package vn.com.hafintech.game.report.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.com.hafintech.game.report.model.search.*;
import vn.com.hafintech.game.report.service.RpAccountsService;
import vn.com.hafintech.game.report.service.TranferAgencyService;
import vn.com.hafintech.game.report.util.R;

@RestController
@RequestMapping("/top-statistic")
@RequiredArgsConstructor
public class TopStatisticController {

    private final RpAccountsService accountsService;
    private final TranferAgencyService transferAgencyService;

    @GetMapping("/account-balance")
    public R<?> topAccountBalance(@Validated TopAccountBalanceSearch search) {
        return R.ok(accountsService.findTopAccountBalance(search));
    }

    @GetMapping("/account-transfer")
    public R<?> topAccountTransfer(@Validated TopTransferUserAgencySearch search) {
        return R.ok(transferAgencyService.topTransferAgency(search));
    }

    @GetMapping("/account-transfer-details")
    public R<?> topAccountTransferDetails(@Validated TopTransferUserAgencySearch search) {
        return R.ok(transferAgencyService.topTransferAgencyDetails(search));
    }

    @GetMapping("/account-balance-history")
    public R<?> findAllAccountBalanceHistory(AccountBalanceHistorySearch search) {
        return R.ok(accountsService.findAllAccountBalanceHistory(search));
    }

    @GetMapping("/top-win-loss")
    public R<?> findAllTopWinLoss(TopWinLossSearch search) {
        return R.ok(accountsService.findAllTopWinLoss(search));
    }

    @GetMapping("/top-no-hu")
    public R<?> findAllNoHu(AccountWinHistorySearch search) {
        return R.ok(accountsService.findAllNoHu(search));
    }

    @GetMapping("/top-win")
    public R<?> findAllWin(AccountWinHistorySearch search) {
        return R.ok(accountsService.findAllWin(search));
    }

    @GetMapping("/top-win-loss-details")
    public R<?> findAllTopWinLossDetailsByService(TopWinLossSearch search) {
        return R.ok(accountsService.findAllServiceWinLossDetails(search));
    }

}
