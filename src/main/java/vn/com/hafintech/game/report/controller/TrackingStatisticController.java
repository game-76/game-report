package vn.com.hafintech.game.report.controller;


import com.baomidou.mybatisplus.extension.api.R;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.com.hafintech.game.report.model.search.BaseDateSearch;
import vn.com.hafintech.game.report.model.search.TrackingSearch;
import vn.com.hafintech.game.report.model.search.TrackingWinLossSearch;
import vn.com.hafintech.game.report.service.TrackingService;

@RestController
@RequestMapping("/tracking-statistic")
@RequiredArgsConstructor
public class TrackingStatisticController {

    private final TrackingService trackingService;

    @GetMapping("/account-play-more")
    public R<?> findAllAccountPlayMore(BaseDateSearch search) {
        return R.ok(trackingService.findAllAccountPlayMore(search));
    }

    @GetMapping("/account-win-loss")
    public R<?> findAllAccountWinLoss(TrackingWinLossSearch search) {
        return R.ok(trackingService.findAllAccountWinLoss(search));
    }

    @GetMapping("/account-get-out")
    public R<?> findAllAccountGetOut(BaseDateSearch search) {
        return R.ok(trackingService.findAllAccountGetOut(search));
    }

    @GetMapping("/iau")
    public R<?> findAllIAU(BaseDateSearch search) {
        return R.ok(trackingService.findAllIAU(search));
    }

    @GetMapping("/nru-daily")
    public R<?> findAllNruDaily(TrackingSearch ts) {
        return R.ok(trackingService.findAllNruDaily(ts));
    }

    @GetMapping("/au-daily")
    public R<?> findAllAuDaily(TrackingSearch ts) {
        return R.ok(trackingService.findAllAuDaily(ts));
    }

    @GetMapping("/cbu")
    public R<?> findAllCbuDaily(TrackingSearch ts) {
        return R.ok(trackingService.findAllCbuDaily(ts));
    }

    @GetMapping("/pu-daily")
    public R<?> findAllPUDaily(TrackingSearch ts) {
        return R.ok(trackingService.findAllPUDaily(ts));
    }

    @GetMapping("/rev-daily")
    public R<?> findAllRevenueDaily(TrackingSearch ts) {
        return R.ok(trackingService.findAllRevenueDaily(ts));
    }

    @GetMapping("/arppu-daily")
    public R<?> findAllARPPUDaily(TrackingSearch ts) {
        return R.ok(trackingService.findAllARPPUDaily(ts));
    }

    @GetMapping("/user-info")
    public R<?> findAllTrackingUserInfo(TrackingSearch ts) {
        return R.ok(trackingService.findAllTrackingUserInfo(ts));
    }

    @GetMapping("/marketing")
    public R<?> findAllTrackingMarketing(TrackingSearch ts) {
        return R.ok(trackingService.findAllTrackingMarketing(ts));
    }

    @GetMapping("/user-campaign")
    public R<?> findAllTrackingUserService(TrackingSearch ts) {
        return R.ok(trackingService.findAllTrackingUserService(ts));
    }
}
