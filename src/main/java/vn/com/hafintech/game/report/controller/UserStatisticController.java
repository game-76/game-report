package vn.com.hafintech.game.report.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.com.hafintech.game.report.model.search.*;
import vn.com.hafintech.game.report.service.ReportUserStatisticService;
import vn.com.hafintech.game.report.service.RpDAUStatisticService;
import vn.com.hafintech.game.report.util.R;

@RestController
@RequestMapping("/user-statistic")
@RequiredArgsConstructor
public class UserStatisticController {

    private final ReportUserStatisticService userStatisticService;
    private final RpDAUStatisticService rpDAUStatisticService;

    @GetMapping("/table-dau")
    public R<?> findAllReportUserDauStatistic(ReportStatisticSearch search) {
        return R.ok(userStatisticService.findAllUserDauBySource(search));
    }


    @GetMapping("/chart-ccu")
    public R<?> findAllCCU(CCUStatisticSearch search) {
        return R.ok(userStatisticService.findAllCCU(search));
    }

    @GetMapping("/chart-nru")
    public R<?> findAllNRUByMinute(ReportStatisticSearch search) {
        return R.ok(userStatisticService.findAllNRUByMinute(search));
    }

    @GetMapping("/chart-peek-ccu")
    public R<?> findAllPeekCCUByMinute(ReportStatisticSearch search) {
        return R.ok(userStatisticService.findAllPeekCCU(search));
    }

    @GetMapping("/chart-dpu")
    public R<?> findAllDPUByMinute(ReportStatisticSearch search) {
        return R.ok(userStatisticService.findAllDPUByMinute(search));
    }

    @GetMapping("/nru-table")
    public R<?> findAllNruTable(TableStatisticSearch search) {
        return R.ok(userStatisticService.findAllNRUBySource(search));
    }

    @GetMapping("/nru-pie")
    public R<?> findAllNruPieChart(MarketStatisticSearch search) {
        return R.ok(userStatisticService.findAllNRUMarketStatisticBySource(search.getFromDate(), search.getToDate()));
    }

    @GetMapping("/dau-rate")
    public R<?> findAllDAUStatistic(BaseDateSearch search) {
        return R.ok(rpDAUStatisticService.findAllRpDauStatistic(search));
    }

    @GetMapping("/table")
    public R<?> findAllUserStatisticTable(TableStatisticSearch search) {
        return R.ok(userStatisticService.findAllRpUserStatistic(search));
    }


}
