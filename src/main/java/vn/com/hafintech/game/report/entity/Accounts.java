package vn.com.hafintech.game.report.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author Mr.007
 * @since 2019-08-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("[TO_BILLINGDATABASE].[Billing.Database].dbo.Accounts")
public class Accounts extends Model<Accounts> {

	private static final long serialVersionUID = 1L;

	@TableId(value = "AccountID", type = IdType.INPUT)
	private Long accountID;

	@TableField("PaygateID")
	private Integer paygateID;

	@TableField("UserName")
	private String userName;

	@TableField("UserPassword")
	private String userPassword;

	@TableField("UserFullname")
	private String userFullname;

	@TableField("UserJoined")
	private Integer userJoined;

	@TableField("UserEmail")
	private String userEmail;

	@TableField("UserBirthday")
	private LocalDateTime userBirthday;

	@TableField("LocationID")
	private Integer locationID;

	@TableField("UserPassport")
	private String userPassport;

	@TableField("UserStatus")
	private Integer userStatus;

	@TableField("UserConfirm")
	private Integer userConfirm;

	@TableField("Mobile")
	private String mobile;

	@TableField("Gender")
	private Integer gender;

	@TableField("Address")
	private String address;

	@TableField("PassLevel2")
	private String passLevel2;

	@TableField("TotalVCoin")
	private Long totalVCoin;

	@TableField("TotalVcoinPayment")
	private Long totalVcoinPayment;

	@TableField("QuestionID")
	private Integer questionID;

	@TableField("Answer")
	private String answer;

	@TableField("Limit")
	private Integer limit;

	@TableField("LastChange")
	private LocalDateTime lastChange;

	@TableField("UserConfirmMobile")
	private Integer userConfirmMobile;

	@TableField("MerchantID")
	private Integer merchantID;

	@TableField("IsOTP")
	private Integer isOTP;

	@TableField("Avatar")
	private Integer avatar;

	@TableField("FrozenValue")
	private Long frozenValue;

	@TableField("CreateTime")
	private LocalDateTime createTime;

	@TableField("CampaignSource")
	private String campaignSource;


	@Override
	protected Serializable pkVal() {
		return this.accountID;
	}

}
