package vn.com.hafintech.game.report.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import vn.com.hafintech.game.report.util.InsertValidation;
import vn.com.hafintech.game.report.util.UpdateValidation;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = false)
@Data
@Accessors(chain = true)
@TableName("RP_Campaign")
public class RpCampaign extends Model<RpCampaign> {

    @NotNull(groups = {UpdateValidation.class})
    @TableId(value = "Id", type = IdType.AUTO)
    private Integer id;
    @TableField("WebsiteUrl")
    @NotBlank(groups = {UpdateValidation.class, InsertValidation.class}, message = "Website URL chưa được nhập")
    private String websiteUrl;
    @NotBlank(groups = {UpdateValidation.class, InsertValidation.class}, message = "Campaign Source chưa được nhập")
    @TableField("CampaignSource")
    private String campaignSource;
    @NotBlank(groups = {UpdateValidation.class, InsertValidation.class}, message = "Campaign Medium chưa được nhập")
    @TableField("CampaignMedium")
    private String campaignMedium;
    @TableField("CampaignName")
    @NotBlank(groups = {UpdateValidation.class, InsertValidation.class}, message = "Campaign Name chưa được nhập")
    private String campaignName;
    @TableField("CampaignTerm")
    private String campaignTerm;
    @TableField("CampaignContent")
    private String campaignContent;
    
    @TableLogic("status")
    @NotBlank(groups = {UpdateValidation.class}, message = "Status chưa được nhập")
    private Integer status;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
