package vn.com.hafintech.game.report.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("RP_DAU_Statistic")
public class RpDAUStatistic extends Model<RpDAUStatistic> {

    @TableId(value = "Id", type = IdType.AUTO)
    private Integer id;

    @TableField("TotalAccount")
    private Long totalAccount;

    @TableField("TotalAccountC")
    private Long totalAccountC;

    @TableField("DAU")
    private Integer dau;

    @TableField("DateTime")
    private LocalDateTime dateTime;

    private Integer r1;
    private Integer r2;
    private Integer r3;
    private Integer r4;
    private Integer r5;
    private Integer r6;
    private Integer r7;
    private Integer r15;
    private Integer r30;

    private Integer c1;
    private Integer c2;
    private Integer c3;
    private Integer c4;
    private Integer c5;
    private Integer c6;
    private Integer c7;
    private Integer c15;
    private Integer c30;

    private Integer finished;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
