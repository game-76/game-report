package vn.com.hafintech.game.report.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("RP_Game_Bank")
public class RpGameBank extends Model<RpGameBank> {

    @TableId("ServiceTypeId")
    private Integer serviceTypeId;

    @TableField("ServiceTypeName")
    private String serviceTypeName;

    @TableField("ApiUrl")
    private String apiUrl;

    @TableField("Status")
    private Integer status;

    @Override
    protected Serializable pkVal() {
        return this.serviceTypeId;
    }
}
