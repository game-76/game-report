package vn.com.hafintech.game.report.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("RP_Game_Bank_Statistic")
public class RpGameBankStatistic extends Model<RpGameBankStatistic> {

    @TableId(value = "Id", type = IdType.AUTO)
    private Integer id;

    @TableField("ServiceTypeId")
    private Integer serviceTypeId;

    @TableField("RoomID")
    private Integer roomID;

    @TableField("LimitFund")
    private Long limitFund;

    @TableField("PrizeFund")
    private Long prizeFund;

    @TableField("JackpotFund")
    private Long jackpotFund;

    @TableField("X2LimitFund")
    private Long x2LimitFund;

    @TableField("X2PrizeFund")
    private Long x2PrizeFund;

    @Override
    protected Serializable pkVal() {
        return this.serviceTypeId;
    }
}
