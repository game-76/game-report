package vn.com.hafintech.game.report.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author Mr.007
 * @since 2019-08-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("RP_Transaction_Statistic")
public class RpTransactionStatistic extends Model<RpTransactionStatistic> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "Id", type = IdType.AUTO)
    private Long id;

    @TableField("ServiceTypeId")
    private Integer serviceTypeId;

    @TableField("ServiceName")
    private String serviceName;

    @TableField("SourceID")
    private Integer sourceID;

    @TableField("RoomID")
    private Integer roomID;

    @TableField("TotalAccount")
    private Long totalAccount;

    @TableField("TotalTrans")
    private Long totalTrans;

    @TableField("TotalFlow")
    private Long totalFlow;

    @TableField("TotalRevenue")
    private Long totalRevenue;

    @TableField("TotalAccountNew")
    private Long totalAccountNew;

    @TableField("TotalTransNew")
    private Long totalTransNew;

    @TableField("TotalFlowNew")
    private Long totalFlowNew;

    @TableField("TotalRevenueNew")
    private Long totalRevenueNew;

    @TableField("LastUpdate")
    private LocalDateTime lastUpdate;

    @TableField("TimeLong")
    private Long timeLong;

    @TableField("Finished")
    private Integer finished;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
