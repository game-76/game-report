package vn.com.hafintech.game.report.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@TableName("RP_User_Statistic")
public class RpUserStatistic extends Model<RpUserStatistic> {
    private Integer id;
    private LocalDateTime dateTime;
    private Integer nru;
    private Integer dau;
    private Integer dpu;
    private Integer peekCCU;
    private Integer serviceTypeId;
    private Integer roomId;
    private Integer sourceID;
}
