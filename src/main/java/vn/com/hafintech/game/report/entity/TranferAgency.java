package vn.com.hafintech.game.report.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.007
 * @since 2019-08-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("RP_TransferAgency")
public class TranferAgency extends Model<TranferAgency> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "TransID", type = IdType.INPUT)
    private Long TransID;

    @TableField("AgencyID")
    private Integer AgencyID;

    @TableField("AgencyParentID")
    private Integer AgencyParentID;

    @TableField("AgencyName")
    private String AgencyName;

    @TableField("AgencyAccountID")
    private Long AgencyAccountID;

    @TableField("AgencyUserName")
    private String AgencyUserName;

    @TableField("LevelID")
    private Integer LevelID;

    @TableField("Amount")
    private Long Amount;

    @TableField("TypeTranfer")
    private Integer TypeTranfer;

    @TableField("CreateTime")
    private LocalDateTime CreateTime;

    @TableField("TypePurchase")
    private Integer TypePurchase;

    @TableField("RefID")
    private Long RefID;

    @TableField("Fee")
    private Integer Fee;

    @TableField("ReceiveAmount")
    private Long ReceiveAmount;

    @TableField("ReceiveAccountID")
    private Long ReceiveAccountID;

    @TableField("ReceiveUserName")
    private String ReceiveUserName;

    @TableField("ReceiveNickName")
    private String ReceiveNickName;

    @TableField("Description")
    private String Description;

    @TableField("CreateTimeInt")
    private Integer CreateTimeInt;

    @TableField("AgencyNickName")
    private String AgencyNickName;


    @Override
    protected Serializable pkVal() {
        return this.TransID;
    }

}
