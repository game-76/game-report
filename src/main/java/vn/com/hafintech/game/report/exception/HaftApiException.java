package vn.com.hafintech.game.report.exception;

import vn.com.hafintech.game.report.constant.CommonConstants;

public class HaftApiException extends RuntimeException {

    private final String code;

    public HaftApiException(String message) {
        super(message);
        this.code = CommonConstants.FAIL;
    }

    public HaftApiException(String code, String message) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
