package vn.com.hafintech.game.report.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.model.BaseStatistics;
import vn.com.hafintech.game.report.model.ChargeStatistic;
import vn.com.hafintech.game.report.model.ChargeStatisticDetailsByCardValue;
import vn.com.hafintech.game.report.model.Partner;

import java.time.LocalDateTime;
import java.util.List;

public interface ChargeJStatisticMapper {

    List<Partner> findAllPartner(@Param("isTopup") Integer isTopup);


    List<BaseStatistics> findAllChargeAccountByIntervalMinute(@Param("partnerId") Integer partnerId,
                                                              @Param("cardType") String cardType,
                                                              @Param("sourceId") Integer sourceId,
                                                              @Param("cardValue") Integer cardValue,
                                                              @Param("fromDate") LocalDateTime fromDate,
                                                              @Param("toDate") LocalDateTime toDate,
                                                              @Param("interval") Integer interval);

    List<BaseStatistics> findAllChargeTransactionByIntervalMinute(@Param("partnerId") Integer partnerId,
                                                                  @Param("cardType") String cardType,
                                                                  @Param("sourceId") Integer sourceId,
                                                                  @Param("cardValue") Integer cardValue,
                                                                  @Param("fromDate") LocalDateTime fromDate,
                                                                  @Param("toDate") LocalDateTime toDate,
                                                                  @Param("interval") Integer interval);

    List<BaseStatistics> findAllChargeAmountByIntervalMinute(@Param("partnerId") Integer partnerId,
                                                             @Param("cardType") String cardType,
                                                             @Param("sourceId") Integer sourceId,
                                                             @Param("cardValue") Integer cardValue,
                                                             @Param("fromDate") LocalDateTime fromDate,
                                                             @Param("toDate") LocalDateTime toDate,
                                                             @Param("interval") Integer interval);

    List<BaseStatistics> findAllChargeJByIntervalMinute(@Param("partnerId") Integer partnerId,
                                                        @Param("cardType") String cardType,
                                                        @Param("sourceId") Integer sourceId,
                                                        @Param("cardValue") Integer cardValue,
                                                        @Param("fromDate") LocalDateTime fromDate,
                                                        @Param("toDate") LocalDateTime toDate,
                                                        @Param("interval") Integer interval);

    IPage<ChargeStatistic> findAllReportStatistic(com.baomidou.mybatisplus.extension.plugins.pagination.Page page, @Param("partnerId") Integer partnerId,
                                                  @Param("cardType") String cardType,
                                                  @Param("sourceId") Integer sourceId,
                                                  @Param("cardValue") Integer cardValue,
                                                  @Param("fromDate") LocalDateTime fromDate,
                                                  @Param("toDate") LocalDateTime toDate);

    IPage<ChargeStatisticDetailsByCardValue> findAllChargeStatisticByCardValue(com.baomidou.mybatisplus.extension.plugins.pagination.Page page,
                                                                               @Param("partnerId") Integer partnerId,
                                                                               @Param("cardType") String cardType,
                                                                               @Param("sourceId") Integer sourceId,
                                                                               @Param("cardValue") Integer cardValue,
                                                                               @Param("fromDate") LocalDateTime fromDate,
                                                                               @Param("toDate") LocalDateTime toDate);

    IPage<ChargeStatisticDetailsByCardValue> findAllChargeStatisticByCardType(Page page,
                                                                              @Param("partnerId") Integer partnerId,
                                                                              @Param("cardType") String cardType,
                                                                              @Param("sourceId") Integer sourceId,
                                                                              @Param("cardValue") Integer cardValue,
                                                                              @Param("fromDate") LocalDateTime fromDate,
                                                                              @Param("toDate") LocalDateTime toDate);
}
