package vn.com.hafintech.game.report.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.model.BaseStatistics;
import vn.com.hafintech.game.report.model.ChargeStatistic;
import vn.com.hafintech.game.report.model.ChargeStatisticDetailsByCardValue;

import java.time.LocalDateTime;
import java.util.List;

public interface CrashoutStatisticMapper {

    List<BaseStatistics> findAllCrashoutAccountByIntervalMinute(@Param("partnerId") Integer partnerId,
                                                                @Param("cardType") String cardType,
                                                                @Param("sourceId") Integer sourceId,
                                                                @Param("cardValue") Integer cardValue,
                                                                @Param("fromDate") LocalDateTime fromDate,
                                                                @Param("toDate") LocalDateTime toDate,
                                                                @Param("interval") Integer interval);

    List<BaseStatistics> findAllCrashoutTransactionByIntervalMinute(@Param("partnerId") Integer partnerId,
                                                                    @Param("cardType") String cardType,
                                                                    @Param("sourceId") Integer sourceId,
                                                                    @Param("cardValue") Integer cardValue,
                                                                    @Param("fromDate") LocalDateTime fromDate,
                                                                    @Param("toDate") LocalDateTime toDate,
                                                                    @Param("interval") Integer interval);

    List<BaseStatistics> findAllCrashoutAmountByIntervalMinute(@Param("partnerId") Integer partnerId,
                                                               @Param("cardType") String cardType,
                                                               @Param("sourceId") Integer sourceId,
                                                               @Param("cardValue") Integer cardValue,
                                                               @Param("fromDate") LocalDateTime fromDate,
                                                               @Param("toDate") LocalDateTime toDate,
                                                               @Param("interval") Integer interval);

    List<BaseStatistics> findAllCrashoutJByIntervalMinute(@Param("partnerId") Integer partnerId,
                                                          @Param("cardType") String cardType,
                                                          @Param("sourceId") Integer sourceId,
                                                          @Param("cardValue") Integer cardValue,
                                                          @Param("fromDate") LocalDateTime fromDate,
                                                          @Param("toDate") LocalDateTime toDate,
                                                          @Param("interval") Integer interval);

    IPage<ChargeStatistic> findAllCrashoutStatistic(Page page, @Param("partnerId") Integer partnerId,
                                                    @Param("cardType") String cardType,
                                                    @Param("sourceId") Integer sourceId,
                                                    @Param("cardValue") Integer cardValue,
                                                    @Param("fromDate") LocalDateTime fromDate,
                                                    @Param("toDate") LocalDateTime toDate);

    IPage<ChargeStatisticDetailsByCardValue> findAllCrashoutStatisticByCardValue(Page page,
                                                                                 @Param("partnerId") Integer partnerId,
                                                                                 @Param("cardType") String cardType,
                                                                                 @Param("sourceId") Integer sourceId,
                                                                                 @Param("cardValue") Integer cardValue,
                                                                                 @Param("fromDate") LocalDateTime fromDate,
                                                                                 @Param("toDate") LocalDateTime toDate);

    IPage<ChargeStatisticDetailsByCardValue> findAllCrashoutStatisticByCardType(Page page,
                                                                                @Param("partnerId") Integer partnerId,
                                                                                @Param("cardType") String cardType,
                                                                                @Param("sourceId") Integer sourceId,
                                                                                @Param("cardValue") Integer cardValue,
                                                                                @Param("fromDate") LocalDateTime fromDate,
                                                                                @Param("toDate") LocalDateTime toDate);
}
