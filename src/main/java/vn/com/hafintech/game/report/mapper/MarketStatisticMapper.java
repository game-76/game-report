package vn.com.hafintech.game.report.mapper;

import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.model.ChargeMarketStatistic;
import vn.com.hafintech.game.report.model.ServiceMarketStatistic;
import vn.com.hafintech.game.report.model.SourceMarketStatistic;

import java.time.LocalDateTime;
import java.util.List;

public interface MarketStatisticMapper {

    List<ServiceMarketStatistic> findAllDAUMarketStatistic(@Param("fromDate") LocalDateTime fromDate,
                                                           @Param("toDate") LocalDateTime toDate);

    List<ServiceMarketStatistic> findAllTransactionMarketStatistic(@Param("fromDate") LocalDateTime fromDate,
                                                           @Param("toDate") LocalDateTime toDate);

    List<ServiceMarketStatistic> findAllFlowMoneyMarketStatistic(@Param("fromDate") LocalDateTime fromDate,
                                                                   @Param("toDate") LocalDateTime toDate);

    List<ServiceMarketStatistic> findAllRevenueMarketStatistic(@Param("fromDate") LocalDateTime fromDate,
                                                                 @Param("toDate") LocalDateTime toDate);


    List<SourceMarketStatistic> findAllDAUMarketStatisticBySource(@Param("fromDate") LocalDateTime fromDate,
                                                                  @Param("toDate") LocalDateTime toDate);

    List<SourceMarketStatistic> findAllTransactionMarketStatisticBySource(@Param("fromDate") LocalDateTime fromDate,
                                                                   @Param("toDate") LocalDateTime toDate);

    List<SourceMarketStatistic> findAllFlowMoneyMarketStatisticBySource(@Param("fromDate") LocalDateTime fromDate,
                                                                 @Param("toDate") LocalDateTime toDate);

    List<SourceMarketStatistic> findAllRevenueMarketStatisticBySource(@Param("fromDate") LocalDateTime fromDate,
                                                               @Param("toDate") LocalDateTime toDate);


    List<ChargeMarketStatistic> findAllChargeAccountMarketStatistic(@Param("fromDate") LocalDateTime fromDate,
                                                                  @Param("toDate") LocalDateTime toDate);

    List<ChargeMarketStatistic> findAllChargeTransactionMarketStatistic(@Param("fromDate") LocalDateTime fromDate,
                                                                          @Param("toDate") LocalDateTime toDate);

    List<ChargeMarketStatistic> findAllChargeJMarketStatistic(@Param("fromDate") LocalDateTime fromDate,
                                                                        @Param("toDate") LocalDateTime toDate);


}
