package vn.com.hafintech.game.report.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.entity.RpUserStatistic;
import vn.com.hafintech.game.report.model.BaseStatistics;
import vn.com.hafintech.game.report.model.SourceMarketStatistic;
import vn.com.hafintech.game.report.model.SourceStatisticBase;
import vn.com.hafintech.game.report.model.UserDAUBySource;

import java.time.LocalDateTime;
import java.util.List;

public interface ReportUserStatisticMapper {

    List<UserDAUBySource> findAllUserDauBySource(@Param("serviceId") Integer serviceId,
                                                 @Param("sourceId") Integer sourceId,
                                                 @Param("roomID") Integer roomId,
                                                 @Param("fromDate") LocalDateTime fromDate,
                                                 @Param("toDate") LocalDateTime toDate);


    List<BaseStatistics> findAllCCU(@Param("serviceId") Integer serviceId,
                                    @Param("online") Integer online,
                                    @Param("sourceId") Integer sourceId,
                                    @Param("roomId") Integer roomId,
                                    @Param("fromDate") LocalDateTime fromDate,
                                    @Param("toDate") LocalDateTime toDate,
                                    @Param("interval") Integer interval);

    List<BaseStatistics> findAllNRUByMinute(
            @Param("sourceId") Integer sourceId,
            @Param("fromDate") LocalDateTime fromDate,
            @Param("toDate") LocalDateTime toDate,
            @Param("interval") Integer interval);

    List<BaseStatistics> findAllDPUByMinute(
            @Param("interval") Integer interval,
            @Param("serviceId") Integer serviceId,
            @Param("sourceId") Integer sourceId,
            @Param("roomId") Integer roomId,
            @Param("fromDate") LocalDateTime fromDate,
            @Param("toDate") LocalDateTime toDate);

    List<BaseStatistics> findAllPeekCCUByMinute(
            @Param("interval") Integer interval,
            @Param("serviceId") Integer serviceId,
            @Param("sourceId") Integer sourceId,
            @Param("roomId") Integer roomId,
            @Param("fromDate") LocalDateTime fromDate,
            @Param("toDate") LocalDateTime toDate
    );

    IPage<SourceStatisticBase> findAllNRUDailyBySource(Page page, @Param("sourceId") Integer sourceId,
                                                       @Param("fromDate") LocalDateTime fromDate,
                                                       @Param("toDate") LocalDateTime toDate);

    List<SourceMarketStatistic> findAllNRUPieBySource(@Param("fromDate") LocalDateTime fromDate,
                                                      @Param("toDate") LocalDateTime toDate);

    List<RpUserStatistic> findAllRpUserStatistic(
            @Param("fromDate") LocalDateTime fromDate,
            @Param("toDate") LocalDateTime toDate,
            @Param("serviceId") Integer serviceId,
            @Param("sourceId") Integer sourceId,
            @Param("roomId") Integer roomId);
}
