package vn.com.hafintech.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.entity.Accounts;
import vn.com.hafintech.game.report.model.AccountBalanceHistory;
import vn.com.hafintech.game.report.model.AccountWinHistory;
import vn.com.hafintech.game.report.model.ServiceWinLossDetails;
import vn.com.hafintech.game.report.model.TopWinLoss;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Mr007
 * @since 2019-08-26
 */
public interface RpAccountsMapper extends BaseMapper<Accounts> {

    IPage<Accounts> findTopAccountBalance(Page page, @Param("fromDate") LocalDateTime fromDate,
                                          @Param("toDate") LocalDateTime toDate,
                                          @Param("nickname") String nickname);

    IPage<AccountBalanceHistory> findAllAccountBalanceHistory(Page page, @Param("nickname") String nickname,
                                                              @Param("serviceTypeId") Integer serviceTypeId,
                                                              @Param("fromDate") LocalDateTime fromDate,
                                                              @Param("toDate") LocalDateTime toDate);

    /**
     * @param page
     * @param nickname
     * @param serviceTypeId
     * @param fromDate
     * @param toDate
     * @param type          1= win, 0 = loss
     * @return
     */
    IPage<TopWinLoss> findAllTopWinLoss(Page page, @Param("nickname") String nickname,
                                        @Param("serviceTypeId") Integer serviceTypeId,
                                        @Param("fromDate") LocalDateTime fromDate,
                                        @Param("toDate") LocalDateTime toDate,
                                        @Param("type") int type);

    IPage<AccountWinHistory> findAllNoHu(Page page, @Param("nickname") String nickname,
                                         @Param("serviceTypeId") Integer serviceTypeId,
                                         @Param("fromDate") LocalDateTime fromDate,
                                         @Param("toDate") LocalDateTime toDate,
                                         @Param("roomID") Integer roomID,
                                         @Param("bettingAmount") Long bettingAmount);

    IPage<AccountWinHistory> findAllWin(Page page, @Param("nickname") String nickname,
                                         @Param("serviceTypeId") Integer serviceTypeId,
                                         @Param("fromDate") LocalDateTime fromDate,
                                         @Param("toDate") LocalDateTime toDate,
                                         @Param("roomID") Integer roomID,
                                         @Param("bettingAmount") Long bettingAmount);

    List<ServiceWinLossDetails> findAllServiceWinLossDetails(@Param("nickname") String nickname,
                                                             @Param("serviceTypeId") Integer serviceTypeId,
                                                             @Param("fromDate") LocalDateTime fromDate,
                                                             @Param("toDate") LocalDateTime toDate,
                                                             @Param("type") int type);
}
