package vn.com.hafintech.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vn.com.hafintech.game.report.entity.RpCampaign;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Mr007
 * @since 2019-08-26
 */
public interface RpCampaignMapper extends BaseMapper<RpCampaign> {

    int countCampaignSource(String source);

}
