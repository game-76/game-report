package vn.com.hafintech.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.entity.RpDAUStatistic;

import java.time.LocalDateTime;

public interface RpDAUStatisticMapper extends BaseMapper<RpDAUStatistic> {

    IPage<RpDAUStatistic> findAllDauStatisticRate(Page page, @Param("fromDate") LocalDateTime fromDate,
                                                  @Param("toDate") LocalDateTime toDate);

}
