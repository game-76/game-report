package vn.com.hafintech.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vn.com.hafintech.game.report.entity.RpGameBank;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Mr007
 * @since 2019-08-26
 */
public interface RpGameBankMapper extends BaseMapper<RpGameBank> {

}
