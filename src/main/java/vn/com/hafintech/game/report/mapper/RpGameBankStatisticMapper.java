package vn.com.hafintech.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.entity.RpGameBankStatistic;
import vn.com.hafintech.game.report.model.GameBankStatisticInfo;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Mr007
 * @since 2019-08-26
 */
public interface RpGameBankStatisticMapper extends BaseMapper<RpGameBankStatistic> {

    List<GameBankStatisticInfo> findAllGameBankStatisticInfo(@Param("serviceTypeId") Integer serviceTypeId,
                                                             @Param("roomId") Integer roomId);

    List<Integer> findAllServiceRoom(@Param("serviceTypeId") Integer serviceTypeId);
}
