package vn.com.hafintech.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.entity.RpTransactionStatistic;

import java.time.LocalDateTime;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Mr.007
 * @since 2019-08-27
 */
public interface RpTransactionStatisticMapper extends BaseMapper<RpTransactionStatistic> {

    LocalDateTime findLastUpdate(@Param("time") Long time);

    int checkFinishedByDay(@Param("time") Long time);

}
