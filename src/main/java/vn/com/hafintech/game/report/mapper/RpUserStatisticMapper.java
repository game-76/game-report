package vn.com.hafintech.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.entity.RpUserStatistic;

import java.time.LocalDateTime;

public interface RpUserStatisticMapper extends BaseMapper<RpUserStatistic> {

    IPage<RpUserStatistic> findAllRpUserStatistic(Page page, @Param("fromDate") LocalDateTime fromDate,
                                                  @Param("toDate") LocalDateTime toDate,
                                                  @Param("serviceId") Integer serviceId,
                                                  @Param("roomId") Integer roomId,
                                                  @Param("sourceId") Integer sourceId);



}
