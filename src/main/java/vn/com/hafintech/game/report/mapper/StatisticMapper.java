package vn.com.hafintech.game.report.mapper;

import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.model.*;

import java.time.LocalDateTime;
import java.util.List;

public interface StatisticMapper {

    List<ServiceType> findAllServiceTypeGame();

    List<ServiceRoom> findAllRoomService(@Param("serviceTypeId") Integer serviceTypeId);

    List<BaseStatistics> findAllDAUbyIntervalMinute(@Param("sourceId") Integer sourceId,
                                                    @Param("serviceId") Integer serviceId,
                                                    @Param("roomId") Integer roomId,
                                                    @Param("fromDate") LocalDateTime fromDate,
                                                    @Param("toDate") LocalDateTime toDate,
                                                    @Param("interval") Integer interval);

    List<BaseStatistics> findAllTransactionbyIntervalMinute(@Param("sourceId") Integer sourceId,
                                                            @Param("serviceId") Integer serviceId,
                                                            @Param("roomId") Integer roomId,
                                                            @Param("fromDate") LocalDateTime fromDate,
                                                            @Param("toDate") LocalDateTime toDate,
                                                            @Param("interval") Integer interval);


    List<BaseStatistics> findAllFlowMoneyByIntervalMinute(@Param("sourceId") Integer sourceId,
                                                          @Param("serviceId") Integer serviceId,
                                                          @Param("roomId") Integer roomId,
                                                          @Param("fromDate") LocalDateTime fromDate,
                                                          @Param("toDate") LocalDateTime toDate,
                                                          @Param("interval") Integer interval);

    List<BaseStatistics> findAllRevenueByIntervalMinute(@Param("sourceId") Integer sourceId,
                                                        @Param("serviceId") Integer serviceId,
                                                        @Param("roomId") Integer roomId,
                                                        @Param("fromDate") LocalDateTime fromDate,
                                                        @Param("toDate") LocalDateTime toDate,
                                                        @Param("interval") Integer interval);


    List<ReportStatistics> findAllReportStatisticsSummary(@Param("fromDate") LocalDateTime fromDate,
                                                           @Param("toDate") LocalDateTime toDate,
                                                           @Param("sourceId") Integer sourceId,
                                                           @Param("serviceId") Integer serviceId,
                                                           @Param("roomId") Integer roomId);


    List<ReportStatisticsDetails> findAllReportStatisticsDetails(@Param("fromDate") LocalDateTime fromDate,
                                                                  @Param("toDate") LocalDateTime toDate,
                                                                  @Param("sourceId") Integer sourceId,
                                                                  @Param("serviceId") Integer serviceId,
                                                                  @Param("roomId") Integer roomId);
}

