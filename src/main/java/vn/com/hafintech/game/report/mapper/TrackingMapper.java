package vn.com.hafintech.game.report.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.model.ActivityTrackingUser;
import vn.com.hafintech.game.report.model.TrackingMarketing;
import vn.com.hafintech.game.report.model.TrackingUser;
import vn.com.hafintech.game.report.model.TrackingUserService;
import vn.com.hafintech.game.report.model.tracking.BaseTracking;

import java.time.LocalDateTime;
import java.util.List;

public interface TrackingMapper {

    IPage<ActivityTrackingUser> findAllAccountPlayMore(Page page, @Param("fromDate") LocalDateTime fromDate,
                                                       @Param("toDate") LocalDateTime toDate);

    /**
     * win = 1
     * loss = 0
     *
     * @param fromDate
     * @param toDate
     * @param win
     * @return
     */
    IPage<ActivityTrackingUser> findAllAccountWinLoss(Page page, @Param("fromDate") LocalDateTime fromDate,
                                                      @Param("toDate") LocalDateTime toDate,
                                                      @Param("win") Integer win);

    IPage<ActivityTrackingUser> findAllAccountGetOut(Page page, @Param("fromDate") LocalDateTime fromDate,
                                                     @Param("toDate") LocalDateTime toDate);

    IPage<ActivityTrackingUser> findAllIAU(Page page, @Param("fromDate") LocalDateTime fromDate,
                                           @Param("toDate") LocalDateTime toDate);

    List<BaseTracking> findAllNruDaily(@Param("fromDate") LocalDateTime fromDate,
                                       @Param("toDate") LocalDateTime toDate,
                                       @Param("sourceId") Integer sourceId,
                                       @Param("campaignSource") String campaignSource);

    List<BaseTracking> findAllAuDaily(@Param("fromDate") LocalDateTime fromDate,
                                      @Param("toDate") LocalDateTime toDate,
                                      @Param("sourceId") Integer sourceId,
                                      @Param("campaignSource") String campaignSource,
                                      @Param("serviceId") Integer serviceId);

    List<BaseTracking> findAllCbuDaily(@Param("fromDate") LocalDateTime fromDate,
                                       @Param("toDate") LocalDateTime toDate,
                                       @Param("sourceId") Integer sourceId,
                                       @Param("campaignSource") String campaignSource,
                                       @Param("serviceId") Integer serviceId);


    List<BaseTracking> findAllPUDaily(@Param("fromDate") LocalDateTime fromDate,
                                      @Param("toDate") LocalDateTime toDate,
                                      @Param("sourceId") Integer sourceId,
                                      @Param("campaignSource") String campaignSource,
                                      @Param("serviceId") Integer serviceId);

    List<BaseTracking> findAllRevenueDaily(@Param("fromDate") LocalDateTime fromDate,
                                           @Param("toDate") LocalDateTime toDate,
                                           @Param("sourceId") Integer sourceId,
                                           @Param("campaignSource") String campaignSource,
                                           @Param("serviceId") Integer serviceId);

    List<BaseTracking> findAllARPPUDaily(@Param("fromDate") LocalDateTime fromDate,
                                         @Param("toDate") LocalDateTime toDate,
                                         @Param("sourceId") Integer sourceId,
                                         @Param("campaignSource") String campaignSource,
                                         @Param("serviceId") Integer serviceId);

    IPage<TrackingUser> findAllTrackingUserInfo(@Param("fromDate") LocalDateTime fromDate,
                                                @Param("toDate") LocalDateTime toDate,
                                                @Param("sourceId") Integer sourceId,
                                                @Param("serviceId") Integer serviceId);

    List<TrackingMarketing> findAllTrackingMarketing(@Param("fromDate") LocalDateTime fromDate,
                                                     @Param("toDate") LocalDateTime toDate,
                                                     @Param("sourceId") Integer sourceId,
                                                     @Param("campaignSource") String campaignSource,
                                                     @Param("serviceId") Integer serviceId);

    List<TrackingUserService> findAllTrackingUserService(@Param("fromDate") LocalDateTime fromDate,
                                                         @Param("toDate") LocalDateTime toDate,
                                                         @Param("sourceId") Integer sourceId,
                                                         @Param("serviceId") Integer serviceId);



}
