package vn.com.hafintech.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.entity.TranferAgency;

import java.time.LocalDateTime;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Mr.007
 * @since 2019-08-28
 */
public interface TranferAgencyMapper extends BaseMapper<TranferAgency> {
    IPage<TranferAgency> topTransferAgencyDetails(Page page, @Param("fromDate") LocalDateTime fromDate,
                                           @Param("toDate") LocalDateTime toDate,
                                           @Param("nickname") String nickname,
                                           @Param("type") Integer type);

    IPage<TranferAgency> topTransferAgency(Page page, @Param("fromDate") LocalDateTime fromDate,
                                           @Param("toDate") LocalDateTime toDate,
                                           @Param("nickname") String nickname,
                                           @Param("type") Integer type);
}
