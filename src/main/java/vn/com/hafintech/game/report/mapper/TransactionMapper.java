package vn.com.hafintech.game.report.mapper;

import org.apache.ibatis.annotations.Param;
import vn.com.hafintech.game.report.model.Transactions;

import java.util.List;

public interface TransactionMapper {

    List<Transactions> findTopTransaction(@Param("accountId") int accountId, @Param("top") Integer top);



}

