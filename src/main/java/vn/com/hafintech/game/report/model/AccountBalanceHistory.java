package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;


@Data
public class AccountBalanceHistory implements Serializable {
    private String serviceName;
    private LocalDateTime createdTime;
    private Long amount;
    private String description;
    private Integer inOut;
    private Long referenceID;

}
