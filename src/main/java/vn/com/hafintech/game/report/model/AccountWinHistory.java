package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class AccountWinHistory implements Serializable {

    private Integer accountID;
    private String nickname;
    private Long totalAmount;
    private String serviceTypeName;
    private Long referenceId;
    private LocalDateTime createTime;
    private Integer roomID;
}
