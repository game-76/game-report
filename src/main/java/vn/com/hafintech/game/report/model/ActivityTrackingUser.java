package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class ActivityTrackingUser implements Serializable {
    private Integer accountID;
    private String userName;
    private String userFullname;
    private String mobile;
    private LocalDateTime lastPlay;
    private Long totalVCoin;
    private Long totalVcoinPayment;
    private Long total;

}
