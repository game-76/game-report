package vn.com.hafintech.game.report.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class BaseStatistics implements Serializable {
    private Long total;
    @JsonFormat(pattern = "HH:mm")
    private LocalDateTime dateTime;
}
