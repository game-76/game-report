package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ChargeMarketStatistic implements Serializable {
    private Integer cardValue;
    private Long percent;
}
