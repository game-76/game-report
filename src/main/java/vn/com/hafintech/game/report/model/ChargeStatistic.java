package vn.com.hafintech.game.report.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
public class ChargeStatistic extends ChargeStatisticBase implements Serializable {
    private LocalDate datetime;
}
