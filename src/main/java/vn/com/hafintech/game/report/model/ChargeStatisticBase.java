package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ChargeStatisticBase implements Serializable {
    private Long totalAccount;
    private Long totalTrans;
    private Long totalAmount;
    private Long totalJ;
}
