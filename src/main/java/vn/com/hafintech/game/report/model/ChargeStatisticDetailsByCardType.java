package vn.com.hafintech.game.report.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
public class ChargeStatisticDetailsByCardType extends ChargeStatisticBase implements Serializable {
    private String cardType;
}
