package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class GameBankStatisticInfo implements Serializable {
    private Integer serviceTypeId;
    private String serviceTypeName;
    private Long totalAmount;
    private Integer totalRoom;
}
