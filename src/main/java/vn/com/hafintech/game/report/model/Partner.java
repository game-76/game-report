package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Partner implements Serializable {

    private Integer id;
    private String partnerName;

}
