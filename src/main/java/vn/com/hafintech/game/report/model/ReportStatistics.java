package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class ReportStatistics implements Serializable {
    private LocalDateTime date;
    private Long sl;
    private Long sgd;
    private Long td;
    private Long ln;

    private Long sln;
    private Long sgdn;
    private Long tdn;
    private Long lnn;
}
