package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ReportStatisticsDetails implements Serializable {
    private Integer serviceType;
    private String serviceTypeName;
    private Long sl;
    private Long sgd;
    private Long td;
    private Long ln;
    private Long sln;
    private Long sgdn;
    private Long tdn;
    private Long lnn;
}
