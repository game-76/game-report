package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class RoomBankStatistic implements Serializable {

    private Integer roomId;
    private Long total;

}
