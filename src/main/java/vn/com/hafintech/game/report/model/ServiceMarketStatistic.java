package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ServiceMarketStatistic implements Serializable {

    private Integer serviceTypeId;
    private String serviceName;
    private Long percent;

}
