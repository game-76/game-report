package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ServiceRoom implements Serializable {
	private Integer serviceId;
	private Integer roomId;
}
