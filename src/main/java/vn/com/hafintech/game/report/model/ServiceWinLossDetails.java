package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ServiceWinLossDetails implements Serializable {

    private String serviceName;
    private String totalAmount;

}
