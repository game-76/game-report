package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Services implements Serializable {

	private Integer serviceId;
	private String serviceKey;
	private Integer serviceType;
	private Integer inOut;
	private Integer serviceCategory;
	private Integer isLoyalty;
	private Integer profitRate;
	private Integer roomId;

}
