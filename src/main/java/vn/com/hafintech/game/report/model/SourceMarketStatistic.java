package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class SourceMarketStatistic implements Serializable {
    private Integer sourceId;
    private Long percent;
}
