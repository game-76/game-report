package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class TopWinLoss implements Serializable {

    private Integer accountID;
    private String nickname;
    private Long totalAmount;

}
