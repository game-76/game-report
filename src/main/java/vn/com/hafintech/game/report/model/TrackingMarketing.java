package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class TrackingMarketing implements Serializable {
    private String CampaignSource;
    private Long NRU;
    private Long NRUPERNAU;
    private Long PayRate;
    private Long PU;
    private Long ARPPU;
    private Long REV;
    private Long ARPU;
    private Long NAU;
    private Long flowMoney;
}
