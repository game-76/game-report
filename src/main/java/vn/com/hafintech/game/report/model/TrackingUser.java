package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class TrackingUser implements Serializable {
    private String userFullName;
    private Long totalTrans;
    private Long flowMoney;
}
