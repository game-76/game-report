package vn.com.hafintech.game.report.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
public class TrackingUserService extends TrackingMarketing implements Serializable {
    private LocalDateTime dateTime;
    private String campaignName;
    private String campaignContent;
}
