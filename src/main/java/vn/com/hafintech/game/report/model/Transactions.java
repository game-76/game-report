package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author Mr.007
 * @since 2019-08-13
 */

@Data
public class Transactions implements Serializable {


    private Long transactionID;

    private Integer serviceID;

    private String serviceName;

    private Integer accountID;

    private String username;

    private Integer relatedAccountID;

    private String relatedUsername;

    private Integer cardType;

    private Long cardID;

    private Long partnerAmount;

    private Long amount;

    private Long subAmount;

    private Long gift;

    private Long tax;

    private String description;

    private Long referenceID;

    private Long relatedTranID;

    private Integer merchantID;

    private Integer sourceID;

    private String clientIP;

    private LocalDateTime createdTime;

    private String updatedUser;

    private Integer roomID;

    private Long discount;

    private Integer inOut;
}
