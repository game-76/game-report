package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserDAU implements Serializable {

    private Long totalWeb;
    private Long totalIos;
    private Long totalAndroid;
    private Long totalClient;

    private List<UserDAUBySource> userDAUBySources;
}
