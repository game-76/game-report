package vn.com.hafintech.game.report.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class UserDAUBySource implements Serializable {

    private LocalDateTime dateTime;
    private Long totalWeb;
    private Long totalApp;
    private Long totalIos;
    private Long totalAndroid;

}
