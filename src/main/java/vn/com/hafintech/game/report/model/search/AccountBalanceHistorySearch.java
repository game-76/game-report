package vn.com.hafintech.game.report.model.search;

import cn.hutool.core.date.DatePattern;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;
import vn.com.hafintech.game.report.page.BaseQueryParamModel;

import java.io.Serializable;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
public class AccountBalanceHistorySearch extends BaseQueryParamModel implements Serializable {

    private String nickname;
    private Integer serviceTypeId;
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime fromDate;
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime toDate;

}
