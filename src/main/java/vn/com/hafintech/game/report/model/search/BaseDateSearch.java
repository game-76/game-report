package vn.com.hafintech.game.report.model.search;

import cn.hutool.core.date.DatePattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import vn.com.hafintech.game.report.page.BaseQueryParamModel;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class BaseDateSearch extends BaseQueryParamModel implements Serializable {
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime fromDate;
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime toDate;

}
