package vn.com.hafintech.game.report.model.search;

import cn.hutool.core.date.DatePattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class ChargeStatisticSearch implements Serializable {
    private Integer partnerId;
    private String cardType;
    private Integer cardValue;
    private Integer sourceId;
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime fromDate;
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime toDate;
}
