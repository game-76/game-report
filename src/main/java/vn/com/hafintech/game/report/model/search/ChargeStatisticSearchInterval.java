package vn.com.hafintech.game.report.model.search;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
public class ChargeStatisticSearchInterval extends ChargeStatisticSearch implements Serializable {
    private Integer interval;
}
