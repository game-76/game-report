package vn.com.hafintech.game.report.model.search;

import cn.hutool.core.date.DatePattern;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;
import vn.com.hafintech.game.report.page.BaseQueryParamModel;

import java.io.Serializable;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
public class TopWinLossSearch extends BaseQueryParamModel implements Serializable {
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime fromDate;
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime toDate;
    private Integer serviceTypeId;
    private String nickName;
    /**
     * type = 0 Loss
     * type = 1 Win
     */
    private Integer type;

}
