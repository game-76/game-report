package vn.com.hafintech.game.report.model.search;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
public class TrackingWinLossSearch extends BaseDateSearch implements Serializable {

    private Integer win;

}
