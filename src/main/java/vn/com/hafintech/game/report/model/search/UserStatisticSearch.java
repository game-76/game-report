package vn.com.hafintech.game.report.model.search;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserStatisticSearch extends BaseDateSearch implements Serializable {

    private Integer sourceId;
    private Integer serviceTypeId;
    private Integer roomId;

}
