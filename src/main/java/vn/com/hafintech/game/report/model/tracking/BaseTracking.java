package vn.com.hafintech.game.report.model.tracking;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class BaseTracking implements Serializable {
    private LocalDateTime dateTime;
    private Integer total;
}
