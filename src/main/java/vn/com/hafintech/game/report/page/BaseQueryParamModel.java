package vn.com.hafintech.game.report.page;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseQueryParamModel implements Serializable {
	private Integer pageNumber;
	private Integer pageSize;
	private String[] ascs;
	private String[] descs;
}
