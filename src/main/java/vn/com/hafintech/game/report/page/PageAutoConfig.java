package vn.com.hafintech.game.report.page;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PageAutoConfig {

	@Bean
	@ConditionalOnMissingBean
	public PageProperties pageProperties() {
		return new PageProperties();
	}

	@Bean
	@ConditionalOnMissingBean
	public PageTool pageTool() {
		return new PageTool(pageProperties());
	}

}
