package vn.com.hafintech.game.report.page;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotNull;

@Data
@ConditionalOnExpression("!'${page}'.isEmpty()")
@ConfigurationProperties(prefix = "page")
public class PageProperties {
    @NotNull
    private Integer defaultPageSize;
    @NotNull
    private Integer maxPageSize;
    @NotNull
    private Integer defaultPageNumber;

}
