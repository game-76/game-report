package vn.com.hafintech.game.report.page;


import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class PageTool {

	private final PageProperties pageProperties;

	public Page getPage(BaseQueryParamModel queryParamModel) {
		Integer defaultPageNo = pageProperties.getDefaultPageNumber();
		Integer defaultPageSize = pageProperties.getDefaultPageSize();
		Integer maxPageSize = pageProperties.getMaxPageSize();
		if (queryParamModel != null && queryParamModel.getPageNumber() != null) {
			defaultPageNo = queryParamModel.getPageNumber();
		}
		if (queryParamModel != null && queryParamModel.getPageSize() != null) {
			defaultPageSize = queryParamModel.getPageSize();
		}
		if (defaultPageSize > maxPageSize) defaultPageSize = maxPageSize;
		Page page = new Page(defaultPageNo, defaultPageSize);
		String[] ascs = queryParamModel == null ? null : queryParamModel.getAscs();
		String[] descs = queryParamModel == null ? null : queryParamModel.getDescs();
		List<OrderItem> orderItems = new ArrayList<>();
		if (!ArrayUtil.isEmpty(ascs)) {
			orderItems.addAll(Arrays.stream(ascs)
				.map(k -> new OrderItem().setAsc(true).setColumn(k)).collect(Collectors.toList()));
		}

		if (!ArrayUtil.isEmpty(descs)) {
			orderItems.addAll(Arrays.stream(descs)
				.map(k -> new OrderItem().setAsc(false).setColumn(k)).collect(Collectors.toList()));
		}
		if (!orderItems.isEmpty()) {
			page.setOrders(orderItems);
		}
		return page;
	}

}
