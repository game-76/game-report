package vn.com.hafintech.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import vn.com.hafintech.game.report.model.BaseStatistics;
import vn.com.hafintech.game.report.model.ChargeStatistic;
import vn.com.hafintech.game.report.model.ChargeStatisticDetailsByCardValue;
import vn.com.hafintech.game.report.model.search.ChargeStatisticSearchInterval;
import vn.com.hafintech.game.report.model.search.ChargeStatisticSearchPage;

import java.util.List;

public interface CashoutStatisticService {

    List<BaseStatistics> findAllCashoutAccountByIntervalMinute(ChargeStatisticSearchInterval searchInterval);

    List<BaseStatistics> findAllCashoutTransactionByIntervalMinute(ChargeStatisticSearchInterval searchInterval);

    List<BaseStatistics> findAllCashoutAmountByIntervalMinute(ChargeStatisticSearchInterval searchInterval);

    List<BaseStatistics> findAllCashoutJByIntervalMinute(ChargeStatisticSearchInterval searchInterval);

    IPage<ChargeStatistic> findAllCashoutStatistic(ChargeStatisticSearchPage statisticSearch);

    IPage<ChargeStatisticDetailsByCardValue> findAllCashoutStatisticByCardValue(ChargeStatisticSearchPage statisticSearch);

    IPage<ChargeStatisticDetailsByCardValue> findAllCashoutStatisticByCardType(ChargeStatisticSearchPage statisticSearch);

}
