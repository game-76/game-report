package vn.com.hafintech.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import vn.com.hafintech.game.report.model.BaseStatistics;
import vn.com.hafintech.game.report.model.ChargeStatistic;
import vn.com.hafintech.game.report.model.ChargeStatisticDetailsByCardValue;
import vn.com.hafintech.game.report.model.Partner;
import vn.com.hafintech.game.report.model.search.ChargeStatisticSearchInterval;
import vn.com.hafintech.game.report.model.search.ChargeStatisticSearchPage;

import java.util.List;

public interface ChargeStatisticService {

    List<Partner> findAllPartner(Integer isTopup);


    List<BaseStatistics> findAllChargeAccountByIntervalMinute(ChargeStatisticSearchInterval searchInterval);

    List<BaseStatistics> findAllChargeTransactionByIntervalMinute(ChargeStatisticSearchInterval searchInterval);

    List<BaseStatistics> findAllChargeAmountByIntervalMinute(ChargeStatisticSearchInterval searchInterval);

    List<BaseStatistics> findAllChargeJByIntervalMinute(ChargeStatisticSearchInterval searchInterval);

    IPage<ChargeStatistic> findAllReportStatistic(ChargeStatisticSearchPage statisticSearch);

    IPage<ChargeStatisticDetailsByCardValue> findAllChargeStatisticByCardValue(ChargeStatisticSearchPage statisticSearch);

    IPage<ChargeStatisticDetailsByCardValue> findAllChargeStatisticByCardType(ChargeStatisticSearchPage statisticSearch);

}
