package vn.com.hafintech.game.report.service;

import vn.com.hafintech.game.report.model.ChargeMarketStatistic;
import vn.com.hafintech.game.report.model.ServiceMarketStatistic;
import vn.com.hafintech.game.report.model.SourceMarketStatistic;

import java.time.LocalDateTime;
import java.util.List;

public interface MarketStatisticService {

    List<ServiceMarketStatistic> findAllDAUMarketStatistic(LocalDateTime fromDate,
                                                           LocalDateTime toDate);

    List<ServiceMarketStatistic> findAllTransactionMarketStatistic(LocalDateTime fromDate,
                                                                   LocalDateTime toDate);

    List<ServiceMarketStatistic> findAllFlowMoneyMarketStatistic(LocalDateTime fromDate,
                                                                 LocalDateTime toDate);

    List<ServiceMarketStatistic> findAllRevenueMarketStatistic(LocalDateTime fromDate,
                                                               LocalDateTime toDate);


    List<SourceMarketStatistic> findAllDAUMarketStatisticBySource(LocalDateTime fromDate,
                                                                  LocalDateTime toDate);

    List<SourceMarketStatistic> findAllTransactionMarketStatisticBySource(LocalDateTime fromDate,
                                                                          LocalDateTime toDate);

    List<SourceMarketStatistic> findAllFlowMoneyMarketStatisticBySource(LocalDateTime fromDate,
                                                                        LocalDateTime toDate);

    List<SourceMarketStatistic> findAllRevenueMarketStatisticBySource(LocalDateTime fromDate,
                                                                      LocalDateTime toDate);

    List<ChargeMarketStatistic> findAllChargeAccountMarketStatistic(LocalDateTime fromDate,
                                                                    LocalDateTime toDate);

    List<ChargeMarketStatistic> findAllChargeTransactionMarketStatistic(LocalDateTime fromDate,
                                                                        LocalDateTime toDate);

    List<ChargeMarketStatistic> findAllChargeJMarketStatistic(LocalDateTime fromDate,
                                                              LocalDateTime toDate);

}
