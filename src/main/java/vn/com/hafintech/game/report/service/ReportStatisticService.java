package vn.com.hafintech.game.report.service;

import vn.com.hafintech.game.report.model.*;
import vn.com.hafintech.game.report.model.search.ReportStatisticSearch;
import vn.com.hafintech.game.report.model.search.TableStatisticSearch;

import java.util.List;

public interface ReportStatisticService {


    List<ServiceType> findAllServiceTypeGame();

    List<ServiceRoom> findAllRoomService(Integer serviceTypeId);

    List<BaseStatistics> findAllDAUbyIntervalMinute(ReportStatisticSearch reportStatisticSearch);

    List<BaseStatistics> findAllTransactionbyIntervalMinute(ReportStatisticSearch reportStatisticSearch);

    List<BaseStatistics> findAllFlowMoneyByIntervalMinute(ReportStatisticSearch reportStatisticSearch);

    List<BaseStatistics> findAllRevenueByIntervalMinute(ReportStatisticSearch reportStatisticSearch);

    List<ReportStatistics> findAllReportStatisticsSummaryList(TableStatisticSearch tss);

    List<ReportStatisticsDetails> findAllReportStatisticsDetailsList(TableStatisticSearch tss);
}
