package vn.com.hafintech.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import vn.com.hafintech.game.report.entity.RpUserStatistic;
import vn.com.hafintech.game.report.model.BaseStatistics;
import vn.com.hafintech.game.report.model.SourceMarketStatistic;
import vn.com.hafintech.game.report.model.SourceStatisticBase;
import vn.com.hafintech.game.report.model.UserDAUBySource;
import vn.com.hafintech.game.report.model.search.CCUStatisticSearch;
import vn.com.hafintech.game.report.model.search.ReportStatisticSearch;
import vn.com.hafintech.game.report.model.search.TableStatisticSearch;

import java.time.LocalDateTime;
import java.util.List;

public interface ReportUserStatisticService {

    List<UserDAUBySource> findAllUserDauBySource(ReportStatisticSearch search);

    List<BaseStatistics> findAllCCU(CCUStatisticSearch statisticSearch);

    List<BaseStatistics> findAllNRUByMinute(ReportStatisticSearch search);

    IPage<SourceStatisticBase> findAllNRUBySource(TableStatisticSearch search);

    List<SourceMarketStatistic> findAllNRUMarketStatisticBySource(LocalDateTime fromDate,
                                                                  LocalDateTime toDate);

    List<RpUserStatistic> findAllRpUserStatistic(TableStatisticSearch search);

    List<BaseStatistics> findAllDPUByMinute(ReportStatisticSearch search);

    List<BaseStatistics> findAllPeekCCU(ReportStatisticSearch search);
}
