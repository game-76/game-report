package vn.com.hafintech.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import vn.com.hafintech.game.report.entity.Accounts;
import vn.com.hafintech.game.report.model.AccountBalanceHistory;
import vn.com.hafintech.game.report.model.AccountWinHistory;
import vn.com.hafintech.game.report.model.ServiceWinLossDetails;
import vn.com.hafintech.game.report.model.TopWinLoss;
import vn.com.hafintech.game.report.model.search.AccountBalanceHistorySearch;
import vn.com.hafintech.game.report.model.search.AccountWinHistorySearch;
import vn.com.hafintech.game.report.model.search.TopAccountBalanceSearch;
import vn.com.hafintech.game.report.model.search.TopWinLossSearch;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Mr007
 * @since 2019-08-26
 */
public interface RpAccountsService extends IService<Accounts> {


    IPage<Accounts> findTopAccountBalance(TopAccountBalanceSearch search);

    IPage<AccountBalanceHistory> findAllAccountBalanceHistory(AccountBalanceHistorySearch search);

    IPage<TopWinLoss> findAllTopWinLoss(TopWinLossSearch search);

    IPage<AccountWinHistory> findAllNoHu(AccountWinHistorySearch search);

    IPage<AccountWinHistory> findAllWin(AccountWinHistorySearch search);

    List<ServiceWinLossDetails> findAllServiceWinLossDetails(TopWinLossSearch search);

}
