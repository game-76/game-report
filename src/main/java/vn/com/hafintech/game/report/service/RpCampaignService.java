package vn.com.hafintech.game.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vn.com.hafintech.game.report.entity.RpCampaign;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Mr007
 * @since 2019-08-26
 */
public interface RpCampaignService extends IService<RpCampaign> {

    void saveCampaign(RpCampaign campaign);

    void updateCampaign(RpCampaign rpCampaign);

    void deleteRpCampaign(Integer id);
}
