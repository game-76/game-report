package vn.com.hafintech.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import vn.com.hafintech.game.report.entity.RpDAUStatistic;
import vn.com.hafintech.game.report.model.search.BaseDateSearch;

public interface RpDAUStatisticService extends IService<RpDAUStatistic> {

    IPage<RpDAUStatistic> findAllRpDauStatistic(BaseDateSearch search);

}
