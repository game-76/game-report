package vn.com.hafintech.game.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vn.com.hafintech.game.report.entity.RpGameBank;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Mr007
 * @since 2019-08-26
 */
public interface RpGameBankService extends IService<RpGameBank> {

}
