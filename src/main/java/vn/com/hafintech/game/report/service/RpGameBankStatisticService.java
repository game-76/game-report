package vn.com.hafintech.game.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vn.com.hafintech.game.report.entity.RpGameBankStatistic;
import vn.com.hafintech.game.report.model.GameBankStatisticInfo;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Mr007
 * @since 2019-08-26
 */
public interface RpGameBankStatisticService extends IService<RpGameBankStatistic> {

    List<GameBankStatisticInfo> findAllGameBankStatisticInfo(Integer serviceTypeId,
                                                             Integer roomID);

}
