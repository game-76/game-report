package vn.com.hafintech.game.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vn.com.hafintech.game.report.entity.RpTransactionStatistic;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Mr.007
 * @since 2019-08-27
 */
public interface RpTransactionStatisticService extends IService<RpTransactionStatistic> {

}
