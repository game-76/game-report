package vn.com.hafintech.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import vn.com.hafintech.game.report.entity.RpUserStatistic;
import vn.com.hafintech.game.report.model.search.UserStatisticSearch;

public interface RpUserStatisticService extends IService<RpUserStatistic> {

    IPage<RpUserStatistic> findAllUserStatistic(UserStatisticSearch search);

}
