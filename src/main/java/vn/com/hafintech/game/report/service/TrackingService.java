package vn.com.hafintech.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import vn.com.hafintech.game.report.model.ActivityTrackingUser;
import vn.com.hafintech.game.report.model.TrackingMarketing;
import vn.com.hafintech.game.report.model.TrackingUser;
import vn.com.hafintech.game.report.model.TrackingUserService;
import vn.com.hafintech.game.report.model.search.BaseDateSearch;
import vn.com.hafintech.game.report.model.search.TrackingSearch;
import vn.com.hafintech.game.report.model.search.TrackingWinLossSearch;
import vn.com.hafintech.game.report.model.tracking.BaseTracking;

import java.util.List;

public interface TrackingService {
    IPage<ActivityTrackingUser> findAllAccountPlayMore(BaseDateSearch search);

    /**
     * win = 1
     * loss = 0
     *
     * @return
     */
    IPage<ActivityTrackingUser> findAllAccountWinLoss(TrackingWinLossSearch search);

    IPage<ActivityTrackingUser> findAllAccountGetOut(BaseDateSearch search);

    IPage<ActivityTrackingUser> findAllIAU(BaseDateSearch search);

    List<BaseTracking> findAllNruDaily(TrackingSearch ts);

    List<BaseTracking> findAllAuDaily(TrackingSearch ts);

    List<BaseTracking> findAllCbuDaily(TrackingSearch ts);

    List<BaseTracking> findAllPUDaily(TrackingSearch ts);

    List<BaseTracking> findAllRevenueDaily(TrackingSearch ts);

    List<BaseTracking> findAllARPPUDaily(TrackingSearch ts);

    IPage<TrackingUser> findAllTrackingUserInfo(TrackingSearch ts);

    List<TrackingMarketing> findAllTrackingMarketing(TrackingSearch ts);

    List<TrackingUserService> findAllTrackingUserService(TrackingSearch ts);
}
