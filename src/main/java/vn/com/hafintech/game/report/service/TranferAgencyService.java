package vn.com.hafintech.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import vn.com.hafintech.game.report.entity.TranferAgency;
import vn.com.hafintech.game.report.model.search.TopTransferUserAgencySearch;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Mr.007
 * @since 2019-08-28
 */
public interface TranferAgencyService extends IService<TranferAgency> {


    IPage<TranferAgency> topTransferAgency(TopTransferUserAgencySearch search);

    IPage<TranferAgency> topTransferAgencyDetails(TopTransferUserAgencySearch search);

}
