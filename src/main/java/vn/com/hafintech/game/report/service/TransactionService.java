package vn.com.hafintech.game.report.service;

import vn.com.hafintech.game.report.model.Transactions;

import java.util.List;

public interface TransactionService {


    List<Transactions> findTopTransaction(int accountId, int top);

}
