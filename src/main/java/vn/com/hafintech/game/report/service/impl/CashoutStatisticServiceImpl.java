package vn.com.hafintech.game.report.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.mapper.CrashoutStatisticMapper;
import vn.com.hafintech.game.report.model.BaseStatistics;
import vn.com.hafintech.game.report.model.ChargeStatistic;
import vn.com.hafintech.game.report.model.ChargeStatisticDetailsByCardValue;
import vn.com.hafintech.game.report.model.search.ChargeStatisticSearchInterval;
import vn.com.hafintech.game.report.model.search.ChargeStatisticSearchPage;
import vn.com.hafintech.game.report.page.PageTool;
import vn.com.hafintech.game.report.service.CashoutStatisticService;
import vn.com.hafintech.game.report.util.ChartUtil;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CashoutStatisticServiceImpl implements CashoutStatisticService {

    private final CrashoutStatisticMapper cashoutJStatisticMapper;
    private final PageTool pageTool;

    @Override
    public List<BaseStatistics> findAllCashoutAccountByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return ChartUtil.findAllDateTimeByInterval(searchInterval.getInterval(), searchInterval.getFromDate(), cashoutJStatisticMapper.findAllCrashoutAccountByIntervalMinute(searchInterval.getPartnerId(),
                searchInterval.getCardType(), searchInterval.getSourceId(), searchInterval.getCardValue(), searchInterval.getFromDate(),
                searchInterval.getToDate(), searchInterval.getInterval()));
    }

    @Override
    public List<BaseStatistics> findAllCashoutTransactionByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return ChartUtil.findAllDateTimeByInterval(searchInterval.getInterval(), searchInterval.getFromDate(),cashoutJStatisticMapper.findAllCrashoutTransactionByIntervalMinute(searchInterval.getPartnerId(),
                searchInterval.getCardType(), searchInterval.getSourceId(), searchInterval.getCardValue(), searchInterval.getFromDate(),
                searchInterval.getToDate(), searchInterval.getInterval()));
    }

    @Override
    public List<BaseStatistics> findAllCashoutAmountByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return ChartUtil.findAllDateTimeByInterval(searchInterval.getInterval(), searchInterval.getFromDate(),cashoutJStatisticMapper.findAllCrashoutAmountByIntervalMinute(searchInterval.getPartnerId(),
                searchInterval.getCardType(), searchInterval.getSourceId(), searchInterval.getCardValue(), searchInterval.getFromDate(),
                searchInterval.getToDate(), searchInterval.getInterval()));
    }

    @Override
    public List<BaseStatistics> findAllCashoutJByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return ChartUtil.findAllDateTimeByInterval(searchInterval.getInterval(), searchInterval.getFromDate(),cashoutJStatisticMapper.findAllCrashoutJByIntervalMinute(searchInterval.getPartnerId(),
                searchInterval.getCardType(), searchInterval.getSourceId(), searchInterval.getCardValue(), searchInterval.getFromDate(),
                searchInterval.getToDate(), searchInterval.getInterval()));
    }

    @Override
    public IPage<ChargeStatistic> findAllCashoutStatistic(ChargeStatisticSearchPage statisticSearch) {
        Page page = pageTool.getPage(statisticSearch);
        return cashoutJStatisticMapper.findAllCrashoutStatistic(page, statisticSearch.getPartnerId(),
                statisticSearch.getCardType(), statisticSearch.getSourceId(), statisticSearch.getCardValue(), statisticSearch.getFromDate(),
                statisticSearch.getToDate());
    }

    @Override
    public IPage<ChargeStatisticDetailsByCardValue> findAllCashoutStatisticByCardValue(ChargeStatisticSearchPage statisticSearch) {
        Page page = pageTool.getPage(statisticSearch);
        return cashoutJStatisticMapper.findAllCrashoutStatisticByCardValue(page, statisticSearch.getPartnerId(),
                statisticSearch.getCardType(), statisticSearch.getSourceId(), statisticSearch.getCardValue(), statisticSearch.getFromDate(),
                statisticSearch.getToDate());
    }

    @Override
    public IPage<ChargeStatisticDetailsByCardValue> findAllCashoutStatisticByCardType(ChargeStatisticSearchPage statisticSearch) {
        Page page = pageTool.getPage(statisticSearch);
        return cashoutJStatisticMapper.findAllCrashoutStatisticByCardType(page, statisticSearch.getPartnerId(),
                statisticSearch.getCardType(), statisticSearch.getSourceId(), statisticSearch.getCardValue(), statisticSearch.getFromDate(),
                statisticSearch.getToDate());
    }
}
