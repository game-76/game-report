package vn.com.hafintech.game.report.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.mapper.ChargeJStatisticMapper;
import vn.com.hafintech.game.report.model.BaseStatistics;
import vn.com.hafintech.game.report.model.ChargeStatistic;
import vn.com.hafintech.game.report.model.ChargeStatisticDetailsByCardValue;
import vn.com.hafintech.game.report.model.Partner;
import vn.com.hafintech.game.report.model.search.ChargeStatisticSearchInterval;
import vn.com.hafintech.game.report.model.search.ChargeStatisticSearchPage;
import vn.com.hafintech.game.report.page.PageTool;
import vn.com.hafintech.game.report.service.ChargeStatisticService;
import vn.com.hafintech.game.report.util.ChartUtil;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ChargeStatisticServiceImpl implements ChargeStatisticService {

    private final ChargeJStatisticMapper chargeJStatisticMapper;
    private final PageTool pageTool;

    @Override
    public List<Partner> findAllPartner(Integer isTopup) {
        return chargeJStatisticMapper.findAllPartner(isTopup);
    }

    @Override
    public List<BaseStatistics> findAllChargeAccountByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return ChartUtil.findAllDateTimeByInterval(searchInterval.getInterval(), searchInterval.getFromDate(),chargeJStatisticMapper.findAllChargeAccountByIntervalMinute(searchInterval.getPartnerId(),
                searchInterval.getCardType(), searchInterval.getSourceId(), searchInterval.getCardValue(), searchInterval.getFromDate(),
                searchInterval.getToDate(), searchInterval.getInterval()));
    }

    @Override
    public List<BaseStatistics> findAllChargeTransactionByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return ChartUtil.findAllDateTimeByInterval(searchInterval.getInterval(), searchInterval.getFromDate(),chargeJStatisticMapper.findAllChargeTransactionByIntervalMinute(searchInterval.getPartnerId(),
                searchInterval.getCardType(), searchInterval.getSourceId(), searchInterval.getCardValue(), searchInterval.getFromDate(),
                searchInterval.getToDate(), searchInterval.getInterval()));
    }

    @Override
    public List<BaseStatistics> findAllChargeAmountByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return ChartUtil.findAllDateTimeByInterval(searchInterval.getInterval(), searchInterval.getFromDate(),chargeJStatisticMapper.findAllChargeAmountByIntervalMinute(searchInterval.getPartnerId(),
                searchInterval.getCardType(), searchInterval.getSourceId(), searchInterval.getCardValue(), searchInterval.getFromDate(),
                searchInterval.getToDate(), searchInterval.getInterval()));
    }

    @Override
    public List<BaseStatistics> findAllChargeJByIntervalMinute(ChargeStatisticSearchInterval searchInterval) {
        return ChartUtil.findAllDateTimeByInterval(searchInterval.getInterval(), searchInterval.getFromDate(),chargeJStatisticMapper.findAllChargeJByIntervalMinute(searchInterval.getPartnerId(),
                searchInterval.getCardType(), searchInterval.getSourceId(), searchInterval.getCardValue(), searchInterval.getFromDate(),
                searchInterval.getToDate(), searchInterval.getInterval()));
    }

    @Override
    public IPage<ChargeStatistic> findAllReportStatistic(ChargeStatisticSearchPage statisticSearch) {
        Page page = pageTool.getPage(statisticSearch);
        return chargeJStatisticMapper.findAllReportStatistic(page, statisticSearch.getPartnerId(),
                statisticSearch.getCardType(), statisticSearch.getSourceId(), statisticSearch.getCardValue(), statisticSearch.getFromDate(),
                statisticSearch.getToDate());
    }

    @Override
    public IPage<ChargeStatisticDetailsByCardValue> findAllChargeStatisticByCardValue(ChargeStatisticSearchPage statisticSearch) {
        Page page = pageTool.getPage(statisticSearch);
        return chargeJStatisticMapper.findAllChargeStatisticByCardValue(page, statisticSearch.getPartnerId(),
                statisticSearch.getCardType(), statisticSearch.getSourceId(), statisticSearch.getCardValue(), statisticSearch.getFromDate(),
                statisticSearch.getToDate());
    }

    @Override
    public IPage<ChargeStatisticDetailsByCardValue> findAllChargeStatisticByCardType(ChargeStatisticSearchPage statisticSearch) {
        Page page = pageTool.getPage(statisticSearch);
        return chargeJStatisticMapper.findAllChargeStatisticByCardType(page, statisticSearch.getPartnerId(),
                statisticSearch.getCardType(), statisticSearch.getSourceId(), statisticSearch.getCardValue(), statisticSearch.getFromDate(),
                statisticSearch.getToDate());
    }
}
