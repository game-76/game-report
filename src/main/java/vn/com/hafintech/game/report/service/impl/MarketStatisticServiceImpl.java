package vn.com.hafintech.game.report.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.mapper.MarketStatisticMapper;
import vn.com.hafintech.game.report.model.ChargeMarketStatistic;
import vn.com.hafintech.game.report.model.ServiceMarketStatistic;
import vn.com.hafintech.game.report.model.SourceMarketStatistic;
import vn.com.hafintech.game.report.service.MarketStatisticService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class MarketStatisticServiceImpl implements MarketStatisticService {

    private final MarketStatisticMapper marketStatisticMapper;

    @Override
    public List<ServiceMarketStatistic> findAllDAUMarketStatistic(LocalDateTime fromDate, LocalDateTime toDate) {
        List<ServiceMarketStatistic> serviceMarketStatistics = marketStatisticMapper.findAllDAUMarketStatistic(fromDate, toDate);
        if (serviceMarketStatistics == null) {
            return null;
        }
        return calculateServicePercent(serviceMarketStatistics);
    }

    @Override
    public List<ServiceMarketStatistic> findAllTransactionMarketStatistic(LocalDateTime fromDate, LocalDateTime toDate) {
        List<ServiceMarketStatistic> serviceMarketStatistics = marketStatisticMapper.findAllTransactionMarketStatistic(fromDate, toDate);
        if (serviceMarketStatistics == null) {
            return null;
        }
        return calculateServicePercent(serviceMarketStatistics);
    }

    @Override
    public List<ServiceMarketStatistic> findAllFlowMoneyMarketStatistic(LocalDateTime fromDate, LocalDateTime toDate) {
        List<ServiceMarketStatistic> serviceMarketStatistics = marketStatisticMapper.findAllFlowMoneyMarketStatistic(fromDate, toDate);
        if (serviceMarketStatistics == null) {
            return null;
        }
        return calculateServicePercent(serviceMarketStatistics);
    }

    @Override
    public List<ServiceMarketStatistic> findAllRevenueMarketStatistic(LocalDateTime fromDate, LocalDateTime toDate) {
        List<ServiceMarketStatistic> serviceMarketStatistics = marketStatisticMapper.findAllRevenueMarketStatistic(fromDate, toDate);
        if (serviceMarketStatistics == null) {
            return null;
        }
        return calculateServicePercent(serviceMarketStatistics);
    }

    @Override
    public List<SourceMarketStatistic> findAllDAUMarketStatisticBySource(LocalDateTime fromDate, LocalDateTime toDate) {
        List<SourceMarketStatistic> serviceMarketStatistics = marketStatisticMapper.findAllDAUMarketStatisticBySource(fromDate, toDate);
        if (serviceMarketStatistics == null) {
            return null;
        }
        return calculateSourcePercent(serviceMarketStatistics);
    }

    @Override
    public List<SourceMarketStatistic> findAllTransactionMarketStatisticBySource(LocalDateTime fromDate, LocalDateTime toDate) {
        List<SourceMarketStatistic> serviceMarketStatistics = marketStatisticMapper.findAllTransactionMarketStatisticBySource(fromDate, toDate);
        if (serviceMarketStatistics == null) {
            return null;
        }
        return calculateSourcePercent(serviceMarketStatistics);
    }

    @Override
    public List<SourceMarketStatistic> findAllFlowMoneyMarketStatisticBySource(LocalDateTime fromDate, LocalDateTime toDate) {
        List<SourceMarketStatistic> serviceMarketStatistics = marketStatisticMapper.findAllFlowMoneyMarketStatisticBySource(fromDate, toDate);
        if (serviceMarketStatistics == null) {
            return null;
        }
        return calculateSourcePercent(serviceMarketStatistics);
    }

    @Override
    public List<SourceMarketStatistic> findAllRevenueMarketStatisticBySource(LocalDateTime fromDate, LocalDateTime toDate) {
        List<SourceMarketStatistic> serviceMarketStatistics = marketStatisticMapper.findAllRevenueMarketStatisticBySource(fromDate, toDate);
        if (serviceMarketStatistics == null) {
            return null;
        }
        return calculateSourcePercent(serviceMarketStatistics);
    }

    @Override
    public List<ChargeMarketStatistic> findAllChargeAccountMarketStatistic(LocalDateTime fromDate, LocalDateTime toDate) {
        List<ChargeMarketStatistic> serviceMarketStatistics = marketStatisticMapper.findAllChargeAccountMarketStatistic(fromDate, toDate);
        if (serviceMarketStatistics == null) {
            return null;
        }
        return calculateChargePercent(serviceMarketStatistics);
    }

    @Override
    public List<ChargeMarketStatistic> findAllChargeTransactionMarketStatistic(LocalDateTime fromDate, LocalDateTime toDate) {
        List<ChargeMarketStatistic> serviceMarketStatistics = marketStatisticMapper.findAllChargeTransactionMarketStatistic(fromDate, toDate);
        if (serviceMarketStatistics == null) {
            return null;
        }
        return calculateChargePercent(serviceMarketStatistics);
    }

    @Override
    public List<ChargeMarketStatistic> findAllChargeJMarketStatistic(LocalDateTime fromDate, LocalDateTime toDate) {
        List<ChargeMarketStatistic> serviceMarketStatistics = marketStatisticMapper.findAllChargeJMarketStatistic(fromDate, toDate);
        if (serviceMarketStatistics == null) {
            return null;
        }
        return calculateChargePercent(serviceMarketStatistics);
    }

    private List<ServiceMarketStatistic> calculateServicePercent(List<ServiceMarketStatistic> serviceMarketStatistics) {
        long total = serviceMarketStatistics.stream().mapToLong(k -> k.getPercent() < 0 ? 0 : k.getPercent()).sum();
        return serviceMarketStatistics.stream().peek(k -> k.setPercent(Math.round((k.getPercent() < 0 ? 0 : k.getPercent()) * 100.0 / total))).collect(Collectors.toList());
    }

    private List<SourceMarketStatistic> calculateSourcePercent(List<SourceMarketStatistic> serviceMarketStatistics) {
        long total = serviceMarketStatistics.stream().mapToLong(k -> k.getPercent() < 0 ? 0 : k.getPercent()).sum();
        return serviceMarketStatistics.stream().peek(k -> k.setPercent(Math.round((k.getPercent() < 0 ? 0 : k.getPercent()) * 100.0 / total))).collect(Collectors.toList());
    }


    private List<ChargeMarketStatistic> calculateChargePercent(List<ChargeMarketStatistic> serviceMarketStatistics) {
        long total = serviceMarketStatistics.stream().mapToLong(ChargeMarketStatistic::getPercent).sum();
        return serviceMarketStatistics.stream().peek(k -> k.setPercent(Math.round(k.getPercent() * 100.0 / total))).collect(Collectors.toList());
    }
}
