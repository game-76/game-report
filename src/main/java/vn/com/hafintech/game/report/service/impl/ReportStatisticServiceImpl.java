package vn.com.hafintech.game.report.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.mapper.StatisticMapper;
import vn.com.hafintech.game.report.model.*;
import vn.com.hafintech.game.report.model.search.ReportStatisticSearch;
import vn.com.hafintech.game.report.model.search.TableStatisticSearch;
import vn.com.hafintech.game.report.page.PageTool;
import vn.com.hafintech.game.report.service.ReportStatisticService;
import vn.com.hafintech.game.report.util.ChartUtil;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ReportStatisticServiceImpl implements ReportStatisticService {

    private final StatisticMapper statisticMapper;
    private final PageTool pageTool;

    @Override
    public List<ServiceType> findAllServiceTypeGame() {
        return statisticMapper.findAllServiceTypeGame();
    }

    @Override
    public List<ServiceRoom> findAllRoomService(Integer serviceTypeId) {
        return statisticMapper.findAllRoomService(serviceTypeId);
    }

    @Override
    public List<BaseStatistics> findAllDAUbyIntervalMinute(ReportStatisticSearch reportStatisticSearch) {
        return ChartUtil.findAllDateTimeByInterval(reportStatisticSearch.getInterval(), reportStatisticSearch.getFromDate(), statisticMapper.findAllDAUbyIntervalMinute(reportStatisticSearch.getSourceId(), reportStatisticSearch.getServiceId(),
                reportStatisticSearch.getRoomId(), reportStatisticSearch.getFromDate(), reportStatisticSearch.getToDate(), reportStatisticSearch.getInterval()));
    }

    @Override
    public List<BaseStatistics> findAllTransactionbyIntervalMinute(ReportStatisticSearch reportStatisticSearch) {
        return ChartUtil.findAllDateTimeByInterval(reportStatisticSearch.getInterval(), reportStatisticSearch.getFromDate(), statisticMapper.findAllTransactionbyIntervalMinute(reportStatisticSearch.getSourceId(), reportStatisticSearch.getServiceId(),
                reportStatisticSearch.getRoomId(), reportStatisticSearch.getFromDate(), reportStatisticSearch.getToDate(), reportStatisticSearch.getInterval()));
    }

    @Override
    public List<BaseStatistics> findAllFlowMoneyByIntervalMinute(ReportStatisticSearch reportStatisticSearch) {
        return ChartUtil.findAllDateTimeByInterval(reportStatisticSearch.getInterval(), reportStatisticSearch.getFromDate(), statisticMapper.findAllFlowMoneyByIntervalMinute(reportStatisticSearch.getSourceId(), reportStatisticSearch.getServiceId(),
                reportStatisticSearch.getRoomId(), reportStatisticSearch.getFromDate(), reportStatisticSearch.getToDate(), reportStatisticSearch.getInterval()));
    }

    @Override
    public List<BaseStatistics> findAllRevenueByIntervalMinute(ReportStatisticSearch reportStatisticSearch) {
        return ChartUtil.findAllDateTimeByInterval(reportStatisticSearch.getInterval(), reportStatisticSearch.getFromDate(), statisticMapper.findAllRevenueByIntervalMinute(reportStatisticSearch.getSourceId(), reportStatisticSearch.getServiceId(),
                reportStatisticSearch.getRoomId(), reportStatisticSearch.getFromDate(), reportStatisticSearch.getToDate(), reportStatisticSearch.getInterval()));
    }

    @Override
    public List<ReportStatistics> findAllReportStatisticsSummaryList(TableStatisticSearch tss) {
        return statisticMapper.findAllReportStatisticsSummary(tss.getFromDate(), tss.getToDate(), tss.getSourceId(), tss.getServiceId(),
                tss.getRoomId());
    }

    @Override
    public List<ReportStatisticsDetails> findAllReportStatisticsDetailsList(TableStatisticSearch tss) {
        return statisticMapper.findAllReportStatisticsDetails(tss.getFromDate(), tss.getToDate(), tss.getSourceId(), tss.getServiceId(),
                tss.getRoomId());
    }
}
