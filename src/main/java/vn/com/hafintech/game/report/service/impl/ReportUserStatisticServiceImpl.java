package vn.com.hafintech.game.report.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.entity.RpUserStatistic;
import vn.com.hafintech.game.report.mapper.ReportUserStatisticMapper;
import vn.com.hafintech.game.report.model.BaseStatistics;
import vn.com.hafintech.game.report.model.SourceMarketStatistic;
import vn.com.hafintech.game.report.model.SourceStatisticBase;
import vn.com.hafintech.game.report.model.UserDAUBySource;
import vn.com.hafintech.game.report.model.search.CCUStatisticSearch;
import vn.com.hafintech.game.report.model.search.ReportStatisticSearch;
import vn.com.hafintech.game.report.model.search.TableStatisticSearch;
import vn.com.hafintech.game.report.page.PageTool;
import vn.com.hafintech.game.report.service.ReportUserStatisticService;
import vn.com.hafintech.game.report.util.ChartUtil;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ReportUserStatisticServiceImpl implements ReportUserStatisticService {

    private final ReportUserStatisticMapper reportUserStatisticMapper;
    private final PageTool pageTool;

    @Override
    public List<UserDAUBySource> findAllUserDauBySource(ReportStatisticSearch search) {
        return reportUserStatisticMapper.findAllUserDauBySource(search.getServiceId(), search.getSourceId(),
                search.getRoomId(), search.getFromDate(), search.getToDate());
    }

    @Override
    public List<BaseStatistics> findAllCCU(CCUStatisticSearch reportStatisticSearch) {
        return ChartUtil.findAllDateTimeByInterval(reportStatisticSearch.getInterval(), reportStatisticSearch.getFromDate(), reportUserStatisticMapper.findAllCCU(
                reportStatisticSearch.getServiceId(),
                reportStatisticSearch.getOnline(),
                reportStatisticSearch.getSourceId(),
                reportStatisticSearch.getRoomId(),
                reportStatisticSearch.getFromDate(), reportStatisticSearch.getToDate(),
                reportStatisticSearch.getInterval()));
    }

    @Override
    public List<BaseStatistics> findAllNRUByMinute(ReportStatisticSearch reportStatisticSearch) {
        return ChartUtil.findAllDateTimeByInterval(reportStatisticSearch.getInterval(), reportStatisticSearch.getFromDate(), reportUserStatisticMapper.findAllNRUByMinute(
                reportStatisticSearch.getSourceId(),
                reportStatisticSearch.getFromDate(), reportStatisticSearch.getToDate(),
                reportStatisticSearch.getInterval()));
    }

    @Override
    public IPage<SourceStatisticBase> findAllNRUBySource(TableStatisticSearch search) {
        Page page = pageTool.getPage(search);
        return reportUserStatisticMapper.findAllNRUDailyBySource(page, search.getSourceId(), search.getFromDate(), search.getToDate());
    }

    @Override
    public List<SourceMarketStatistic> findAllNRUMarketStatisticBySource(LocalDateTime fromDate, LocalDateTime toDate) {
        List<SourceMarketStatistic> serviceMarketStatistics = reportUserStatisticMapper.findAllNRUPieBySource(fromDate, toDate);
        if (serviceMarketStatistics == null) {
            return null;
        }
        return calculateSourcePercent(serviceMarketStatistics);
    }

    @Override
    public List<RpUserStatistic> findAllRpUserStatistic(TableStatisticSearch search) {
        return reportUserStatisticMapper.findAllRpUserStatistic(search.getFromDate(), search.getToDate(),
                search.getServiceId(), search.getSourceId(), search.getRoomId());
    }

    @Override
    public List<BaseStatistics> findAllDPUByMinute(ReportStatisticSearch search) {
        return ChartUtil.findAllDateTimeByInterval(search.getInterval(), search.getFromDate(),
                reportUserStatisticMapper.findAllDPUByMinute(
                        search.getInterval(),
                        search.getServiceId(),
                        search.getSourceId(),
                        search.getRoomId(),
                        search.getFromDate(), search.getToDate()));
    }

    @Override
    public List<BaseStatistics> findAllPeekCCU(ReportStatisticSearch search) {
        return ChartUtil.findAllDateTimeByInterval(search.getInterval(), search.getFromDate(),
                reportUserStatisticMapper.findAllPeekCCUByMinute(
                        search.getInterval(),
                        search.getServiceId(),
                        search.getSourceId(),
                        search.getRoomId(),
                        search.getFromDate(), search.getToDate()));
    }

    private List<SourceMarketStatistic> calculateSourcePercent(List<SourceMarketStatistic> serviceMarketStatistics) {
        long total = serviceMarketStatistics.stream().mapToLong(SourceMarketStatistic::getPercent).sum();
        return serviceMarketStatistics.stream().peek(k -> k.setPercent(Math.round(k.getPercent() * 100.0 / total))).collect(Collectors.toList());
    }
}
