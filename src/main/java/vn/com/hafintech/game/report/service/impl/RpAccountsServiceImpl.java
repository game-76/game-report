package vn.com.hafintech.game.report.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.entity.Accounts;
import vn.com.hafintech.game.report.mapper.RpAccountsMapper;
import vn.com.hafintech.game.report.model.AccountBalanceHistory;
import vn.com.hafintech.game.report.model.AccountWinHistory;
import vn.com.hafintech.game.report.model.ServiceWinLossDetails;
import vn.com.hafintech.game.report.model.TopWinLoss;
import vn.com.hafintech.game.report.model.search.AccountBalanceHistorySearch;
import vn.com.hafintech.game.report.model.search.AccountWinHistorySearch;
import vn.com.hafintech.game.report.model.search.TopAccountBalanceSearch;
import vn.com.hafintech.game.report.model.search.TopWinLossSearch;
import vn.com.hafintech.game.report.page.PageTool;
import vn.com.hafintech.game.report.service.RpAccountsService;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Mr007
 * @since 2019-08-26
 */
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class RpAccountsServiceImpl extends ServiceImpl<RpAccountsMapper, Accounts> implements RpAccountsService {

    private final PageTool pageTool;

    @Override
    public IPage<Accounts> findTopAccountBalance(TopAccountBalanceSearch search) {
        Page page = pageTool.getPage(search);
        return baseMapper.findTopAccountBalance(page, search.getFromDate(), search.getToDate(), search.getNickname());
    }

    @Override
    public IPage<AccountBalanceHistory> findAllAccountBalanceHistory(AccountBalanceHistorySearch search) {
        Page page = pageTool.getPage(search);
        return baseMapper.findAllAccountBalanceHistory(page, search.getNickname(), search.getServiceTypeId(), search.getFromDate(), search.getToDate());
    }

    @Override
    public IPage<TopWinLoss> findAllTopWinLoss(TopWinLossSearch search) {
        Page page = pageTool.getPage(search);
        return baseMapper.findAllTopWinLoss(page, search.getNickName(), search.getServiceTypeId(), search.getFromDate(), search.getToDate(), search.getType());
    }

    @Override
    public IPage<AccountWinHistory> findAllNoHu(AccountWinHistorySearch search) {
        Page page = pageTool.getPage(search);
        return baseMapper.findAllNoHu(page, search.getNickName(), search.getServiceTypeId(), search.getFromDate(), search.getToDate(), search.getRoomID(),search.getBettingAmount());
    }

    @Override
    public IPage<AccountWinHistory> findAllWin(AccountWinHistorySearch search) {
        Page page = pageTool.getPage(search);
        return baseMapper.findAllWin(page, search.getNickName(), search.getServiceTypeId(), search.getFromDate(), search.getToDate(), search.getRoomID(),search.getBettingAmount());
    }

    @Override
    public List<ServiceWinLossDetails> findAllServiceWinLossDetails(TopWinLossSearch search) {
        return baseMapper.findAllServiceWinLossDetails(search.getNickName(), search.getServiceTypeId(), search.getFromDate(), search.getToDate(), search.getType());
    }
}
