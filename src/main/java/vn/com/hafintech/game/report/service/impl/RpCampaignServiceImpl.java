package vn.com.hafintech.game.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.entity.RpCampaign;
import vn.com.hafintech.game.report.exception.HaftApiException;
import vn.com.hafintech.game.report.mapper.RpCampaignMapper;
import vn.com.hafintech.game.report.service.RpCampaignService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Mr007
 * @since 2019-08-26
 */
@Service
@RequiredArgsConstructor
@Transactional
public class RpCampaignServiceImpl extends ServiceImpl<RpCampaignMapper, RpCampaign> implements RpCampaignService {

    @Override
    public void saveCampaign(RpCampaign campaign) {
        int count = baseMapper.selectCount(new QueryWrapper<RpCampaign>().lambda()
                .eq(RpCampaign::getCampaignSource, campaign.getCampaignSource().trim())
                .eq(RpCampaign::getStatus, 1)
        );
        if (count > 0) {
            throw new HaftApiException("Source đã tồn tại");
        }
        this.baseMapper.insert(campaign);
    }

    @Override
    public void updateCampaign(RpCampaign rpCampaign) {
        int count = baseMapper.selectCount(new QueryWrapper<RpCampaign>().lambda()
                .eq(RpCampaign::getCampaignSource, rpCampaign.getCampaignSource().trim())
                .eq(RpCampaign::getStatus, 1)
                .ne(RpCampaign::getId, rpCampaign.getId())
        );
        if (count > 0) {
            throw new HaftApiException("Source đã tồn tại");
        }
        this.baseMapper.updateById(rpCampaign);
    }

    @Override
    public void deleteRpCampaign(Integer id) {
        this.baseMapper.deleteById(id);
    }
}
