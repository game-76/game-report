package vn.com.hafintech.game.report.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.entity.RpDAUStatistic;
import vn.com.hafintech.game.report.mapper.RpDAUStatisticMapper;
import vn.com.hafintech.game.report.model.search.BaseDateSearch;
import vn.com.hafintech.game.report.page.PageTool;
import vn.com.hafintech.game.report.service.RpDAUStatisticService;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class RpDAUStatisticServiceImpl extends ServiceImpl<RpDAUStatisticMapper, RpDAUStatistic> implements RpDAUStatisticService {

    private final PageTool pageTool;

    @Override
    public IPage<RpDAUStatistic> findAllRpDauStatistic(BaseDateSearch search) {
        Page page = pageTool.getPage(search);
        return baseMapper.findAllDauStatisticRate(page, search.getFromDate(), search.getToDate());
    }
}
