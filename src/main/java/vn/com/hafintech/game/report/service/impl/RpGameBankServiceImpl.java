package vn.com.hafintech.game.report.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import vn.com.hafintech.game.report.entity.RpGameBank;
import vn.com.hafintech.game.report.mapper.RpGameBankMapper;
import vn.com.hafintech.game.report.service.RpGameBankService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Mr007
 * @since 2019-08-26
 */
@Service
public class RpGameBankServiceImpl extends ServiceImpl<RpGameBankMapper, RpGameBank> implements RpGameBankService {


}
