package vn.com.hafintech.game.report.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.entity.RpGameBankStatistic;
import vn.com.hafintech.game.report.mapper.RpGameBankStatisticMapper;
import vn.com.hafintech.game.report.model.GameBankStatisticInfo;
import vn.com.hafintech.game.report.service.RpGameBankStatisticService;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Mr007
 * @since 2019-08-26
 */
@Service
public class RpGameBankStatisticServiceImpl extends ServiceImpl<RpGameBankStatisticMapper, RpGameBankStatistic> implements RpGameBankStatisticService {

    @Transactional(readOnly = true)
    @Override
    public List<GameBankStatisticInfo> findAllGameBankStatisticInfo(Integer serviceTypeId, Integer roomID) {
        return baseMapper.findAllGameBankStatisticInfo(serviceTypeId, roomID);
    }
}
