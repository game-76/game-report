package vn.com.hafintech.game.report.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import vn.com.hafintech.game.report.entity.RpTransactionStatistic;
import vn.com.hafintech.game.report.mapper.RpTransactionStatisticMapper;
import vn.com.hafintech.game.report.service.RpTransactionStatisticService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Mr.007
 * @since 2019-08-27
 */
@Service
public class RpTransactionStatisticServiceImpl extends ServiceImpl<RpTransactionStatisticMapper, RpTransactionStatistic> implements RpTransactionStatisticService {

}
