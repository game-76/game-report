package vn.com.hafintech.game.report.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.entity.RpUserStatistic;
import vn.com.hafintech.game.report.mapper.RpUserStatisticMapper;
import vn.com.hafintech.game.report.model.search.UserStatisticSearch;
import vn.com.hafintech.game.report.page.PageTool;
import vn.com.hafintech.game.report.service.RpUserStatisticService;

@Service
@RequiredArgsConstructor
public class RpUserStatisticServiceImpl extends ServiceImpl<RpUserStatisticMapper, RpUserStatistic> implements RpUserStatisticService {

    private final PageTool pageTool;

    @Transactional(readOnly = true)
    @Override
    public IPage<RpUserStatistic> findAllUserStatistic(UserStatisticSearch search) {
        Page page = pageTool.getPage(search);
        return baseMapper.findAllRpUserStatistic(page, search.getFromDate(), search.getToDate(), search.getServiceTypeId(), search.getRoomId(), search.getSourceId());
    }
}
