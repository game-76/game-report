package vn.com.hafintech.game.report.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.mapper.TrackingMapper;
import vn.com.hafintech.game.report.model.ActivityTrackingUser;
import vn.com.hafintech.game.report.model.TrackingMarketing;
import vn.com.hafintech.game.report.model.TrackingUser;
import vn.com.hafintech.game.report.model.TrackingUserService;
import vn.com.hafintech.game.report.model.search.BaseDateSearch;
import vn.com.hafintech.game.report.model.search.TrackingSearch;
import vn.com.hafintech.game.report.model.search.TrackingWinLossSearch;
import vn.com.hafintech.game.report.model.tracking.BaseTracking;
import vn.com.hafintech.game.report.page.PageTool;
import vn.com.hafintech.game.report.service.TrackingService;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class TrackingServiceImpl implements TrackingService {

    private final TrackingMapper trackingMapper;
    private final PageTool pageTool;

    @Transactional(readOnly = true)
    @Override
    public IPage<ActivityTrackingUser> findAllAccountPlayMore(BaseDateSearch search) {
        Page page = pageTool.getPage(search);
        page.setOrders(Collections.singletonList(new OrderItem().setAsc(false).setColumn("count(*)")));
        return trackingMapper.findAllAccountPlayMore(page, search.getFromDate(), search.getToDate());
    }

    @Transactional(readOnly = true)
    @Override
    public IPage<ActivityTrackingUser> findAllAccountWinLoss(TrackingWinLossSearch search) {
        Page page = pageTool.getPage(search);
        page.setOrders(Collections.singletonList(new OrderItem().setAsc(false).setColumn("sum(t.ReturnAmount - t.PlayAmount)")));
        return trackingMapper.findAllAccountWinLoss(page, search.getFromDate(), search.getToDate(), search.getWin());
    }

    @Transactional(readOnly = true)
    @Override
    public IPage<ActivityTrackingUser> findAllAccountGetOut(BaseDateSearch search) {
        Page page = pageTool.getPage(search);
        page.setOrders(Collections.singletonList(new OrderItem().setAsc(false).setColumn("UserName")));
        return trackingMapper.findAllAccountGetOut(page, search.getFromDate(), search.getToDate());
    }

    @Transactional(readOnly = true)
    @Override
    public IPage<ActivityTrackingUser> findAllIAU(BaseDateSearch search) {
        Page page = pageTool.getPage(search);
        page.setOrders(Collections.singletonList(new OrderItem().setAsc(false).setColumn("UserName")));
        return trackingMapper.findAllIAU(page, search.getFromDate(), search.getToDate());
    }

    @Transactional(readOnly = true)
    @Override
    public List<BaseTracking> findAllNruDaily(TrackingSearch ts) {
        return trackingMapper.findAllNruDaily(ts.getFromDate(), ts.getToDate(), ts.getSourceId(), ts.getCampaignSource());
    }

    @Transactional(readOnly = true)
    @Override
    public List<BaseTracking> findAllAuDaily(TrackingSearch ts) {
        return trackingMapper.findAllAuDaily(ts.getFromDate(), ts.getToDate(), ts.getSourceId(), ts.getCampaignSource(), ts.getServiceId());
    }

    @Transactional(readOnly = true)
    @Override
    public List<BaseTracking> findAllCbuDaily(TrackingSearch ts) {
        return trackingMapper.findAllCbuDaily(ts.getFromDate(), ts.getToDate(), ts.getSourceId(), ts.getCampaignSource(), ts.getServiceId());
    }

    @Transactional(readOnly = true)
    @Override
    public List<BaseTracking> findAllPUDaily(TrackingSearch ts) {
        return trackingMapper.findAllPUDaily(ts.getFromDate(), ts.getToDate(), ts.getSourceId(), ts.getCampaignSource(), ts.getServiceId());
    }

    @Transactional(readOnly = true)
    @Override
    public List<BaseTracking> findAllRevenueDaily(TrackingSearch ts) {
        return trackingMapper.findAllRevenueDaily(ts.getFromDate(), ts.getToDate(), ts.getSourceId(), ts.getCampaignSource(), ts.getServiceId());
    }

    @Transactional(readOnly = true)
    @Override
    public List<BaseTracking> findAllARPPUDaily(TrackingSearch ts) {
        return trackingMapper.findAllARPPUDaily(ts.getFromDate(), ts.getToDate(), ts.getSourceId(), ts.getCampaignSource(), ts.getServiceId());
    }

    @Transactional(readOnly = true)
    @Override
    public IPage<TrackingUser> findAllTrackingUserInfo(TrackingSearch ts) {
        return trackingMapper.findAllTrackingUserInfo(ts.getFromDate(), ts.getToDate(), ts.getSourceId(), ts.getServiceId());
    }

    @Transactional(readOnly = true)
    @Override
    public List<TrackingMarketing> findAllTrackingMarketing(TrackingSearch ts) {
        return trackingMapper.findAllTrackingMarketing(ts.getFromDate(), ts.getToDate(), ts.getSourceId(), ts.getCampaignSource(), ts.getServiceId());
    }

    @Transactional(readOnly = true)
    @Override
    public List<TrackingUserService> findAllTrackingUserService(TrackingSearch ts) {
        return trackingMapper.findAllTrackingUserService(ts.getFromDate(), ts.getToDate(), ts.getSourceId(), ts.getServiceId());
    }
}
