package vn.com.hafintech.game.report.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.entity.TranferAgency;
import vn.com.hafintech.game.report.mapper.TranferAgencyMapper;
import vn.com.hafintech.game.report.model.search.TopTransferUserAgencySearch;
import vn.com.hafintech.game.report.page.PageTool;
import vn.com.hafintech.game.report.service.TranferAgencyService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Mr.007
 * @since 2019-08-28
 */
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class TranferAgencyServiceImpl extends ServiceImpl<TranferAgencyMapper, TranferAgency> implements TranferAgencyService {

    private final PageTool pageTool;

    @Override
    public IPage<TranferAgency> topTransferAgency(TopTransferUserAgencySearch search) {
        Page page = pageTool.getPage(search);
        return baseMapper.topTransferAgency(page, search.getFromDate(), search.getToDate(), search.getNickname(), search.getType());
    }

    @Override
    public IPage<TranferAgency> topTransferAgencyDetails(TopTransferUserAgencySearch search) {
        Page page = pageTool.getPage(search);
        return baseMapper.topTransferAgencyDetails(page, search.getFromDate(), search.getToDate(), search.getNickname(), search.getType());
    }
}
