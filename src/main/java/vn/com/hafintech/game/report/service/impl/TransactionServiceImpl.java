package vn.com.hafintech.game.report.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.hafintech.game.report.mapper.TransactionMapper;
import vn.com.hafintech.game.report.model.Transactions;
import vn.com.hafintech.game.report.service.TransactionService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final TransactionMapper transactionMapper;

    @Transactional(readOnly = true)
    @Override
    public List<Transactions> findTopTransaction(int accountId, int top) {
        return transactionMapper.findTopTransaction(accountId,top);
    }
}
