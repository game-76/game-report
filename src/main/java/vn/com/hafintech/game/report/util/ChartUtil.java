package vn.com.hafintech.game.report.util;

import lombok.experimental.UtilityClass;
import vn.com.hafintech.game.report.model.BaseStatistics;

import java.time.*;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@UtilityClass
public class ChartUtil {

    public List<BaseStatistics> findAllDateTimeByInterval(int interval, LocalDateTime dateTime, List<BaseStatistics> baseStatistics) {
        LocalDate currentDay = LocalDate.of(dateTime.getYear(), dateTime.getMonth(), dateTime.getDayOfMonth());
        Set<BaseStatistics> map = new HashSet<>();
        for (int i = 0; i < (24 * 60 * 60); i = i + (interval*60)) {
            LocalTime localTime = Instant.ofEpochSecond(i).atZone(ZoneId.of("GMT")).toLocalTime();
            LocalDateTime dt = LocalDateTime.of(currentDay, localTime);
            map.add(new BaseStatistics().setDateTime(dt).setTotal(0L));
        }
        if (baseStatistics != null) {
            for(BaseStatistics bs : baseStatistics) {
                map.remove(new BaseStatistics().setDateTime(bs.getDateTime()).setTotal(0L));
                map.add(bs);
            }
        }
        return map.stream().sorted(Comparator.comparing(BaseStatistics::getDateTime)).collect(Collectors.toList());
    }
}
