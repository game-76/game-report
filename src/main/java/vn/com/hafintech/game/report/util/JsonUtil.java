package vn.com.hafintech.game.report.util;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import lombok.experimental.UtilityClass;
import org.springframework.lang.Nullable;
import vn.com.hafintech.game.report.config.HaftJavaTimeModule;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;


@UtilityClass
public class JsonUtil {


    @Nullable
    public static String toJson(@Nullable Object object) throws JsonProcessingException {
        if (object == null) {
            return null;
        }
        return getInstance().writeValueAsString(object);
    }


    @Nullable
    public static byte[] toJsonAsBytes(@Nullable Object object) throws JsonProcessingException {
        if (object == null) {
            return null;
        }
        return getInstance().writeValueAsBytes(object);
    }


    public static JsonNode readTree(String jsonString) throws IOException {
        Objects.requireNonNull(jsonString, "jsonString is null");
        return getInstance().readTree(jsonString);
    }

    public static JsonNode readTree(InputStream in) throws IOException {
        Objects.requireNonNull(in, "InputStream in is null");
        return getInstance().readTree(in);
    }

    public static JsonNode readTree(byte[] content) throws IOException {
        Objects.requireNonNull(content, "byte[] content is null");
        return getInstance().readTree(content);
    }

    public static JsonNode readTree(JsonParser jsonParser) throws IOException {
        Objects.requireNonNull(jsonParser, "jsonParser is null");
        return getInstance().readTree(jsonParser);
    }


    @Nullable
    public static <T> T readValue(@Nullable byte[] content, Class<T> valueType) throws IOException {
        if (ObjectUtil.isEmpty(content)) {
            return null;
        }
        return getInstance().readValue(content, valueType);
    }

    @Nullable
    public static <T> T readValue(@Nullable String jsonString, Class<T> valueType) throws IOException {
        if (StrUtil.isBlank(jsonString)) {
            return null;
        }
        return getInstance().readValue(jsonString, valueType);
    }

    @Nullable
    public static <T> T readValue(@Nullable InputStream in, Class<T> valueType) throws IOException {
        if (in == null) {
            return null;
        }
        return getInstance().readValue(in, valueType);
    }


    @Nullable
    public static <T> T readValue(@Nullable byte[] content, TypeReference<?> typeReference) throws IOException {
        if (ObjectUtil.isEmpty(content)) {
            return null;
        }
        return getInstance().readValue(content, typeReference);
    }


    @Nullable
    public static <T> T readValue(@Nullable String jsonString, TypeReference<?> typeReference) throws IOException {
        if (StrUtil.isBlank(jsonString)) {
            return null;
        }
        return getInstance().readValue(jsonString, typeReference);
    }


    @Nullable
    public static <T> T readValue(@Nullable InputStream in, TypeReference<?> typeReference) throws IOException {
        if (in == null) {
            return null;
        }
        return getInstance().readValue(in, typeReference);
    }


    public static MapType getMapType(Class<?> keyClass, Class<?> valueClass) {
        return getInstance().getTypeFactory().constructMapType(Map.class, keyClass, valueClass);
    }


    public static CollectionLikeType getListType(Class<?> elementClass) {
        return getInstance().getTypeFactory().constructCollectionLikeType(List.class, elementClass);
    }


    @Nullable
    public static <T> List<T> readList(@Nullable byte[] content, Class<T> elementClass) throws IOException {
        if (ObjectUtil.isEmpty(content)) {
            return Collections.emptyList();
        }
        return getInstance().readValue(content, getListType(elementClass));
    }


    @Nullable
    public static <T> List<T> readList(@Nullable InputStream content, Class<T> elementClass) throws IOException {
        if (content == null) {
            return Collections.emptyList();
        }
        return getInstance().readValue(content, getListType(elementClass));
    }


    @Nullable
    private static <T> List<T> readList(@Nullable String content, Class<T> elementClass) throws IOException {
        if (ObjectUtil.isEmpty(content)) {
            return Collections.emptyList();
        }
        return getInstance().readValue(content, getListType(elementClass));
    }

    @Nullable
    public static <K, V> Map<K, V> readMap(@Nullable byte[] content, Class<?> keyClass, Class<?> valueClass) throws IOException {
        if (ObjectUtil.isEmpty(content)) {
            return Collections.emptyMap();
        }
        return getInstance().readValue(content, getMapType(keyClass, valueClass));
    }


    @Nullable
    public static <K, V> Map<K, V> readMap(@Nullable InputStream content, Class<?> keyClass, Class<?> valueClass) throws IOException {
        if (ObjectUtil.isEmpty(content)) {
            return Collections.emptyMap();
        }
        return getInstance().readValue(content, getMapType(keyClass, valueClass));
    }


    @Nullable
    public static <K, V> Map<K, V> readMap(@Nullable String content, Class<?> keyClass, Class<?> valueClass) throws IOException {
        if (ObjectUtil.isEmpty(content)) {
            return Collections.emptyMap();
        }
        return getInstance().readValue(content, getMapType(keyClass, valueClass));
    }

    public static ObjectMapper getInstance() {
        return JacksonHolder.INSTANCE;
    }

    private static class JacksonHolder {
        private static ObjectMapper INSTANCE = new JacksonObjectMapper();
    }

    private static class JacksonObjectMapper extends ObjectMapper {
        private static final long serialVersionUID = 4288193147502386170L;

        JacksonObjectMapper() {
            super();
            super.setLocale(Locale.ENGLISH);
            super.setTimeZone(TimeZone.getTimeZone(ZoneId.systemDefault()));
            super.setDateFormat(new SimpleDateFormat(DatePattern.NORM_DATETIME_PATTERN));
            super.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
            super.configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
            super.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
            super.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            super.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
            super.setTimeZone(TimeZone.getTimeZone(ZoneId.systemDefault()));
            super.findAndRegisterModules();
            super.registerModule(new HaftJavaTimeModule());
        }

    }
}
