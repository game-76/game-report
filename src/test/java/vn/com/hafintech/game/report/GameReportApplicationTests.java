package vn.com.hafintech.game.report;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vn.com.hafintech.game.report.model.search.TableStatisticSearch;

import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameReportApplicationTests {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Test
    public void contextLoads() {


        MappedStatement ms = sqlSessionFactory.getConfiguration().getMappedStatement("findAllReportStatisticsSummary");
        TableStatisticSearch tss = new TableStatisticSearch();
        tss.setFromDate(LocalDateTime.now());
        BoundSql boundSql = ms.getBoundSql(tss); // pass in parameters for the SQL statement

        System.out.println("SQL" + boundSql.getSql());
    }

}
