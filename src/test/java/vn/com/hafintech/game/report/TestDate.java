package vn.com.hafintech.game.report;

import org.junit.Test;

import java.time.*;
import java.util.Set;

public class TestDate {

    @Test
    public void name() {
//        List<LocalDateTime> allDateTimeByInterval = ChartUtil.findAllDateTimeByInterval(15 * 60, LocalDateTime.now());
//        allDateTimeByInterval = allDateTimeByInterval.stream().sorted(Comparator.comparingLong(LocalDateTime::getSecond)).collect(Collectors.toList());
//        allDateTimeByInterval.forEach(k -> System.out.println(k.toString()));
    }

    @Test
    public void testLocalTime() {
        LocalTime localTime = Instant.ofEpochSecond(15 * 60).atZone(ZoneId.systemDefault()).toLocalTime();
        System.out.println(localTime.toString());
    }

    @Test
    public void findZone() {
        Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();
        for (String zone : availableZoneIds) {

            ZoneId zoneId = ZoneId.of(zone);
            ZoneOffset offset = zoneId.getRules().getOffset(LocalDateTime.now());
            System.out.println("Zone: " + zone + ", Offset: " + offset.toString());

        }
    }
}
